<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>

<div class="footer-div width100 same-padding">
    <p class="footer-p white-text text-center">© <?php echo $time;?> Thousand Media, All Rights Reserved.</p>
</div>
<!-- Refer Link Modal -->
<div id="referlink-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referlink">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 text-center">What is Article Link/URL?</h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">Article link/URL is the link to your article. It shall not repeat with other article or contain any spacing or symbol such as coma and dot , 'etc. You can use - to separate the text. The more popular and hot keyword used to form the link will help your article get better ranking in Google search result page.</p>
        <img src="img/thousand-media/article-link.png" class="width100" alt="What is Article Link/URL?" title="What is Article Link/URL?">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referlink">Close</div>
     </div>           
	</div>
</div>
<!-- Refer Texteditor Modal -->
<div id="refertexteditor-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-refertexteditor">&times;</span>
        <div class="clear"></div>

    <div class="width100 overflow tutorial-div">
        <p class="step-p step-p2">How to place image? Click the image icon. Please don't directly copy paste image inside.</p>
        <img src="img/thousand-media/tutorial-04.jpg" class="width100">  
    	<p class="step-p step-p2">Right click the image and choose copy image address.</p>
        <img src="img/thousand-media/tutorial-05.jpg" class="width100" >         
    	<p class="step-p step-p2">Paste the image address inside and click ok.</p>
        <img src="img/thousand-media/tutorial-06.jpg" class="width100" >  
              
    	<p class="step-p step-p2"><b>How to Embed Youtube Video?</b></p>	   
    	<p class="step-p step-p2">Click iframe (the Earth icon) from the tool list.</p>
        <img src="img/thousand-media/tutorial-01.jpg" class="width100">
    	<p class="step-p step-p2">Click share below the Youtube video.</p>
        <img src="img/thousand-media/desktop-view-step1.jpg" class="width100">        
    	<p class="step-p step-p2">Click embed.</p>
        <img src="img/thousand-media/desktop-view-step2a.jpg" class="width100" >     
    	<p class="step-p step-p2">Copy the link ONLY, start from https</p>
        <img src="img/thousand-media/desktop-view-step3b.jpg" class="width100" >  
    	<p class="step-p step-p2">Paste the link and click ok.</p>
        <img src="img/thousand-media/tutorial-02.jpg" class="width100" >         
        <p class="step-p step-p2"><b>How to Embed Facebook Video?</b></p>
    	<p class="step-p step-p2">Make sure the video settings is public. Click share and then click embed.</p>
        <img src="img/thousand-media/fb1.png" class="width100" >            
    	<p class="step-p step-p2">Copy the link inside the after src (the highlighted part inside the picture).</p>
        <img src="img/thousand-media/fb2.png" class="width100">        
    	<p class="step-p step-p2">Click iframe (the Earth icon) from the tool list.</p>
        <img src="img/thousand-media/tutorial-01.jpg" class="width100">             
    	<p class="step-p step-p2">Paste the link and click ok.</p>
        <img src="img/thousand-media/tutorial-02.jpg" class="width100" >  
                
    	<p class="step-p step-p2"><b>How to insert link? Click the link/chain icon.</b></p>
        <img src="img/thousand-media/tutorial-03.jpg" class="width100" >
    	<p class="step-p step-p2">Type the display text for the link. Paste the link inside URL column.</p>
        <img src="img/thousand-media/link-tutorial.png" class="width100" >        
                        
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-refertexteditor">Close</div>
     </div>           
	</div>
</div>
<!-- Refer Keyword Modal -->
<div id="referde-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referde">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 text-center">Where Article Summary Will Be Shown?</h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">Where Article Summary Will Be Shown?</p>
        <img src="img/thousand-media/description.jpg" class="width100" alt="Where Article Summary Will Be Shown?" title="Where Article Summary Will Be Shown?">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referde">Close</div>
     </div>           
	</div>
</div>

<!-- Refer Keyword Modal -->
<div id="referkeyword-modal" class="modal-css">
	<div class="modal-content-css refer-modal-content">
        <span class="close-css close-referkeyword">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1 text-center">What is Google Keyword?</h1>	   
	<div class="clear"></div>
    <div class="width100 overflow tutorial-div">
    	<p class="step-p step-p2">Google Keyword is the keyword when the user typed to search for the content in Google Search. Google will show the website contains the keyword on top of the search result. You can help user to find this article easier if you key in related keyword. This keyword is only helping the system, it won't appear inside the article or anywhere to be seen. You need to use a coma , to separate each keyword. Example: eating, habit, healthy, vegetable,</p>
        <img src="img/thousand-media/meta-keyword.jpg" class="width100" alt="What is Google Keyword?" title="What is Google Keyword?">
    </div>        
     <div class="width100 overflow">
     	<div class="clean-button clean close-btn pink-button close-referkeyword">Close</div>
     </div>           
	</div>
</div>


<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>


<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
	<script src="js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
  <script src="js/index.js"></script>
  <script>
    (function(){
      new Progressive({
        el: '#app',
        lazyClass: 'lazy',
        removePreview: true,
        scale: true
      }).fire()
    })()
  </script>