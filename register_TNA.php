<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>

<title>Registration | Thousand Media</title>

<meta property="og:url" content="https://thousandmedia.asia/register.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Registration | Thousand Media" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />

<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">
<link rel="canonical" href="https://thousandmedia.asia/register.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">


<div class="width100 same-padding overflow gold-bg min-height-footer-only">

	<h2 class="h1-title">Add New User</h2> 
	<div class="clear"></div>
        <form action="utilities/registerFunction.php" method="POST">

            <div class="dual-input">
                <p class="input-top-text">Username</p>
                <input class="aidex-input clean" type="text" placeholder="Username" id="register_username" name="register_username" required>        
            </div> 

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Fullname</p>
                <input class="aidex-input clean"  type="text" placeholder="Fullname" id="register_fullname" name="register_fullname" required>        
            </div> 

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Contact</p>
                <input class="aidex-input clean"  type="text" placeholder="Contact" id="register_phone" name="register_phone" required>  
            </div>  

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Email</p>
                <input class="aidex-input clean"  type="email" placeholder="Email" id="register_email_user" name="register_email_user" required>        
            </div> 

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-text">Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">  				
                </div>
            </div>
            
            <div class="dual-input second-dual-input">
                <p class="input-top-text">Retype Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>  
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            	</div>
            </div>

            <div class="clear"></div>

            <button class="clean-button clean login-btn pink-button" name="submit">Create Account</button>
        </form>
       
</div>
<div class="clear"></div>

<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>