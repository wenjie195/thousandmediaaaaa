<header id="header" class="header header--fixed same-padding header1 menu-white blog-menu" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<a href="index.php">
            <img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media">
            <!--<img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">-->
            </a>
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="index.php" class="black-text menu-padding red-hover">Thousand Media</a>
            <!--<a href="#promotion" class="white-text menu-padding red-hover">Why Thousand Media</a>
 			<div class="dropdown">
            <a class="black-text menu-padding red-hover pointer hover1">Services <img src="img/thousand-media/arrow.png" class="dropdown-png hover1a"><img src="img/thousand-media/dropdown-pink.png" class="dropdown-png hover1b"></a>
                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="malaysia-penang-graphic-design-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Graphic Design</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-marketing-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Marketing Solutions</a></p>
						<p class="dropdown-p"><a href="malaysia-penang-content-copywriting.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Writing</a></p>                         
                	</div>            
            
            </div> -->           
            
 			<div class="dropdown">
            <a class="black-text menu-padding red-hover pointer hover1">Blog <img src="img/thousand-media/arrow.png" class="dropdown-png hover1a"><img src="img/thousand-media/dropdown-pink.png" class="dropdown-png hover1b"></a>
                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="blog.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Latest</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-content-marketing.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Marketing</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-graphic-design.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Design</a></p>
                	</div>            
            
            </div>            

		<!-- Mobile View-->
            <a href="index.php" class="black-text menu-padding red-hover2">
            	<img src="img/thousand-media/about-us2.png" class="menu-img" alt="About Thousand Media" title="About Thousand Media">
            </a>
			<!--
            <a href="blog.php" class="black-text menu-padding red-hover2">
            	<img src="img/thousand-media/blog2.png" class="menu-img" alt="Blog" title="Blog">            
            </a>
            <a href="malaysia-penang-graphic-design-services.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/graphic-design3-pink.png" class="menu-img" alt="Graphic Design" title="Graphic Design">            
            </a>            
            <a href="malaysia-penang-marketing-services.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-112.png" class="menu-img" alt="Marketing Solutions" title="Marketing Solutions">            
            </a>
            <a href="malaysia-penang-content-copywriting.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/content-writing-pink.png" class="menu-img" alt="Content Writing" title="Content Writing">            
            </a> -->           
            <a href="malaysia-penang-content-marketing.php" class="black-text menu-padding red-hover2">
            	<img src="img/thousand-media/content-marketing.png" class="menu-img" alt="Content Marketing" title="Content Marketing">            
            </a>
            <a href="malaysia-penang-graphic-design.php" class="black-text red-hover2">
            	<img src="img/thousand-media/graphic-design.png" class="menu-img" alt="Graphic Design" title="Graphic Design">            
            </a>            
        </div>
	</div>

</header>