<!--- Login Modal -->
<div id="login-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close close1">&times;</span>
    <div class="width100">
    	<div class="modal-left">
        	<img src="img/thousand-media/login.png" class="width100">
        </div>
        <div class="modal-right">
        	<h2 class="login-h2">Welcome Back</h2>
            <p class="login-p">Not yet have an account? <a class="open-register red-link">Create one.</a></p>
            <form class="login-form">
				<div class="input-container">
					<input type="email" class="inputa clean2"  name="" placeholder="Email">
					<img src="img/thousand-media/icon-05.png" class="input-icon" alt="email" title="email">
				</div>
				<div class="input-container last-input-container">
					
					<input type="password" class="inputa clean2 password-input"  name="" placeholder="Password">
					<img src="img/thousand-media/icon-06.png" class="input-icon"> 
                    <img src="img/thousand-media/icon-07.png" class="input-icon ow-icon-eye2">  
				</div>                  
				<div class="checkbox1">
					<input class="checkbox-input" type="checkbox" value="" id="">
                    	<label class="checkbox-label" for="">
                        	Remember Me
                    	</label>               
				</div>
                
				<button class="subscribe-button confirm-btn clean" name="" id="" >Login</button>                           	
           		<p class="login-p forgot-p"><a class="red-link open-forgot-password">Forgot Password?</a></p>
            </form>
        </div>
    </div>
  </div>

</div>
</div>




<!--- Login Modal -->
<div id="register-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close close2">&times;</span>
    <div class="width100">
    	<div class="modal-left">
        	<img src="img/thousand-media/register.png" class="width100">
        </div>
        <div class="modal-right">
        	<h2 class="login-h2">Register</h2>
            <p class="login-p">Already have an account? <a class="open-login red-link">Login here.</a></p>
            <form class="login-form">
				<div class="input-container">
					<input type="text" class="inputa clean2"  name="" placeholder="Full Name">
					<img src="img/thousand-media/name-icon.png" class="input-icon" alt="Full Name" title="Full Name">
				</div>            
				<div class="input-container">
					<input type="email" class="inputa clean2"  name="" placeholder="Email">
					<img src="img/thousand-media/icon-05.png" class="input-icon" alt="email" title="email">
				</div>
				<div class="input-container">
					
					<input type="password" class="inputa clean2 password-input"  name="" placeholder="Password">
					<img src="img/thousand-media/icon-06.png" class="input-icon"><img src="img/thousand-media/icon-07.png" class="input-icon ow-icon-eye2">    
				</div> 
				<div class="input-container">
					
					<input type="password" class="inputa clean2 password-input"  name="" placeholder="Retype Password">
					<img src="img/thousand-media/icon-06.png" class="input-icon"><img src="img/thousand-media/icon-07.png" class="input-icon ow-icon-eye2">    
				</div>    
 				<div class="input-container">
                    <select class="input-select inputa clean2">
                      <option class="input-option" disabled selected>Country</option>
                      <option class="input-option"  >Malaysia</option>
                      <option class="input-option"  >Singapore</option>
                      <option class="input-option"  >Thailand</option>
                    </select>
					<img src="img/thousand-media/country.png" class="input-icon" alt="country" title="country">
				</div>   
 				<div class="input-container">
                	<input type="text" class="inputa clean2 country-code-input" id="country-code-id"  name="" placeholder="">
					<input type="text" class="inputa clean2 phone-input" id="phone-number-id"  name="" placeholder="Phone Number">
                    <div class="clear"></div>
					<img src="img/thousand-media/phone.png" class="input-icon phone-icon" alt="Phone Number" title="Phone Number">
				</div>                                                                          
				<div class="checkbox1">
					<input class="checkbox-input" type="checkbox" value="" id="">
                    	<label class="checkbox-label" for="">
                        	I agree to the <a class="red-link">Terms,</a> <a class="red-link">Privacy Policy</a> and <a class="red-link">Fees.</a>
                    	</label>
                </div>
				<div class="checkbox1">                   
 					<input class="checkbox-input" type="checkbox" value="" id="">
                    	<label class="checkbox-label" for="">
                        	Yes, I want to receive Thousand Media’s newsletter.
                    	</label>                                  
				</div>
				<button class="subscribe-button confirm-btn clean" name="" id="" >Register</button>                           	
            </form>
        </div>
    </div>
  </div>

</div>



<!--- Forgot Password Modal -->
<div id="forgot-password-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close close-forgot-password">&times;</span>
    <div class="width100">
    	<div class="modal-left">
        	<img src="img/thousand-media/forgot-password.png" class="width100">
        </div>
        <div class="modal-right">
        	<h2 class="login-h2">Forgot Password</h2>
            <p class="login-p">Try <a class="open-login red-link">login</a> again.</p>
            <form class="login-form">        
				<div class="input-container">
					<input type="email" class="inputa clean2"  name="" placeholder="Email">
					<img src="img/thousand-media/icon-05.png" class="input-icon" alt="email" title="email">
				</div>
  				
				<button class="subscribe-button confirm-btn clean open-verify" name="" id="" >Submit</button>                           	
            </form>
        </div>
    </div>
  </div>

</div>


<!--- Verify Modal -->
<div id="verify-modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close close-verify">&times;</span>
    <div class="width100">
    	<div class="modal-left">
        	<img src="img/thousand-media/reset-password.png" class="width100">
        </div>
        <div class="modal-right">
        	<h2 class="login-h2">Verify</h2>
            <p class="login-p">Try <a class="open-login red-link">login</a> again.</p>
            <form class="login-form">        
				<div class="input-container">
					<input type="text" class="inputa clean2"  name="" placeholder="Key in the code you received through your email">
					<img src="img/thousand-media/code.png" class="input-icon" alt="Verify Code" title="Verify Code">
				</div>
				<div class="input-container">
					
					<input type="password" class="inputa clean2 password-input"  name="" placeholder="New Password">
					<img src="img/thousand-media/icon-06.png" class="input-icon"><img src="img/thousand-media/icon-07.png" class="input-icon ow-icon-eye2">    
				</div> 
				<div class="input-container">
					
					<input type="password" class="inputa clean2 password-input"  name="" placeholder="Retype Password">
					<img src="img/thousand-media/icon-06.png" class="input-icon"><img src="img/thousand-media/icon-07.png" class="input-icon ow-icon-eye2">    
				</div>				
				<button class="subscribe-button confirm-btn clean" name="" id="" >Submit</button>                           	
            </form>
        </div>
    </div>
  </div>

</div>

<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
        <script>
	$(document).ready(function() {
	var s = $(".menu-white");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=200) {
			s.addClass("menu-bg");
		} else {
			s.removeClass("menu-bg");	
		}
		});
	});
	if (window.matchMedia('screen and (max-width: 1200px)').matches) {
	$(document).ready(function() {
	var s = $(".menu-white");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=100) {
			s.addClass("menu-bg");
		} else {
			s.removeClass("menu-bg");	
		}
		});
	});		
		}
	</script> 
 
<script>
// Login modal
var modal = document.getElementById('login-modal');
// Get the button that opens the modal
var btn = document.getElementsByClassName('open-login')[0];
var btna = document.getElementsByClassName('open-login')[1];
var btnb = document.getElementsByClassName('open-login')[2];
var btnc = document.getElementsByClassName('open-login')[3];
var btnd = document.getElementsByClassName('open-login')[4];
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close1")[0];


// Register modal
var modal1 = document.getElementById('register-modal');
// Get the button that opens the modal
var btn1 = document.getElementsByClassName('open-register')[0];
var btn1a = document.getElementsByClassName('open-register')[1];
var btn1b = document.getElementsByClassName('open-register')[2];
// Get the <span> element that closes the modal
var span1 = document.getElementsByClassName("close2")[0];


// Forgot Password modal
var modal2 = document.getElementById('forgot-password-modal');
// Get the button that opens the modal
var btn2 = document.getElementsByClassName('open-forgot-password')[0];

// Get the <span> element that closes the modal
var span2 = document.getElementsByClassName("close-forgot-password")[0];


// Verify modal
var modalverify = document.getElementById('verify-modal');
// Get the button that opens the modal
var btnverify = document.getElementsByClassName('open-verify')[0];

// Get the <span> element that closes the modal
var spanverify = document.getElementsByClassName("close-verify")[0];



// When the user clicks on the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}
btna.onclick = function() {
  modal.style.display = "block";
}
btnb.onclick = function() {
  modal1.style.display = "none";	
  modal.style.display = "block";
}
btnc.onclick = function() {
  modal2.style.display = "none";	
  modal.style.display = "block";
}
btnd.onclick = function() {
  modalverify.style.display = "none";	
  modal.style.display = "block";
}
btn1.onclick = function() {
  modal1.style.display = "block";
}
btn1a.onclick = function() {
  modal1.style.display = "block";
}
btn1b.onclick = function() {
  modal.style.display = "none";
  modal1.style.display = "block";
}
btn2.onclick = function() {
  modal.style.display = "none";
  modal2.style.display = "block";
}
btnverify.onclick = function() {
  modal2.style.display = "none";	
  modalverify.style.display = "block";
}
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}
span1.onclick = function() {
  modal1.style.display = "none";
}
span2.onclick = function() {
  modal2.style.display = "none";
}
spanverify.onclick = function() {
  modalverify.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
  if (event.target == modal1) {
    modal1.style.display = "none";
  }  
  if (event.target == modal2) {
    modal2.style.display = "none";
  }    
  if (event.target == modalverify) {
    modalverify.style.display = "none";
  }   
  
  }

</script>  
<script> 
 $('#phone-number-id').on({
    focus: function () {
        $('#country-code-id').addClass('focused');
    },

    blur: function () {
        $('#country-code-id').removeClass('focused');
    }
});   
</script>   
