<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<title>5 Steps to Convert Your Brick and Mortar Business to Online Business During This COVID-19 Crisis | Thousand Media</title>
<meta property="og:url" content="https://thousandmedia.asia/steps-to-convert-business-to-online-business.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/online-business5.jpg" />
<meta property="og:title" content="5 Steps to Convert Your Brick and Mortar Business to Online Business During This COVID-19 Crisis | Thousand Media" />
<meta property="og:description" content="The best way is to find as many platforms as possible to find out which platform is the best, then double and triple on the platform that works." />
<meta name="description" content="The best way is to find as many platforms as possible to find out which platform is the best, then double and triple on the platform that works." />
<meta name="keywords" content="online business transformation, online business, online, convert, business, how to start an online business, work from home, malaysia, covid-19, Graphic Design, 平面设计, 设计, typography, photography, demonstration, illustration,  Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, blog, article, tips, news, etc">
<link rel="canonical" href="https://thousandmedia.asia/steps-to-convert-business-to-online-business.php" />
<?php include 'css.php'; ?>
</head>

<body class="body" >

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>

	<?php include 'blog-header.php'; ?>
<div class="blog-bg-div article-bg">

    <div class="article-padding width100 menu-distance">
        <div class="width100 same-padding">
            <h1 class="thousand-h1 blog-title article-title">5 Steps to Convert Your Brick and Mortar Business to Online Business During This COVID-19 Crisis</h1>
            <div class="gradient-border first-div-gradient blog-gradient"></div>
            <p class="author-name">by <b class="author-name weight-800">Joe D Flezer</b></p>
			<a href="malaysia-penang-content-marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 article-cat blog-article-cat">Content Marketing</div></a>
            <div class="width100 article-content">
            	<img src="img/thousand-media/online-business1.jpg" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">
            	<p class="article-date">8 Apr 2020</p>
                <h3 class="title-h3">Step 1 Research</h3>
                <p class="article-p">
                    
                    Competitor research - Look into competitors and understand how their business works, Understand competitor marketing strategy and emulate it.
                    <br><br>
                    <b>How?</b>
                    <br><br>
                	Use Google to search for your niche product and from there look into competitor websites and gather information on the website such as the website layout design, promotion method and social media.
                    <br><br>
                    Tool 1: <b>Ubersuggest</b> allow to analyze the competitor website visitors.<br>
                    Tool 2: <b>Waybackmachine</b> to look into technology competitors are using.<br>
                    Tool 3: <b>Socialblade</b> to analyze the social media performance
					<br><br>
                    After gathering so much information, we have a clear idea on what platform to go into.
                    <br><br>
                </p>
				<img src="img/thousand-media/online-business2.jpg" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">   
                <h3 class="title-h3">What should you do as new to E-commerce?</h3>
             
                <p class="article-p">

                <ol class="article-ul">
                	<li>Google search for your competitors at least 5 main competitors that are related to your niche/product. Try more than one search term. Example, smartphone case, smartphone protection case, rubber case, smartphone casing.</li>
                    <li>Having the competitor websites data, then you may look into the website designs, product display and social media. Look into all competitors and figure out what element all of them have, we are not going to copy but to emulate means copy but make it better. 
Why social media because this could determine whether social media is important or not. Some businesses don’t really require social media to grow whereas most of the customers come from external sources such as google, e-commerce platforms, referral and so on. Conversely, social media might be useful if competitors' social media are strong.</li>
                    <li>You may also want to research on a platform that your competitors use other than website and social media.</li>
                </ol>
        
                </p>
                <img src="img/thousand-media/online-business3.png" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">
                <h3 class="title-h3">Step 2 Choose Platforms</h3>
                <p class="article-p">
                	<b>How to choose?</b>
					<br><br>
                    Based on the data gather, you can decide on The best platforms to choose that competitors are using or try on a new platforms but this comes with cost as you do not know whether it works or not.<br><br>
                    Here are some of the platforms you can start with:<br><br>
                    <b>Social Media:</b><br>
                    <ul class="article-ul">
                    	<li>Facebook Groups</li>
                        <li>Facebook Page</li>
                        <li>Facebook & Instagram Ads</li>
                        <li>Instagram</li>
                        <li>Youtube Video</li>
                    </ul>
                    <br><br>
                    <b>E-commerce Platforms:</b><br>
                    <b>Product:</b> Shopee, Lelong, Mudah, Ebay, Lazada<br>
					<b>Service:</b> Fiverr, Upwork, Freelancer
					<br><br>
                    <b>Google:</b><br>
                    <ul class="article-ul">
                    	<li>Google Mybusiness</li>
                        <li>Google Image</li>
                        <li>Google Search Engine Result Page (Google Search)</li>
                    </ul>
                    <br><br>
                    <b>Website:</b><br>
                    <ul class="article-ul">
                    	<li>Wix</li>
                        <li>Easy Store</li>
                        <li>Sitegiant</li>
                        <li>Thousand Media (full customized)</li>
                    </ul>                                        
                    <br><br>
                    <b>External Platforms</b><br>
                    These platforms are difficult to define as they vary between industries. The best way to look for this platform is to Google search for it. Search terms, top platform, best place, where to promote, how to reach more audience for my product, what platform is the best for.
                    <br><br>
                    The best way is to find as many platforms as possible to find out which platform is the best, then double and triple on the platform that works.<br><br>
                </p>
                <img src="img/thousand-media/online-business4.jpg" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">
                <h3 class="title-h3">Step 3 Budget Your Online Business</h3>
                <p class="article-p">
                	The things you might think about how much to spend and what to spend. The best is to know what purpose you are spending or identify the problem you can’t overcome so you set your budget higher for that.
                   <br><br>
                   For example, the person is good at marketing but bad at creating websites so he can spend on outsourcing the person to do the website. Conversely, let's say the person is good at designing but bad at marketing, then he or she could spend on an online marketing course to improve on the skills or outsource experts $, yes we are promoting ourselves, <b>Thousand Media</b>. 
                   <br><br>
                   Another way to decide on budget is through observations. Look into the things that show results and double the budget on that.
                   <br>
                   <ul class="article-ul">
                    	<li>Marketing Budget</li>
                        <li>Advertisement Cost</li>
                        <li>AB testing or Ads Testing Cost</li>
                        <li>Paid Ads</li>
                        <li>Paid Influencer</li>
                    </ul>
                    <br><br>
                    <b>Website/Platforms Budget</b><br> 
                    <ul class="article-ul">
                    	<li>Some platforms are free but it requires some budget to boost it</li>
						<li>Website need a domain, website service provider/server hosting</li> 
						<li>Estimated RM60 to RM500 per month</li> 
                    </ul>
                    <br><br>
                    <b>Outsourcing Budget</b><br>    
                        You may choose to do it all by yourself for free but the quality and expertise is the concerning part. Designing your own logo, create a good photograph of product, marketing plan, designing the website or start a template website takes skills & times. By outsourcing a freelancer or us are an alternative choice and it costs $  in exchange for the expertise, skills and time. 
					<br><br>
                    
                </p>
                
                <img src="img/thousand-media/online-business5.jpg" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">
                <h3 class="title-h3">Step 4 Execution</h3>
                <p class="article-p">
                	Once everything is ready
                    <br><br>
                	<b>Website</b><br>
                    <ul class="article-ul">
                    	<li>Design a <b>logo</b>, usually for this part, the cheapest cost is to look into Youtube tutorial and create one of your own logo</li>
                    	<li>Plan for the <b>layout</b> based on simplicity, usefulness, convenient, usability.</li>
                        <li><b>Color</b> theme is based on your product, service and brand.</li>
                        <li><b>Graphic</b> contents pages can get it for free from Unsplash for high quality copyright free picture.</li>
                        <li><b>Written contents</b> that persuade (copywriting), is a technique to persuade through words.</li>
                    </ul>
                    <br><br>
                	<b>Social Media</b><br>
                    <ul class="article-ul">
                    	<li>Create and build your brand through social media. Different social media has different tactics to grow and gain visitors.</li>
                    	<li>Look into the successful competitor social media & emulate their posting but add it some new flavor such as the contents, time of posting,  posting consistency, caption, hashtag, tagging and what features they used.</li>
                    </ul>                    
                    <br><br>
                    <b>Ecommerce Platforms</b><br>
                    <ul class="article-ul">
                    	<li>In these platforms, the first thing to do is research. Yes research first, not straight posting your product service and after a few months no result.</li>
                    	<li>Understand how the platforms work then only make decisions on that.</li>
                    </ul>
                    <br><br>
                    <b>Other Platforms</b><br>                    
                    This also depends on the industry itself, some industries have better platforms for example service platforms like Fiverr, Upwork, Freelancer.<br><br>
                </p>
                
                
                <img src="img/thousand-media/online-business6.jpg" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">      
                <h3 class="title-h3">Step 5 Fulfill the Order</h3>    
                <p class="article-p">
                    The process of fulfilling the orders placed online and managing customer expectations are crucial. Here are a few things to take note. 
					<br><br>
                    <b>Payment Channels</b><br>
                    Bank transfer, Ipay88, Paypal, MOL pay, Ewallet payment, Maybank QR pay, TouchNGo Ewallet payment and others
                    <br><br>
                    <b>Shipping Time</b><br>
                    Be clear on the shipping time with your customers, inform them when they will receive the product if ordered now. Usually is to give at 1 to 2 days buffer so incase shipment delay and also to create a high satisfaction rate. If the product is going to take 3 days to arrive, you can mention to your customer it will take 5 days so the customer will feel happy since the service is delivered faster than expected. Managing expectations is important to get highly satisfied customers.
                    <br><br>
                    <b>Packaging</b><br>
                    It is important to make the packaging look nice as it will represent the quality of the brand. Another thing is due to shipping via third part and for fragile items it is very important to bubble wrap or place some styrofoam to protect the goods as we want the goods to reach safely on the customer hand.
                    <br><br>
                    <b>Return & Refund Policy</b><br>
                    Be clear with the return and refund policy set by looking through other online examples on how the return and refund policy looks like. Some shops might be very lenient with it’s return and refund policy because they might have a high profit margin product or have large sales to recover from the losses. Nonetheless, as a starter refund and policy should only happen if the item is broken or received wrong item.
                    
                   
                </p> 
               
            </div>
            
            <div class="share-div">
            	<h3 class="thousand-h1 blog-title3 share-title">Share</h3>	
                <div class="clear"></div>
            	<script async src="https://static.addtoany.com/menu/page.js"></script>

                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        <a class="a2a_button_copy_link"></a>
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_linkedin"></a>
                        <a class="a2a_button_blogger"></a>
                        <a class="a2a_button_facebook_messenger"></a>
                        <a class="a2a_button_whatsapp"></a>
                        <a class="a2a_button_wechat"></a>
                        <a class="a2a_button_line"></a>
                        <a class="a2a_button_telegram"></a>
                        <!--<a class="a2a_button_print"></a>-->
                        </div> 
                               
            </div>
            <div class="clear"></div>
            <div class="share-div comment-div">
            	<h3 class="thousand-h1 blog-title3 share-title">Comment</h3>	
                <div class="clear"></div>            
            	<div id="disqus_thread"></div>
				<script>
                
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                var disqus_config = function () {
                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                */
                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://thousandmedia.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>                 <script id="dsq-count-scr" src="//thousandmedia.disqus.com/count.js" async></script>         
                            
                            
                            
                             <a href="https://thousandmedia.asia/steps-to-convert-business-to-online-business.php#disqus_thread" class="live-hide"></a>
            </div>            
            
            
            
            
            
            
            
            <h3 class="thousand-h1 blog-title3">Latest Article</h3>
            <a href="blog.php"><div class="white-button">See More</div></a>
            <div class="clear"></div>
            	<div class="width100 white-box2">
                    <div class="three-div-image planning-bg">
                	<a href="how-to-register-prihatin-malaysia-wage-subsidy.php" class="hover-opacity">
                    	<img src="img/thousand-media/rm600-wage-subsidy.jpg" class="width100" alt="4 Steps to Register RM600 Wage Subsidy" title="4 Steps to Register RM600 Wage Subsidy">
                   </a>
                </div>
                <div class="three-div-content">
                    <h2 class="blog-title-h2 text-overflow"><a href="how-to-register-prihatin-malaysia-wage-subsidy.php" class="hover-turn-red">4 Steps to Register RM600 Wage Subsidy</a></h2>
                    <p class="blog-date"><a href="how-to-register-prihatin-malaysia-wage-subsidy.php" class="hover-opacity">1 Apr 2020</a></p>
                    <p class="blog-desc"><a href="how-to-register-prihatin-malaysia-wage-subsidy.php" class="hover-opacity">Private sector employers can register for RM600 wage subsidy per worker for a duration of 3 months offered by the Social Security Organisation (Socso) due to Covid-19 outbreak. This is to help the employers in surviving their business yet retaining their employees.
</a></p>
                    <a href="malaysia-penang-content-marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 blog-article-cat">Marketing</div></a>
                </div>
            </div>            
            
            
            	<div class="width100 white-box2">
                    <div class="three-div-image planning-bg">
                        <a href="copywriting-content-marketing-content-strategy.php" class="hover-opacity">
                            <img src="img/thousand-media/content-marketing.jpg" class="width100" alt="Content Marketing" title="Content Marketing">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="copywriting-content-marketing-content-strategy.php" class="hover-turn-red">Copywriting vs content marketing vs content strategy. Which is the best to make or increase your brand exposure?</a></h2>
                        <p class="blog-date"><a href="copywriting-content-marketing-content-strategy.php" class="hover-opacity">19 Feb 2020</a></p>
                        <p class="blog-desc"><a href="copywriting-content-marketing-content-strategy.php" class="hover-opacity">Let’s review the definition of copywriting, content marketing and strategy first before proceeding to the detailed information. The word “content” is a tricky concept because there are several formats and types that constitute content.</a></p>
                        <a href="malaysia-penang-content-marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 blog-article-cat width-auto">Marketing</div></a>
                    </div>
                </div>            
            	<div class="width100 white-box2">
                    <div class="three-div-image planning-bg">
                        <a href="definition-of-graphic-design-designer.php" class="hover-opacity">
                            <img src="img/thousand-media/graphic-design.jpg" class="width100" alt="Graphic Design" title="Graphic Design">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="definition-of-graphic-design-designer.php" class="hover-turn-red">What is the definition of graphic design, and what are the best things to take into consideration to become a designer.</a></h2>
                        <p class="blog-date"><a href="definition-of-graphic-design-designer.php" class="hover-opacity">19 Feb 2020</a></p>
                        <p class="blog-desc"><a href="definition-of-graphic-design-designer.php" class="hover-opacity">Graphic design is the specialty of making visual content to impart messages. Graphic Design is the process of communication with people and solving problems with the support of typography, photography, demonstration and illustration.</a></p>
                        <a href="malaysia-penang-graphic-design.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat width-auto">Graphic Design</div></a>
                    </div>
                </div>             
            
            <!--<div class="three-in-one-row">
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                	</div>
                	<div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>    
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                	</div>
                	<div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>                         
          </div>-->
          <div class="clear"></div>
          
          
        </div>
</div>    
</div>          


    
    


<?php include 'js.php'; ?>

</body>
</html>