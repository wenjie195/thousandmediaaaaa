<!doctype html>
<html>
<head>
 
<title>Malaysia Graphic Design & Social Media Marketing Agency | Thousand Media Online Advertising Strategy & Digital Marketing</title>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://thousandmedia.asia/" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/fb-meta.jpg" />
<meta property="og:title" content="Malaysia Graphic Design & Social Media Marketing Agency | Thousand Media Online Advertising Strategy  & Digital Marketing" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">
  <link rel="stylesheet" type="text/css" href="./slick/slick.css">
  <link rel="stylesheet" type="text/css" href="./slick/slick-theme.css">
<?php include 'css.php'; ?>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
    </script>
    <script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
    </script>

  <link rel="canonical" href="https://thousandmedia.asia/" />
</head>

<body class="body" >
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<div id="overlay">
 <div class="center-food"><img src="img/thousand-media/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>
<header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<img src="img/thousand-media/logo.png" class="logo-img web-logo red-logo" alt="Thousand Media" title="Thousand Media">
            <img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">
           
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="#whatyouneed" class="white-text menu-padding opacity-white-hover red-hover">About Thousand Media</a>
            <a href="#packagess" class="white-text menu-padding opacity-white-hover red-hover">Services</a>
 			<!--<div class="dropdown">
            <a class="white-text menu-padding red-hover opacity-white-hover ">Services <img src="img/thousand-media/dropdown.png" class="dropdown-png white-dropdown"><img src="img/thousand-media/dropdown-grey.png" class="dropdown-png grey-dropdown"></a>
                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="malaysia-penang-graphic-design-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Graphic Design</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-marketing-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Marketing Solutions</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-content-copywriting.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Writing</a></p>    
                	</div>            
            
            </div>-->
            
            <a href="#contact" class="white-text menu-padding opacity-white-hover red-hover">Contact Us</a>
            <a href="digital-marketing-blog-news.php" class="white-text opacity-white-hover red-hover">Blog</a>
		<!-- Mobile View-->
            <a href="#whatyouneed" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/about-us.png" class="menu-img" alt="About Thousand Media" title="About Thousand Media">
            </a>
            <!--
            <a href="#promotion" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/why-choose-us.png" class="menu-img" alt="Why Thousand Media" title="Why Thousand Media">
            </a
            <a href="malaysia-penang-graphic-design-services.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/graphic-design3.png" class="menu-img" alt="Graphic Design" title="Graphic Design">            
            </a>  -->          
            <a href="#packagess" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Services" title="Services">            
            </a>
			<!--<a href="malaysia-penang-content-copywriting.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/content-writing-white.png" class="menu-img" alt="Content Writing" title="Content Writing">            
            </a>>-->
            <a href="#contact" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-10.png" class="menu-img" alt="Contact" title="Contact">            
            </a>
            <a href="digital-marketing-blog-news.php" class="white-text red-hover2">
            	<img src="img/thousand-media/blog.png" class="menu-img" alt="Blog" title="Blog">            
            </a>            
        </div>
	</div>

</header>
<div class="width100 second-row purple-bg2" id="whatyouneed">
	<div class="width100 stars-bg overflow">
        <div class="float-left first-left-div padding-left">
            <h1 class="thousand-h1 ow-white-text">Thousand Media</h1>
            <div class="gradient-border first-div-gradient first-border"></div>
            <p class="slogan-p">Creative Anytime</p>
            <div class="clear"></div>
            <p class="thousand-p first-div-p">Ineffective marketing, low visitors & low sales?<br><b>Our mission is to solve your business problems.</b></p>
        </div> 
        <div class="float-right first-right-div">
    
            <!--<video poster="img/thousand-media/rocket2.jpg" class="width100 web-rocket" playsinline autoplay muted loop  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
              <source src="img/thousand-media/rocket-animation2.mp4" type="video/mp4"  class="width100 web-rocket" >
              Your browser does not support the video tag.
            </video>-->
           <img src="img/thousand-media/digital-marketing-rocket.png" class="wow pulse animated width100" data-wow-iteration="infinite" style="visibility: visible; animation-duration: 8s; animation-iteration-count: infinite; animation-name: pulse;"  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
    
    
            <!--<img src="img/thousand-media/rocket-mobile4.gif" class="width100 mobile-rocket" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">-->
            <!--<video poster="img/thousand-media/rocket-mobile.jpg" class="width100 mobile-rocket" autoplay loop  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
              <source src="img/thousand-media/rocket-mobile.mp4" type="video/mp4"  class="width100 web-rocket" >
              Your browser does not support the video tag.
            </video>  -->      
        </div>
    </div>
	<div class="clear"></div>
    <div class="third-row width100 same-padding overflow">
        <div class="third-row-first-div float-left">
            <img src="img/thousand-media/service.png" class="wow pulse animated width100" data-wow-iteration="infinite" style="visibility: visible; animation-duration: 8s; animation-iteration-count: infinite; animation-name: pulse;" alt="Digital Marketing Services" title="Digital Marketing Services">
        </div>
        <div class="third-row-second-div float-right">
            <p class="thousand-p2"><b>Good product + brand</b> can generate revenues but without visitors no will know. <b>High traffic</b> is nice but without a good product and brand, no one cares.</p>
            <div class="gradient-border second-gradient"></div>
        </div>    
    </div> 
    <div class="clear"></div>
    <div class="width100 overflow">
    	<img src="img/thousand-media/gradient.png" class="width100">
    </div>   
</div>
<div class="clear"></div>
<div class="width100 overflow three-pack" id="packagess">
	
        <div class="pack1 pack">
        	<div class="bottom-bg marketing-bg">
                <p class="pack-b"><span class="thinner">PREMIUM</span><br>Consultancy</p>
                <p class="package-b3">(Min 6 Months)*</p>
                <ul class="pack-ul">
                    <li>Marketing Assessment</li>
                    <li>Marketing Strategy</li>
                    <li>Social Media Management</li>
                    <li>Google Ads</li>
                    <li>SEO</li>
                    <li>Video x 1</li>
                    <li>Photoshoot x 1</li>
                    <li>RM 1000 Ads Fee</li>
                </ul>
                <div class="divider"></div>
                <p class="bold-price">RM 6578/Month</p>
                <p class="slogan-p2">Sky Rocket Your Leads and Sales!</p>
                <a  class="pointer pack-a open-form"><div class="red-btn ow-pink-btn pack-btn">Contact Us</div></a>
       		</div>
        </div>
    
    
        <div class="pack2 pack">
        	<div class="bottom-bg content-bg">
                <p class="pack-b"><span class="thinner">MOST RECOMMENDED</span><br>Social Media Management<br>Facebook & Instagram</p>
                <p class="package-b3">(Min 3 Months)*</p>
                <ul class="pack-ul">
                    <li>Branding Assessment<br><span class="minor-span">• Logo</span></li>
                    <li>Social Media Posting<br>(10 – 12 Posts)<br><span class="minor-span">• Graphic Design </span><br><span class="minor-span">• Copywriting</span><br><span class="minor-span"><i>(Content Provided by Customer)</i></span></li>
                    <li>*1 Facebook Campaign Idea<br><span class="minor-span">Eg: Live, Luckdraw, Free Items, Storytelling</span></li>
                    <li>RM 300 Ads Fee</li>
                    <li>Free Digital Library Access</li>
                    <li>Facebook Ads Management</li>
                    <li>1 Hour Consultation</li>
                </ul>
                <div class="divider"></div>
                <p class="bold-price"><span class="small-just">Just</span> RM 2299/Month</p>
                <p class="slogan-p2">We Get Your Work Done!</p>
                <a  class="pointer pack-a open-form"><div class="red-btn ow-pink-btn pack-btn">Contact Us</div></a>
            </div>
        </div>
    
    
        <div class="pack3 pack">
        	<div class="bottom-bg graphic-bg ow-bottom-bg">
            	<div class="moon-bg2">
                    <p class="pack-b"><span class="thinner">FOR MICRO BUSINESS</span><br>Digital Library & Groups</p>
                    <p class="package-b3">(1 Month)*</p>
                </div>
                 <ul class="pack-ul">
                    <li>Content Marketing & Tools<br><span class="minor-span"><i>Eg: Canva & Video Template</i></span></li>
                    <li>Facebook Ads Guide</li>
                    <li>Instagram Guide</li>
                    <li>COMING SOON</li>
                </ul>
                <div class="divider divider3"></div>
                <p class="bold-price"><span class="small-just">Just</span> RM 350/Month</p>
                <p class="slogan-p2">For Beginners</p>                
                <a class="pointer pack-a open-form"><div class="red-btn ow-pink-btn pack-btn">Contact Us</div></a>
            </div>
        </div> 
          
</div>

<div class="four-row-div same-padding width100">
	<img src="img/thousand-media/left-galaxy.png" class="wow pulse animated left-img" data-wow-iteration="infinite" style="visibility: visible; animation-duration: 8s; animation-iteration-count: infinite; animation-name: pulse;" alt="Digital Marketing Services" title="Digital Marketing Services" alt="Cretivity Galaxy" title="Cretivity Galaxy">
    <img src="img/thousand-media/right-galaxy.png" class="wow pulse animated right-img" data-wow-iteration="infinite" style="visibility: visible; animation-duration: 8s; animation-iteration-count: infinite; animation-name: pulse;" alt="Digital Marketing Services" alt="Cretivity Galaxy" title="Cretivity Galaxy">
    <p class="text-center img-p"><img src="img/thousand-media/idea.png" class="wow pulse animated bulb-img" data-wow-iteration="infinite" style="visibility: visible; animation-duration: 8s; animation-iteration-count: infinite; animation-name: pulse;" alt="Creaivity" title="Creaivity"></p>
    <p class="row-4-p">Our services are package in the way to scale your business <b class="scale-b">10x</b></p>
    <!--<p class="arrow-p"><img src="img/thousand-media/arrow.png" class="text-center arrow-img"></p>-->
    
</div> 
<div class="clear"></div>

<div class="testi-div same-padding width100">
        <h2 class="thousand-h2 text-center">Testimonial</h2>
        <div class="gradient-border margin-auto"></div>
  <section class="regular slider">
	<a href="https://foodee.my/" target="_blank" class="opacity-hover">   
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-01.png" class="testi-logo" alt="FOODee" title="FOODee">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">FOODee</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p"><b>Thousand Media</b> is impressive! They created impressive designs for my Logo and social media postings. They are professional and efficient in meeting project deadlines while charging a reasonable price. I will throw all my designs for Thousand Media and just focus on my food research!</p>
        </div>
    </div>
    </a>
    <a href="https://tevy.asia/" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/tevy.png" class="testi-logo" alt="Tevy" title="Tevy">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Tevy</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">They are in charge of handling all my creative parts which including logo, social media posting and minor content writing part. Overall very satisfied with their services, can complete work on time and sometimes before the deadline. Efficient.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/Travelloo-110329750747303" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/travelloo.png" class="testi-logo" alt="Travelloo" title="Travelloo">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Travelloo</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">Help me with the logo design and some marketing planning for my business. Provide useful content that catch audiences’ eyeballs to my page. Their suggestions can meet my wants hence effective.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/loadeeapp" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-04.png" class="testi-logo" alt="Loadee" title="Loadee">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Loadee</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">Working out with my graphic designs, enhancing the website to increase brand name and reputation. They have their own ideas but still can create the design based on my requests.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/ChillitBuddy/" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-02.png" class="testi-logo" alt="Chill’it Buddy" title="Chill’it Buddy">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Chill’it Buddy</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">I wanted to build an attractive website and I started with Thousand Media. The website they created for me is impressive and user-friendly. I enjoyed communicating with Thousand Media's agents as they are friendly.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/FelicitePuff" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-03.png" class="testi-logo" alt="Félicité" title="Félicité">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Félicité</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">Request Thousand Media to help my business to increase traffic, likes, followers and etc to our social media site. Contents and strategies are well performed and it does help with our social media platform. Overall strategies and results are good.</p>
        </div>
    </div>
    </a>
  </section>
  <section class="regular2 slider">
  	<a href="https://foodee.my/" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-01.png" class="testi-logo" alt="FOODee" title="FOODee">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">FOODee</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p"><b>Thousand Media</b> is impressive! They created impressive designs for my Logo and social media postings. They are professional and efficient in meeting project deadlines while charging a reasonable price. I will throw all my designs for Thousand Media and just focus on my food research!</p>
        </div>
    </div>
    </a>
    <a href="https://tevy.asia/" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/tevy.png" class="testi-logo" alt="Tevy" title="Tevy">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Tevy</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">They are in charge of handling all my creative parts which including logo, social media posting and minor content writing part. Overall very satisfied with their services, can complete work on time and sometimes before the deadline. Efficient.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/Travelloo-110329750747303" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/travelloo.png" class="testi-logo" alt="Travelloo" title="Travelloo">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Travelloo</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">Help me with the logo design and some marketing planning for my business. Provide useful content that catch audiences’ eyeballs to my page. Their suggestions can meet my wants hence effective.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/loadeeapp" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-04.png" class="testi-logo" alt="Loadee" title="Loadee">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Loadee</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">Working out with my graphic designs, enhancing the website to increase brand name and reputation. They have their own ideas but still can create the design based on my requests.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/ChillitBuddy/" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-02.png" class="testi-logo" alt="Chill’it Buddy" title="Chill’it Buddy">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Chill’it Buddy</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">I wanted to build an attractive website and I started with Thousand Media. The website they created for me is impressive and user-friendly. I enjoyed communicating with Thousand Media's agents as they are friendly.</p>
        </div>
    </div>
    </a>
    <a href="https://www.facebook.com/FelicitePuff" target="_blank" class="opacity-hover">
    <div class="testi-item">
     	<div class="width100 overflow">
        	<div class="left-logo-div">
            	<img src="img/thousand-media/client-03.png" class="testi-logo" alt="Félicité" title="Félicité">
            </div>
            <div class="right-detail-div">
            	<p class="testi-title">Félicité</p>
                <p class="review-p"><img src="img/thousand-media/5-star-review.png" class="review-png" alt="Excellent Service" title="Excellent Service"></p>
            </div>
        </div>
        <div class="review-info-div">
        	<p class="review-info-p">Request Thousand Media to help my business to increase traffic, likes, followers and etc to our social media site. Contents and strategies are well performed and it does help with our social media platform. Overall strategies and results are good.</p>
        </div>
    </div>
    </a>
  </section>		
</div>
<div class="clear"></div>
<div class="width100 overflow same-padding">
        <h2 class="thousand-h2 text-center">FAQs</h2>
        <div class="gradient-border margin-auto"></div>
        <div class="faq-div overflow">
            <div class="question-div option-heading ow-margin-top0">
                <p>1. Does it work for my business?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            Our marketing packs are bundled together in a way to resolve business owners’ biggest challenges and to overcome it through deep market research, marketing plan and much more.
            
            </div>
            <div class="clear"></div>
            <div class="question-div option-heading">
                <p>2. What if it doesn’t work?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            	Feel free to unsubscribe to us, if it doesn’t work for your business.
            </div>            
            <div class="clear"></div>
            <div class="question-div option-heading">
                <p>3. The price is too expensive?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            	Price represents the quality and professionalism. Thousand Media does not offer products but solutions to your business problems.
            </div>                 
            <div class="clear"></div>
            <div class="question-div option-heading">
                <p>4. How does the queued based for unlimited package works?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            	Queued based means the client submits requests and our assigned team will work on it one request and proceed to next requests once it’s done. Requests will be done around 1 to 2 reviews per business day.
            </div>              
            <div class="clear"></div>
            <div class="question-div option-heading">
                <p>5. How does this going to help my business?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            	Businesses without online markets nowadays are dying. However, entering the online market is difficult challenges and without proper skills, businesses might lose money more than profit. Thus, Thousand Media is a platform to help your online business.
            </div> 
            <div class="clear"></div>
            <div class="question-div option-heading">
                <p>6. Why do I need SEO?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            	It is now estimated that approximately 80%-90% of customers will check online reviews before making a purchase. Search Engine Optimization gives your company a better chance of being seen by potential customers. According to Forbes, almost 60% of small businesses are now investing in some form of online marketing. So the odds are that your competitors are using SEO.
            </div>            
            <div class="clear"></div>
            <div class="question-div option-heading">
               <p>7. Why should I hire an agency rather than in-house?</p>
            </div>
            <div class="option-content is-hidden answer-div">
            	It is better to let the professionals handle it. You and your staff are experts at what you do and we are the experts of digital marketing. The primary benefit of using a digital marketing agency is experience and resources. A digital marketing agency like Thousand Media has access to knowledge and resources that you may not.
            </div>              
                       
		</div>
</div>







<div id="contact">
    <div class="contactus-row width100">
		<div class="contact-big-div overflow">
            

           <h2 class="thousand-h2 ow-margin-bottom">Customized Packages</h2>
           <div class="gradient-border"></div> 
			<p class="contact-p1">Customize marketing & design services based on your business needs. Consult us now!</p>
                <!-- <form id="contactform" method="post" action="index.php" class="form-class extra-margin"> -->
                <form class="form-class extra-margin" action="utilities/selectCustomizedPackageFunction.php" method="POST">
                  <input type="text" name="name" placeholder="Your Name" class="input-name clean" ><br>
                  <input type="email" name="email" placeholder="Email" class="input-name clean" ><br>
                  <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" ><br>
                  <textarea name="comments" placeholder="Type your message here" class="input-message clean" ></textarea>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required>                  
                  </div>
                  <div class="float-left radio-p-div">
                  	<p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
                  </div>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required>                   
                  </div>
                  <div class="float-left radio-p-div">                                    
                  	<p class="opt-msg left">I just want to be contacted based on my request/ inquiry</p>
                  </div>
                  <div class="clear"></div>
                  <div class="width100 text-center">
                  	<input type="submit" name="submit" value="Send" class="input-submit white-text clean pointer">
                  </div>
                </form>      
            <h2 class="thousand-h2 ow-margin-bottom">Digital Marketing Sales</h2>
            <div class="gradient-border"></div>
            <div class="one-row width100 margin-top30">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/manager.png" class="width100 box-icon contact-left-icon" alt="Thousand Media Email"  title="Thousand Media Email">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p"><b>Kevin Yam</b></p>
                </div>
           </div>              
            <div class="one-row width100">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/call.png" class="width100 box-icon contact-left-icon" alt="Contact Us Thousand Media"  title="Contact Us Thousand Media">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p"><span class="web-span">+6 016 532 4691</span><a href="tel:+60165324691" class="tel-a">+6 016 532 4691</a></p>
                </div>
           </div>     
            <div class="one-row width100">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/address.png" class="width100 box-icon contact-left-icon" alt="Contact Us Thousand Media"  title="Contact Us Thousand Media">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p">1-15-12B, Suntech@Penang Cybercity, Lintang Mayang Pasir 3, 11950 Bayan Lepas</p>
                </div>
           </div>             
           
           
                   
            <h2 class="thousand-h2 ow-margin-bottom">Contact Us</h2>
            <div class="gradient-border"></div>
       
           <p class="icon-p">
                <a href="https://www.instagram.com/thousandmedia.asia/" target="_blank">
                    <img src="img/thousand-media/instagram2.png" class="social-icon hover-opacity" alt="Thousand Media Instagram"  title="Thousand Media Instagram">
                </a>
                <a href="https://www.facebook.com/thousandmedia" target="_blank">
                    <img src="img/thousand-media/facebook2.png" class="social-icon social-icon2 hover-opacity" alt="Thousand Media Facebook"  title="Thousand Media Facebook">
                </a>
                <a href="https://twitter.com/media_thousand" target="_blank">
                    <img src="img/thousand-media/twitter.png" class="social-icon social-icon2 hover-opacity" alt="Thousand Media Twitter"  title="Thousand Media Twitter">
                </a>                
                
                
           </p>
           <p class="icon-p"><a class="tm-a" href="terms.php" target="_blank">Terms and Conditions</a></p>
     

       </div>
        
    </div>
    <div class="clear"></div>
	<div class="footer-img-div">
    	<img src="img/thousand-media/footer.png" class="width100" alt="Creative" title="Creative">
    </div>
    <div class="footer-div width100 same-padding home-footer">
        <p class="footer-p white-text text-center">© <?php echo $time;?> Thousand Media, All Rights Reserved.</p>
    </div>



<div id="form-modal" class="modal-css">

  <!-- Modal content need to click learn more-->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-form">&times;</span>
                <!-- <form id="contactform" method="post" action="index.php" class="form-class extra-margin"> -->
                <form class="form-class extra-margin" action="utilities/selectPackageFunction.php" method="POST">
                  <input type="text" name="name" placeholder="Your Name" class="input-name clean form-input" ><br>
                  <!-- <input type="email" name="email" placeholder="Email" class="input-name clean form-input" ><br> -->
                  <input type="text" name="email" placeholder="Email" class="input-name clean form-input" ><br>
                  <!-- <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean form-input" ><br> -->
                  <input type="text" name="phone" placeholder="Contact Number" class="input-name clean form-input" ><br>

                    <select class="input-name clean form-input" name="package" required>
                        <option value="">Please Select a Package</option>
                        <option value="Consultancy">Consultancy</option>
                        <option value="Social Media Management">Social Media Management</option>
                        <option value=">Digital Library & Groups">Digital Library & Groups</option>
                        <!--<option value="Thousand Arts">Thousand Arts</option>
                        <option value="Infinity Content">Infinity Content</option>
                        <option value="7 Days Free Trial">7 Days Free Trial</option>-->
                    </select>

                  <textarea name="comments" placeholder="Type your message here" class="input-message clean form-input" ></textarea>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required>                  
                  </div>
                  <div class="float-left radio-p-div">
                  	<p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
                  </div>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required>                   
                  </div>
                  <div class="float-left radio-p-div">                                    
                  	<p class="opt-msg left">I just want to be contacted based on my request/ inquiry</p>
                  </div>
                  <div class="clear"></div>
                   
                  <input type="submit" name="submit" value="Send" class="input-submit white-text clean pointer hover-a-reverse width100">
                </form> 
  </div>

</div>

<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#d21f3c;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
.playstore-img:hover{
	opacity:0.8 !Important;}
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>



<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
        <script>
	$(document).ready(function() {
	var s = $(".menu-white");
	var r = $(".red-logo");
	var w = $(".white-logo");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=200) {
			s.addClass("menu-bg");
			r.addClass("display-none");
			w.addClass("display-block");
		} else {
			s.removeClass("menu-bg");
			r.removeClass("display-none");
			w.removeClass("display-block");	
		}
		});
	});

	</script> 
<!--- Modal Box --->
<script>
var formmodal = document.getElementById("form-modal");
var openform = document.getElementsByClassName("open-form")[0];
var openform1 = document.getElementsByClassName("open-form")[1];
var openform2 = document.getElementsByClassName("open-form")[2];
var openform3 = document.getElementsByClassName("open-form")[3];
var openform4 = document.getElementsByClassName("open-form")[4];
var openform5 = document.getElementsByClassName("open-form")[5];
var openform6 = document.getElementsByClassName("open-form")[6];
var openform7 = document.getElementsByClassName("open-form")[7];
var openform8 = document.getElementsByClassName("open-form")[8];
var openform9 = document.getElementsByClassName("open-form")[9];
var openform10 = document.getElementsByClassName("open-form")[10];
var closeform = document.getElementsByClassName("close-form")[0];

if(openform){
openform.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform1){
openform1.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform2){
openform2.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform3){
openform3.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform4){
openform4.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform5){
openform5.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform6){
openform6.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform7){
openform7.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform8){
openform8.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform9){
openform9.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform10){
openform10.onclick = function() {
  formmodal.style.display = "block";
}
}
if(closeform){
closeform.onclick = function() {
  formmodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == formmodal) {
    formmodal.style.display = "none";
  }
}
</script>    
  <script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
  
   
    $(document).on('ready', function() {
      
      $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
       $(".regular2").slick({
        dots: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2
      });    
    });
</script>
<script>
jQuery(function($) { // DOM ready and $ alias in scope

  /**
   * Option dropdowns. Slide toggle
   */
  $(".option-heading").on('click', function() {
    $(this).toggleClass('is-active').next(".option-content").stop().slideToggle(500);
  });

});

</script>
<!---- Contact Us Form ---->

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "thousandmedia.asia@gmail.com, sherry2.vidatech@gmail.com";
    // $email_to = "wenjie195.vidatech@gmail.com";
    $email_subject = "Contact Form via Thousand Media website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone']) ||
        !isset($_POST['comments'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; // required
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 

 
  if(strlen($comments) < 2) {
    $error_message .= 'The message you entered do not appear to be valid.\n';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you for contacting us. We will be in touch with you very soon.")</script>';
header("Location: https://vidatechft.com/vidatechinc/index.php"); /* Redirect browser */
exit();
?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>