<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>

<title>Login | Thousand Media</title>
<meta property="og:title" content="Login | Thousand Media" />
<link rel="canonical" href="https://thousandmedia.asia/login.php" />
<meta property="og:url" content="https://thousandmedia.asia/login.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />

<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />

<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">

<?php include 'css.php'; ?>
</head>

<body class="body">


<div class="width100 second-row purple-bg2 min-height100vh login-big-div">
	<div class="width100 stars-bg overflow">
        <div class="float-left first-left-div padding-left ow-margin-top50">
        	<a href="index.php">
            	<img src="img/thousand-media/logo-white.png" class="login-logo-img opacity-hover" title="Thousand Media" alt="Thousand Media">
            </a>
            <h1 class="thousand-h1 ow-white-text">Thousand Media</h1>
            <div class="gradient-border first-div-gradient first-border"></div>
            <p class="slogan-p">Creative Anytime</p>
            <div class="clear"></div>
        <form action="utilities/loginFunction.php" method="POST">

            <div class="per-input">
                <p class="input-top-text white-text">Username</p>
                <input class="aidex-input clean" type="text" placeholder="Username" id="username" name="username" required>        
            </div> 

            <div class="per-input">
                <p class="input-top-text white-text">Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="password" name="password" required>
                    <img src="img/thousand-media/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
                </div>
            </div>

            <div class="clear"></div>

            <button class="clean-button clean login-btn pink-button" name="submit">Login</button>
        </form>            
        </div> 
        <div class="float-right first-right-div">
    
            <!--<video poster="img/thousand-media/rocket2.jpg" class="width100 web-rocket" playsinline autoplay muted loop  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
              <source src="img/thousand-media/rocket-animation2.mp4" type="video/mp4"  class="width100 web-rocket" >
              Your browser does not support the video tag.
            </video>-->
           <img src="img/thousand-media/digital-marketing-rocket.png" class="wow pulse animated width100" data-wow-iteration="infinite" style="visibility: visible; animation-duration: 8s; animation-iteration-count: infinite; animation-name: pulse;"  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
    
    
            <!--<img src="img/thousand-media/rocket-mobile4.gif" class="width100 mobile-rocket" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">-->
            <!--<video poster="img/thousand-media/rocket-mobile.jpg" class="width100 mobile-rocket" autoplay loop  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
              <source src="img/thousand-media/rocket-mobile.mp4" type="video/mp4"  class="width100 web-rocket" >
              Your browser does not support the video tag.
            </video>  -->      
        </div>
    </div>
	
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
<script>
function myFunctionA()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>