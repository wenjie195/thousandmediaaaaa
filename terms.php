<!DOCTYPE html>

<!--

Thousand Media reserves all of the rights of this website.

Contents of this webpage can't be seen as they are not meant to be viewed or copied.

Any violator will be prosecuted to the full extent of law and may face civil and criminal
charges and huge monetary fines. Beware!
-->






























































































<html>
<head>
<meta charset="utf-8">
<title>Terms and Conditions | Thousand Media Online advertising Strategy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:url" content="https://thousandmedia.asia/terms.php" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/FBCover.png" />
<meta property="og:title" content="Terms and Conditions | Thousand Media Online advertising Strategy" />
<meta property="og:description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="author" content="Thousand Media">
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, etc">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137506603-1');
</script>
<!-- Global site tag (gtag.js) - Google Ads: 678086507 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-678086507"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-678086507'); </script>
<!-- Event snippet for Website lead conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-678086507/_hlVCMbwucABEOuOq8MC'}); </script>

<link rel="canonical" href="https://thousandmedia.asia/terms.php" />
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<link rel="icon" href="./img/thousand-media/thousand-media-favicon.png"   /> 


</head>

<body class="body">
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<div class="data-top-left-border"></div>

<div class="data-white-bg">
   <div class="data-content" id="style-1">
   
         <h1 class="data-h1">TERMS AND CONDITIONS FOR DIGITAL MARKETING SERVICES BY THOUSAND MEDIA</h1>
         <p class="data-p">
			This Service Agreement (“Agreement”) is hereby entered into between Thousand Media (hereinafter referred to as “the Agency”) and the party set forth in the related order form (“the Client” or “you”) incorporated herein by this reference (together with any subsequent attached forms submitted by Client, the “Order Form”) and applies to the purchase of all Digital Marketing Services (hereinafter collectively referred to as “Services”) ordered by Client.
         </p>
         <h1 class="data-h1">1. DEFINITIONS AND INTERPRETATION</h1>
         <p class="data-p"><b>1.1</b> In these terms and conditions, the following definitions apply unless otherwise stated:</p>       
         <p class="data-p"><b>‘Business Day’</b> means a day (other than a Saturday, Sunday or public holiday) when banks in Malaysia are open for business.</p>  
         <p class="data-p"><b>‘Contract’</b> means the contract between the Agency and the Client for the supply of Services governed by these Terms and the Order.</p>
         <p class="data-p"><b>‘Client’</b> means the individual or business entity who purchases Services from the Agency and whose details are set out in the Order.</p>                     
         <p class="data-p"><b>'Force Majeure Event'</b> means an event beyond the reasonable control of either party, including but not limited to strikes, lock-outs or other industrial disputes, failure of a utility service or transport network, act of God, war, riot, civil commotion, malicious damage, compliance with any law or governmental order, rule, regulation or direction, accident, breakdown of plant or machinery, fire, flood, storm or default of suppliers or subcontractors.</p>
         <p class="data-p"><b>‘Intellectual Property Rights’</b> means all patents, rights to inventions, utility models, copyright and related rights, trademarks, service marks, trade, business and domain names, rights in trade dress or get-up, rights in goodwill or to sue for passing off, unfair competition rights, rights in designs, rights in computer software, database right, topography rights, moral rights, rights in confidential information (including know-how and trade secrets) and any other intellectual property rights, in each case whether registered or unregistered and including all applications for and renewals or extensions of such rights, and all similar or equivalent rights or forms of protection in any part of the world.</p>
         <p class="data-p"><b>‘Order’</b> means the order placed by the Client through counter-signing the Agency’s Quotation form.</p>
		 <p class="data-p"><b>‘Order Form’</b> means a Quotation form counter-signed by the Client which together with these terms and conditions shall form a binding contract.</p>
         <p class="data-p"><b>‘Quotation’</b> means the written quotation prepared by the Agency, which contains its proposals for providing Services to the Clients.</p>
         <p class="data-p"><b>‘Services’</b> means the services the Agency will provide to the Client as specified in the Order.</p>
         <p class="data-p"><b>‘Specification’</b> means the description or specification of the Services in the Order.</p>
         <p class="data-p"><b>‘Terms’</b> means these terms and conditions as updated from time to time by the Agency.</p>
         <p class="data-p"><b>1.2</b> Where these Terms use words in their singular form, they shall also be read to include the plural form of the word and vice versa. Where these Conditions use words which denote a particular gender, they shall be also read to include all genders and vice versa.</p>
		 <p class="data-p"><b>1.3</b> The headings in this document are inserted for convenience only and shall not affect the construction or interpretation of these Terms.</p>
         <h1 class="data-h1">2. TERMS AND CONDITIONS</h1>
         <p class="data-p"><b>2.1</b> These Terms shall apply to all agreements concluded between the Company and the Client to the exclusion of any other terms that the Client seeks to impose or incorporate, or which are implied by trade, custom, practice or course of dealing.</p>
         <p class="data-p"><b>2.2</b> These Terms and the Order may only be varied by express written agreement between the Company and the Client.</p>
         
		 <h1 class="data-h1">3. THE SERVICES</h1>         
         <p class="data-p"><b>3.1</b> We will provide the Services to Client during the Term.</p>
         <p class="data-p"><b>3.2</b> In order for the Agency to provide the level of service proposed we would require input from Client to gather information about Client business, products and services relevant to the Services. Client shall devote such time as is reasonably necessary to provide this information in a timely manner upon request and will make a suitable manager or other senior employee available when reasonably necessary to facilitate the orderly flow of requests and information between the Agency and Client.</p>
		 <p class="data-p"><b>3.3</b> Services for <b>Thousand Arts</b> and <b>Infinity Contents</b> provided are not necessarily include all the listed services in the package column. Our team of 1 project leader and 2 designers will work on your one <b>prioritized request</b> and then move on to the next request repeatedly on business days.</p>
         <p class="data-p"><b>3.4</b> Social Media Management</p>
         <p class="data-p">a) In performing Social Media Services the Agency shall develop a plan intended to improve the visibility of the Clients business profile on the selected social media sites.</p>
         <p class="data-p">b) The Client acknowledges that the Social Media Services will require the Client to enable the agency to make posts on the Clients behalf across the social media sites included with the service in order to influence the visibility and ranking of the Clients profile.</p>
         <p class="data-p">c) The Client shall be responsible for providing the Agency with the necessary login details to make posts and for providing copy and/or information necessary for the agency to ensure posts are fully effective.</p>
         <p class="data-p">d) The Client retains full responsibility for maintaining their social media profile and all links and content contained therein.</p>
         <h1 class="data-h1">4. THE CONTRACT</h1>  
         <p class="data-p"><b>4.1</b> The Order constitutes an offer by the Client to purchase the Services in accordance with these Terms. The Client shall ensure that the terms of the Order and any relevant Specification are complete and accurate.</p>                    
		 <p class="data-p"><b>4.2</b> The Order shall only be deemed to be accepted when the Agency issues a written acceptance of the Order, or when the Agency has started to provide the Services having received the Order, whichever happens first, at which point the Contract shall come into existence.</p>
         <p class="data-p"><b>4.3</b> The Contract constitutes the entire agreement between the Agency to provide the Services to the Client and for the Client to purchase those Services, in accordance with these Terms.</p>
         <p class="data-p"><b>4.4</b> The Client acknowledges that it has not relied on any statement, promise or representation made or given by or on behalf of the Agency which is not set out in the Contract. Any samples, drawings, descriptive matter, or advertising issued by the Agency and any descriptions or illustrations contained in the Agency’s catalogues or brochures are issued or published for the sole purpose of giving an approximate idea of the Services described in them. They shall not form part of the Contract or any other contract between the Agency and the Client for the supply of Services.</p>
         <p class="data-p"><b>4.5</b> A Quotation for the supply of Services given by the Agency shall not constitute an offer. A Quotation shall only be valid for a period of 14 Business Days from its date of issue.</p>
         
         <h1 class="data-h1">5. AGENCY OBLIGATIONS AND WARRANTIES</h1> 
         <p class="data-p"><b>5.1</b> The Agency warrants that it will provide the Services as stipulated in the Order using reasonable care and skill to conform in all material respects with the Specification.</p>
         <p class="data-p"><b>5.2</b> The Agency shall use all reasonable endeavours to meet any performance dates specified in the Order but any such dates shall be estimates only and time shall not be of the essence for the provision of the Services.  The Agency shall not be liable for any delay in delivery of the Services caused by a Force Majeure event or the Client’s failure to provide the Agency with adequate delivery instructions or any other instructions relevant to the supply of the Services.</p>       
         <p class="data-p"><b>5.3</b> The Agency shall have the right to make any changes to the Services which are necessary to comply with any applicable law.</p>
         
         <h1 class="data-h1">6. CLIENT’S OBLIGATIONS AND INDEMNITIES</h1> 
         <p class="data-p"><b>6.1</b> The Client shall provide assistance and technical information to the Agency, as reasonably required by the Agency in sufficient time to facilitate the execution of an Order in accordance with any estimated delivery dates or milestones. The Client shall have sole responsibility for ensuring the accuracy of all information provided to the Agency and warrants and undertakes to the Agency that the Client’s employees assisting in the execution of an Order have the necessary skills and authority.</p>
  		 <p class="data-p"><b>6.2</b> The Client shall be obliged as quickly as possible and within the agreed deadline to comment on and or approve materials provided under the Services, including (without limitation) advertising copy, search terms, and graphic materials submitted by the Agency. In addition, the Client shall be obliged as quickly as possible and within the agreed deadline to implement changes on websites, in IT systems or where it may otherwise be required by the Agency.</p>
         <p class="data-p"><b>6.3</b> The Client shall be obliged to inform the Agency immediately of changes of domain names, websites, technical setup and any other material information regarding the technical infrastructure which may affect the Services delivered by the Agency.</p>
         <p class="data-p"><b>6.4</b> In the event that the Client fails to undertake those acts or provide those materials required under this clause 5 within any agreed deadline (and at least within 15 Business Days of the date requested by the Agency) the Agency shall be entitled to invoice for the Services that it has supplied and the remaining Services specified in the Order whether or not the Agency has been able to deliver them.</p>
         <p class="data-p"><b>6.5</b> As standard across the Services and unless otherwise notified, the Client shall be exclusively responsible for implementing the optimization changes recommended by the Agency. As notified by the Agency, in certain cases for amendments to existing optimizations, the Client shall allow the Agency use of the site’s FTP or content management system’s username and password in order to gain access to add keywords.</p>
         <p class="data-p"><b>6.6</b> The Agency require that prior notice be given for any alterations relating to the Client’s website(s) that may affect the services supplied by the Agency. If alterations are made by the Client or a third party to the Client’s site(s) search engine placements may be affected and the Agency cannot be held responsible.</p>
         
         <h1 class="data-h1">7. PRICES</h1>
         <p class="data-p"><b>7.1</b> Unless otherwise expressly stated, all prices shall be in Ringgit Malaysia (RM). In the event that duties are introduced or changed after the conclusion of an Order, the Agency shall be entitled to adjust the agreed prices accordingly.</p> 
         <p class="data-p"><b>7.2</b> The Client acknowledges that certain Services may involve the licensing of third party Intellectual Property Rights and that the Client may be required to enter into a license directly with such third party. Unless otherwise expressly stated, all prices shall be exclusive of costs for the acquisition of Intellectual Property Rights for materials to be included in marketing materials, including if relevant (but without limitation) pictures and licenses from third party owners and licensors.</p>
         <p class="data-p"><b>7.3</b> The price stated in the Order shall be an estimate based on a qualified estimate of the number of hours required to provide the Services.  This is an estimate only and Services shall be invoiced in accordance with the actual number of hours spent in accordance with the price set out in the Order or Quotation and in the event that the price is not so stipulated, the Client shall be charged at the hourly rate specified in the Agency’s then current price list. The Agency shall be obliged to update the estimate and budgets on an ongoing basis following, among other things, changes made to an Order.</p>
         <p class="data-p"><b>7.4</b> Whilst every effort is made to ensure that costing estimates are accurate, the Agency reserves the right to amend any estimate, should an error or omission have been made.</p>
         
         <h1 class="data-h1">8. PAYMENT TERMS</h1>
         <p class="data-p"><b>8.1</b> The Agency shall invoice the Client monthly, either in advance or following Services delivered.</p>
         <p class="data-p"><b>8.2</b> The Client shall pay each invoice submitted by the Agency within 14 Business Days of the date of the invoice and in cleared funds in accordance with clause 8.3 below.  The invoice number shall be stated on all payments and payment by Cheque and Bank Transfer are accepted.</p> 
         <p class="data-p"><b>8.3</b> Failure to secure final approval from Client on website designs, social media page creation, digital creatives or coop reimbursement on digital ads will not be considered a reason to delay payment beyond the due date.</p>
         <p class="data-p"><b>8.4</b> If the Client subsequently requires the Agency to complete the work within a shorter time frame than specified in the Order, the Agency reserves the right to charge additional monies to prioritize such projects ahead of pre-planned work.</p>
         <p class="data-p"><b>8.5</b> Pricing is based on current scope of work. In the event additional services are required or there is a major change in the scope of work, then Agency reserves the right to adjust the pricing. In the event the travel required to fulfill these services is more than 50 miles, then Agency shall be reimbursed for expenses incurred. All expenses must be pre-approved by the Client.</p>
         
         <h1 class="data-h1">9. ADDITIONAL SERVICES</h1>
         <p class="data-p">Agency may provide additional services including but not limited to training, photography or video services based on the client’s request.</p>
         
         <h1 class="data-h1">10. DELAYS AND COMPLAINTS</h1>
         <p class="data-p"><b>10.1</b> In the event that the Client proves that the Services are delayed or not in accordance with the Contract, the Agency shall be obliged to remedy or redeliver, at its own discretion, without undue delay. In the event that the Services continue to be not in accordance with the Contract after reasonable attempts have been made to remedy this, the Client shall be entitled to cancel the Order.</p>
         <p class="data-p"><b>10.2</b> Complaints concerning delays shall be submitted immediately after the time when the Client became or should have become aware of the matter. If the Client fails to bring the defect (unless by its very nature it is impossible to ascertain within such a period) to the attention of the Agency within 48 hours the Client shall be deemed to have accepted the Services and shall not be entitled to assert remedies based on delays or breach of Contract. </p>
         <p class="data-p"><b>10.3</b> The Client hereby acknowledges that certain Services rely upon goods and/or services being provided by third parties (‘<b>Third Party Services</b>’). The Client acknowledges that the Third Party Services will be governed by that third party’s terms and conditions and that the Agency cannot provide any warranties in respect of the Third Party’s Services and will not be liable to the Client for any delays and/or failings in respect of the same. Providers of Third Party Services may provide their own warranties to the Client and the Client must satisfy itself whether or not such warranties (where given) are acceptable for the Client’s business purposes or risk management policies.</p>
         <p class="data-p"><b>10.4</b> The Agency’s only responsibility in respect of the Third Party Services is to take reasonable care and skill when selecting the providers of the same.</p>
         <p class="data-p"><b>10.5</b> The Client’s exclusive remedies for late delivery or Services not conforming with the Contract are as specified in this clause 10 and, if the remedies set out in these Terms have been exhausted, the Client’s final remedy is limited to cancellation of the Contract and the Agency’s sole liability is to refund any payments for Services not conforming with the Contract, subject to the limitations set out in clause 11 below.</p>
         
         <h1 class="data-h1">11. EXCUSABLE DELAY</h1>
         <p class="data-p">The Agency will not be liable for any damages related to delay or failure to perform due to causes beyond its control, including but not limited to, fire, strike, work stoppage or other labor interruption, freight embargo, terrorism, sabotage, war, civil disturbance, governmental action, rules or regulations, failure of machinery, equipment or information systems, failure of suppliers and digital partners, the elements, flooding, power outages or interruptions or acts of God. The Agency’s inability or failure to perform will not constitute a breach of this Agreement. Performance by the Agency of its obligations under this Agreement will be suspended during this type of delay or failure to perform. The Client may, however, terminate this Agreement if suspension lasts more than thirty (30) days.</p>
         
         <h1 class="data-h1">12. LIABILITY</h1>
         <p class="data-p"><b>12.1</b> Except as expressly stated in this Clause 11, the Agency shall have no liability to the Client for any loss or damage whatsoever arising from or in connection with the provision of the Services or for any claim made against the Client by any third party.</p>
         <p class="data-p"><b>12.2</b> Without prejudice to the generality, the Agency shall have no liability for any losses or damages which may be suffered by the Client whether the same are suffered directly or indirectly or are immediate or consequential which fall into the following categories:</p>
         <p class="data-p">a) Any indirect or consequential loss arising under or in relation to the Contract even though the Agency was aware of the circumstances in which such loss could arise;</p>
         <p class="data-p">b) Loss of profits; loss of anticipated savings; loss of business opportunity or goodwill;</p>
         <p class="data-p">c) Loss of data; and</p>
         <p class="data-p">d) Fraudulent clicks on any of the Client’s accounts managed by the Agency.</p>
         <p class="data-p"><b>12.3</b> To the extent such liability is not excluded, the Agency’s total liability (whether in contract, tort (including negligence or otherwise)) under or in connection with the Contract or based on any claim for indemnity or contribution (including for damage to tangible property) or otherwise will not in any event exceed the total sum invoiced for the Services.</p>
         
         <h1 class="data-h1">13. TERM, TERMINATION AND ASSIGNMENT</h1>
         <p class="data-p"><b>13.1</b> Client agrees the term of this agreement will be for three (3) months. The agreement will automatically renew three (3) months from the date the agreement is signed unless a written notice of either party’s intent to the other that they will not be renewing is provided 30 days prior to the annual renewal date. </p>
         <p class="data-p"><b>13.2</b> Without limiting its other rights or remedies, each party may terminate the Contract with immediate effect by giving written notice to the other party if the other party:</p>
         <p class="data-p">a) (if such breach is remediable) fails to remedy that breach within 30 days of that party being notified in writing of the breach; or</p>
         <p class="data-p">b) becomes or is insolvent or is unable to pay its debts, a petition is presented or meeting convened or resolution passed for winding up the defaulting party or the defaulting party enters into liquidation whether compulsorily or voluntarily or compounds with its creditors generally or has a receiver, administrator, or administrative receiver appointed over all or any part of its assets or the defaulting party ceases to carry on all or a substantial part of its business.</p>
         <p class="data-p"><b>13.3</b> The Agency shall, in addition to all other rights and remedies under these Terms be entitled to terminate this Contract without notice in the event that any of its charges for the Services are not paid in accordance with these Terms.</p>
         <p class="data-p"><b>13.4</b> Upon termination, for whatever reason, the parties shall be obliged to return all materials received from the other pursuant to the Contract without undue delay. If relevant, the Client shall be obliged to remove codes, etc., from websites without undue delay. If the Client fails to do so, the Agency shall be entitled to invoice the Client in line with its then current terms and conditions for subsequent Services without such invoicing amounting to a waiver of the Agency’s right to terminate the Contract.</p>
         <p class="data-p"><b>13.5</b> The Client shall not be permitted to assign or transfer all or any part of its rights or obligations under the Contract and these Terms without the prior written consent of the Agency.</p>
         <p class="data-p"><b>13.6</b> The Agency shall be entitled to assign or subcontract any of its rights or obligations under the Contract and these Terms and the Client acknowledges that certain elements of the Services will be provided by third parties.</p>
         
         <h1 class="data-h1">14. CONTENT</h1>
         <p class="data-p">Client agrees to provide all content required (text, articles, photos, graphics, videos, etc.) for the support of Agency’s efforts.</p>
         
         <h1 class="data-h1">15. ADDITIONAL TOOLS/SOFTWARE & COST</h1>
         <p class="data-p">Agency may require certain tool/software/services to support our efforts. Client agrees to be responsible for all cost one time or monthly for such tools/software. Agency agrees that it will not purchase or subscribe to such software without pre-approval from the Client.</p>
         
         <h1 class="data-h1">16. INTELLECTUAL PROPERTY RIGHTS</h1>
         <p class="data-p"><b>16.1</b> It is the responsibility of the Client to ensure that they have the right to use any Intellectual Property Rights when they provide any text, image or representation (“Materials”) to the Agency for incorporation into the Services and the Client hereby grants or agrees to procure the grant of (as applicable) an irrevocable license to the Agency to use such Materials for the purposes of providing the Services for the duration of the Contract.</p>
         <p class="data-p"><b>16.2</b> The Client shall be responsible for ensuring that the contents of Materials which the Client has contributed or approved are not in contravention of legislation, decency, marketing rules or any other third-party rights. The Agency shall be entitled to reject and delete such material without incurring any liability. In addition, the Agency shall be entitled to cancel the Order.</p>
         <p class="data-p"><b>16.3</b> The Client shall indemnify the Agency against all damages, losses and expenses suffered or incurred by the Agency as a result of the Materials which the Client has contributed or approved being in contravention of legislation, decency, marketing rules or any action that any such Materials infringe any Intellectual Property Rights of a third party.</p>
         <p class="data-p"><b>16.4</b> The parties shall be obliged to notify the other party without undue delay of any claims raised against a party as described above. </p>
         <p class="data-p"><b>16.5</b> Unless expressly stated otherwise in these Terms or in an Order, the Intellectual Property Rights created, developed, subsisting or used in connection with the Services and whether in existence at the date hereof or created in the future shall vest in and be the property of the Agency or the relevant third party from whom the Agency has acquired a right of use with a view to executing the Order. The Client agrees to execute and deliver such documents and perform such acts as may be necessary from time to time to ensure such Intellectual Property Rights vest in the Agency.</p>
         <p class="data-p"><b>16.6</b> If the Agency makes software, scripts, ASP services etc. available to the Client as part of the execution of an Order, the Client shall only acquire a non-exclusive personal nontransferable license to use such material until the Services under this agreement cease.</p>
         <p class="data-p"><b>16.7</b> The Client hereby irrevocably licenses the Agency to use and display the Client’s name, figure, logo etc. as a reference on the Agency’s website, other marketing materials or types of media whilst they are a Client of the Agency and for 18 months after the Contract terminates. The Client agrees to send the Agency it’s most recent logo or figure as and when it is amended from time to time.</p>
         
         <h1 class="data-h1">17. CONFIDENTIALITY AND PERSONAL DATA</h1>
         <p class="data-p"><b>17.1</b> A party <b>(Receiving Party)</b> shall keep in strict confidence all technical or commercial know-how, specifications, inventions, processes or initiatives which are of a confidential nature and have been disclosed to the Receiving Party by the other party <b>(Disclosing Party)</b>, its employees, agents or subcontractors, and any other confidential information concerning the Disclosing Party's business or its products or its services which the Receiving Party may obtain. The Receiving Party shall restrict disclosure of such confidential information to such of its employees, agents or subcontractors as need to know it for the purpose of discharging the Receiving Party's obligations under the Contract, and shall ensure that such employees, agents or subcontractors are subject to obligations of confidentiality corresponding to those which bind the Receiving Party. This clause shall survive termination of the Contract.</p>
         <p class="data-p"><b>17.2</b> During the term of the Contract, the Agency shall take the same care as the Agency uses with it own confidential information, to avoid, without the Client’s consent, the disclosure to any third party (except a subcontractor working on the Services who is subject to similar undertakings of confidentiality) of any of the Client’s business or operational information which the Client has designated as confidential.</p>
         <p class="data-p"><b>17.3</b> The obligation in Clause 16.2 shall not apply to any information which is or becomes publicly available otherwise than through a breach of this agreement, is already or rightly comes into the Agency’s possession without an accompanying obligation of confidence, is independently developed by the Agency, or which the Agency is required to disclose by law.</p>
         <p class="data-p"><b>17.4</b> During the term of the Contract, the Client will not disclose to any persons within its organization that do not have a need to know, or to any third party, any information and non-Client materials provided by the Agency concerning the method or approach the Agency uses in providing the Services.</p>
         <p class="data-p"><b>17.5</b> The Client shall be obliged to indemnify the Agency for any loss, including costs incidental to legal proceedings, suffered by the Agency as a result of the processing of personal data which the Client has contributed being in contravention of marketing law. The parties shall be obliged to notify the other party without undue delay of any claims raised against a party as described in the present clause.</p>
         
         <h1 class="data-h1">18. PERFORMANCE LIABILITY</h1>
         <p class="data-p"><b>18.1</b> WHEREAS, the parties acknowledge that the Internet is neither owned nor controlled by any one entity; therefore, Agency can make no guarantee on the results that may be provided as a result of our work. Agency represents that in good faith it shall make every effort to ensure that the Clients digital marketing is successful and leads are generated as a result of our work.</p>
         <p class="data-p"><b>18.2</b> Agency does not warrant that the functions supplied by its work, web pages, digital marketing, consultation, advice, or work will meet the Client’s requirements or that the operation of the work/deliverables will be uninterrupted or error-free. The entire risk as to the quality and performance of the work and deliverables is with Client.</p>
         <p class="data-p"><b>18.3</b> In no event, will Agency be liable to the Client or any third party for any damages, including any lost profits, lost savings or other incidental, consequential or special damages arising out of the operation of or inability to operate these digital marketing services or website(s), even if Agency has been advised of the possibility of such damages.</p>
         
         <h1 class="data-h1">19. THOUSAND MEDIA VENDORS</h1>
         <p class="data-p">In connection with the Services provided hereunder, Agency has the right to utilize contractors, third-party companies, and vendors selected by Agency at its sole discretion (each a Vendor) to complete or support the completion of the work at hand. Purchased work from Vendors shall be made under such terms Agency deems in its sole discretion as acceptable (Vendor Terms). Agency will be responsible for all cost associated with the Vendor, unless the cost is provided to the Client, and the Client agrees in writing to pay said cost.</p>
         
         <h1 class="data-h1">20. ERRORS</h1>
         <p class="data-p"><b>20.1</b> The Client may not claim a breach, terminate or cancel this Agreement if there are typographical errors, incorrect ad placements, under deliveries, omissions or errors in advertising, social media and website content provided by the Agency. The Agency agrees to take corrective action within 2 business days of notification by the Client, that portion of the advertising, website or social media content which may have been rendered valueless by such typographical errors, incorrect ad placements, under deliveries or omission of copy, unless such error arose due to the error or omission of Client, or after the advertisement, website or social media content had been set and proofed or otherwise confirmed by the Client or the advertisement was submitted after start date.</p>
         <p class="data-p"><b>20.2</b> The Agency will not be liable to Client for any loss or damage that results from a typographical error, incorrect ad placement, under delivery, omission or error related to the products and services it provides. The Client may request a credit or make good pursuant to clause 21 of this agreement.</p>
         
         <h1 class="data-h1">21. CREDITS AND MAKE GOODS</h1>
         <p class="data-p"><b>21.1</b> When there are typographical errors, incorrect ad placements, under deliveries, omissions or errors in advertising, social media and website content the Client may request a credit1 or make good. If Client notifies Agency of errors or omissions after Client approves advertising, social media or website content no credit will be issued and Client assumes all liability as a result of these errors or omissions.</p>
         <p class="data-p"><b>21.2</b> Credits for errors related to website and social media content not to exceed 20% of the Client cost of the product or service according to the rate card. Credit for errors with advertising not to exceed 100% the Client cost of the product or service caused by such error according to the rate card. If there are disputes or discrepancies regarding a credit, the Client may, prior to final resolution, deduct only the amount in question from the charge and pay the balance.
         <br><span style="font-size:13px;">Note: Credits: A request for a credit and any claim for adjustment due to errors must be made within two business days from the date the advertising, social media or website content goes live.</span>
         </p>
         <p class="data-p"><b>21.3</b> Make goods<sup>2</sup> for ad placements that served incorrectly, under delivered or contained incorrect content will be rerun by the Agency and the Client will receive 20% more impressions at no charge as part of this remedy. The number of impressions used to calculate the make good will be based solely on the total number of impressions that served incorrectly and not the entire number of impressions that were contracted.</p>
         
		 <h1 class="data-h1">22. TRANSFER OF THE SITE(S) RECEIVING THE SERVICES</h1>
         <p class="data-p">If the ownership of the Site(s) receiving the Services transfers to another business, Client will remain liable for the Agreed Budget Charges for the remainder of the Term unless we consent to the novation of this Agreement to the new owner. If Client notify the Agency that Client have closed the Site(s) receiving the Services and no longer have an interest in receiving the Services we shall be entitled to elect to terminate this Agreement with immediate effect. If we do so we shall be entitled to issue Client with an invoice for the balance of the Charges that would have fallen due over the remainder of the Initial Term or Additional Term (as applicable).</p>
         
         <h1 class="data-h1">23. ADVERTISING CONTENT</h1>
         <p class="data-p">The Agency may, in its sole discretion, edit, alter, omit, reject or cancel at any time any of Client’s digital advertising products or services to meet industry standards. All digital advertising placements are at the option of the Agency, unless a specific placement is purchased by the Client. Failure to meet placement requests will not constitute cause for adjustment, refund, make good, termination or cancellation of this Agreement. Refer to clause 21 of this agreement for Credits and Make Goods.</p>
         
         <h1 class="data-h1">24. OUR LIABILITY</h1>
         <p class="data-p"><b>24.1</b> We shall not be liable to Client for any damage to software, damage to or loss of data, loss of profit, anticipated profits, revenues, anticipated savings, goodwill or business opportunity, or for any indirect or consequential loss or damage.</p>
         <p class="data-p"><b>24.2</b>
          Our aggregate liability to Client in respect of any claims based on events in any 12-month period arising out of or in connection with this Agreement (or any related contract including website design or hosting), whether in contract or tort (including negligence) or otherwise, shall in no circumstances exceed 100% of the total Charges actually paid by Client to the Agency under this Agreement and any related contract in that 12-month period.<br>
         <span style="font-size:13px;">Note: Make Goods: A request for a ‘make good’ in lieu of a credit may be made by Advertiser for ad placements.</span>
         </p>
         <p class="data-p"><b>24.3</b> Nothing in this Agreement shall operate to exclude or limit our liability for: (a) death or personal injury caused by our negligence; or (b) fraud; or (c) any other liability that cannot be excluded or limited under Malaysia law.</p>
         
         <h1 class="data-h1">25. FORCE MAJEURE</h1>
         <p class="data-p"><b>25.1</b> Neither party shall be held liable for a Force Majeure Event.</p>
         <p class="data-p"><b>25.2</b> If a party believes that a Force Majeure Event has occurred, such party shall immediately inform the other party of the start and end of the Force Majeure Event.</p>
         <p class="data-p"><b>25.3</b> Notwithstanding the other provisions of the present Terms, each party shall be entitled to terminate the Contract without liability to the other by written notice to the other party in the event that the performance of the Contract is impeded for more than 6 months due to a Force Majeure Event. </p>
         
         <h1 class="data-h1">26. BANKRUPTCY</h1>
         <p class="data-p">In the event of either party becoming bankrupt, insolvent, or committing any act of Bankruptcy or insolvency or going into liquidation or in the event that a Receiver or Administrator or Administrative Receiver is appointed in respect of any of its assets then the other party shall have the right to terminate this contract with immediate effect with no further liability (save for any accrued rights of action or damages due to them on the date of termination) to the other party.</p>
         
         <h1 class="data-h1">27. MISCELLANEOUS</h1>
         <p class="data-p"><b>27.1</b> The Agency reserves the right to modify or discontinue, temporarily or permanently, the Services with or without notice to the Client and the Agency shall not be liable to the Client or any third party for any modification to or discontinuance of these Services save for the return of any prepaid sums in connection with the provision of the Services which are subsequently not provided.</p>
         <p class="data-p"><b>27.2</b> The Agency shall be free to provide its Services to third parties whether during or following the provision of the Services to the Client.</p>
         <p class="data-p"><b>27.3</b> During the term of the Contract, the Client agrees not to employ or engage or offer to employ or engage anyone designated by the Agency to work on the Services.</p>
         <p class="data-p"><b>27.4</b> The failure of either party to enforce or to exercise at any time or for any period of time any right pursuant to these Terms does not constitute, and shall not be construed as, a waiver of such terms or rights and shall in no way affect that party’s right later to enforce or to exercise it.</p>
         <p class="data-p"><b>27.5</b> If any term of these Terms is found illegal, invalid or unenforceable under any applicable law, such term shall, insofar as it is severable from the remaining Terms, be deemed omitted from these Terms and shall in no way affect the legality, validity or enforceability of the remaining Terms which shall continue in full force and effect and be binding on the parties to the Contract.</p>
         <p class="data-p"><b>27.6</b> Any valid alteration to or variation of these Terms must be in writing signed on behalf of each of the parties by duly authorized officers.</p>
         <p class="data-p"><b>27.7</b> A person who is not a party to the Contract shall not have any rights under or in connection with it.</p>
         <p class="data-p"><b>27.8</b> All notices must be in writing to Thousand Media, or such address as is advised by the Agency.</p>
         
         <h1 class="data-h1">28. FAX/SCANNED COPY OF SIGNATURE</h1>         
         <p class="data-p">Both parties agree that a faxed or scanned copy of the signed document by either or both parties shall be considered acceptable, legal, and legally binding.</p>
         
         <h1 class="data-h1">29. NO WAIVER</h1>
         <p class="data-p">The Agency’s failure to insist upon the performance by the Client of any term or condition of this Agreement or to exercise any of the Agency’s rights under this Agreement on one or more occasions will not result in a waiver or loss of the Agency’s right to require future performance of these terms and conditions or to exercise its rights in the future.</p>
         
         <h1 class="data-h1">30. GOVERNING LAW AND JURISDICTION</h1>
         <p class="data-p">This agreement and any dispute or claim arising out of or in connection with it or its formation including non-contractual disputes or claims shall be governed by Malaysia Law and the parties submit to the exclusive jurisdiction of the courts of Malaysia to settle any such dispute or claim.</p>
         
         <h1 class="data-h1">31. AGREEMENT REVISIONS</h1>
         <p class="data-p">Revisions to this Agreement will be considered agreed to by Agency and Client when requested changes have been signed by both parties.</p>
         
         <p class="data-p" style="margin-bottom:0 !important;">© <?php echo $time;?> Thousand Media, All Rights Reserved.</p>                   
   </div>
   
<div class="data-bottom-right-border"></div>   
   
   
</div>
 
 
 
 
 <!----- Style ---->
<style>
*{
	box-sizing:border-box;}
.body{
    font-family: 'Raleway', sans-serif;
	margin:0 auto;
	border:0;
}
a:hover{
	opacity:0.85;
	transition:.15s ease-in-out;}
.data-white-bg{
	background-image:url(https://thousandmedia.asia/img/thousand-media/terms.jpg);
	background-position:center;
	background-size:cover;
	width:100%;
	height:100vh;
	overflow:hidden;}
.data-content {
    width: 60%;
    margin-left: 20%;
    background-color: rgba(255, 255, 255, 0.9);
	padding: 40px 100px;
	height: 70vh;
    margin-top: 15vh;
	overflow-y: scroll;
	border-radius: 10px;
}
.data-top-left-border {
    position: absolute;
    width: 12%;
    height: 18vh;
    border-left: 2px solid white;
    border-top: 2px solid white;
    margin-left: 10%;
    margin-top: 5vh;
	border-radius: 10px;
}
.data-bottom-right-border {
    width: 12%;
    height: 18vh;
    border-bottom: 2px solid white;
    border-right: 2px solid white;
    position: absolute;
    right: 10%;
    bottom: 5vh;
	border-radius: 10px;
}
.data-h1 {
    color: #717171;
    border-top: 1px solid #ececec;
    padding-top: 35px;
    padding-bottom: 35px;
    border-bottom: 1px solid #ececec;
    font-weight: 300 !important;
	letter-spacing: 1.5px;
}
.data-p {
    margin-top: 40px;
    color: #777777;
    font-size: 17px;
    line-height: 27px;
	margin-bottom:40px;
}
.data-p2 {

    color: #777777;
    font-size: 17px;
    line-height: 27px;

}
.data-ul {
    padding-left: 2%;}
.data-li{
    color: #777777;
    font-size: 17px;
    line-height: 27px;
	margin-bottom: 5px;
	}
.data-span {
    font-size: 14px;}
.no-border{
	border:0 !important;}
.data-title {
    font-size: 21px;
    margin-bottom: 0;
    padding-bottom: 0;
	font-weight:bold;
}

/*
 *  STYLE 1
 */

#style-1::-webkit-scrollbar-track
{
	border-radius: 10px;
	background-color: #f1f1f1;
}

#style-1::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
	border-radius: 25px;
}

#style-1::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	background-color: #b3b3b3;
}


@media all and (max-width: 1200px){
.data-content {
    width: 70%;
    margin-left: 15%;
    height: 80vh;
    margin-top: 10vh;}
.data-h1{
	font-size: 24px;
	padding-top: 25px;
    padding-bottom: 25px;	
}
.data-p{
	font-size: 15.5px;	
}			
	}
@media all and (max-width: 800px){
.data-h1 {
    padding-top: 20px;
    padding-bottom: 20px;
    font-size: 22px;
}
.data-p {
    margin-top: 30px;
    font-size: 15px;
    line-height: 24px;
    margin-bottom: 30px;
}
.data-p2{
	font-size: 15px;
    line-height: 24px;}
.data-li {
    font-size: 15px;
    line-height: 24px;
    margin-bottom: 5px;
}

.data-content{
    background-color: rgba(255, 255, 255, 0.90);}
}
@media all and (max-width: 600px){
.data-top-left-border{
	margin-left:5%;}	
.data-bottom-right-border{
	right: 5%;
	}
.data-content {
    width: 80%;
    margin-left: 10%;
	padding: 20px 50px;
	text-align: justify;}
.data-h1{
	font-size: 24px;
	text-align:left;}
.data-title{
	text-align:left;}	
	
	}
@media all and (max-width: 500px){
.data-p{
	font-size: 14px;	
}
.data-h1 {
    font-size: 21px;}	
}
@media all and (max-width: 450px){	
.data-top-left-border, .data-bottom-right-border{
	display:none;}
.data-content {
    width: 90%;
    margin-left: 5%;
    padding: 20px 30px;
	height: 90vh;
    margin-top: 5vh;}
}
@media all and (max-width: 400px){
.data-p {
    font-size: 13.5px;
}
.data-h1 {
    font-size: 19px;
    padding-top: 15px;
    padding-bottom: 15px;}	
}
</style>
<!---script--->



         
</body>
</html>
