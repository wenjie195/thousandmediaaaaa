<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Users.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUsers($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$articleRows = getArticles($conn," WHERE display = 'Yes' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>

<title>Admin Article Dashboard | Thousand Media</title>

<meta property="og:url" content="https://thousandmedia.asia/adminArticlesDashboard.php" />
<link rel="canonical" href="https://thousandmedia.asia/adminArticlesDashboard.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Malaysia Graphic Design & Social Media Marketing Agency Blog | Thousand Media Online Advertising Strategy" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />

<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">

<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height100vh overflow menu-distance">


	    <div class="scroll-div margin-top30">
            <table class="table-css">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>ID</th>
                        <th>Author</th>
                        <th>Article</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = connDB();
                    if($articleRows)
                    {
                        for($cnt = 0;$cnt < count($articleRows) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo $articleRows[$cnt]->getDateCreated();;?></td>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $articleRows[$cnt]->getAuthorName();;?></td>
                            <td><?php echo $articleRows[$cnt]->getTitle();;?></td>                       
                         


                            <td>
                                <form action="adminEditArticles.php" method="POST">
                                    <button class="clean edit-btn" type="submit" name="news_uid" value="<?php echo $articleRows[$cnt]->getUid();?>">
                                    Edit
                                    </button>
                                </form>
                            </td>
                            <td>
                                <?php $display = $articleRows[$cnt]->getDisplay();
                                if($display == 'Yes')
                                {
                                ?>
                                    <form action="utilities/adminDeleteArticleFunction.php" method="POST">
                                        <input class="aidex-input clean" type="hidden" value="<?php echo $articleRows[$cnt]->getUid();?>" id="article_uid" name="article_uid">
                                        <button class="clean edit-btn"  name="Submit">Delete</button>
                                    </form>
                                <?php
                                }
                                ?>
                            </td>   
                        </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    $conn->close();
                    ?>
                </tbody>
            </table>
		</div>


</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Article Added !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to add new article !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "The TITLE or LINK of the article has been used <br> Please get a new !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Article Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update article !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Article Deleted !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to delete article !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>                 