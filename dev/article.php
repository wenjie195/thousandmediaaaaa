<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Business Blog | Thousand Media</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:url" content="https://thousandmedia.asia/business.php" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Business Blog| Thousand Media" />
<meta property="og:description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="author" content="Thousand Media">
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, blog, article, tips, news, etc">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137506603-1');
</script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
    </script>
    <script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
    </script>
   <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '390782708409897'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=390782708409897&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
  <link rel="canonical" href="https://thousandmedia.asia/business.php" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/thousandmedia-style.css">
  <link rel="icon" href="./img/thousand-media/thousand-media-favicon.png"   />
</head>

<body class="body" >

<div id="overlay">
 <div class="center-food"><img src="img/thousand-media/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>


<div class="blog-bg-div article-bg">
	<?php include 'blog-header.php'; ?>
    <div class="article-padding width100">
        <div class="width100 same-padding">
            <h1 class="thousand-h1 blog-title article-title">Plan Your Future With Us</h1>
            <div class="gradient-border first-div-gradient blog-gradient"></div>
			<a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 article-cat">Business</div></a>
            <div class="width100 article-content">
            	<img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
            	<p class="article-date">22 Feb 2020</p>
                <p class="article-p">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisi facilisi.
                    <br><br>
                    Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
                    <br><br>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper susciis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                    <br><br>
                    Lorem ipsum doloons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.duis dolore te feugait facilisi.
                </p>
            </div>
            <h3 class="thousand-h1 blog-title3">Latest Article</h3>
            <a href="blog.php"><div class="white-button">See More</div></a>
            <div class="clear"></div>
            <div class="three-in-one-row">
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100 big-web" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                            <img src="img/thousand-media/transparent.png" class="width100 height100 big-mobile" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100 big-web" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                            <img src="img/thousand-media/transparent.png" class="width100 height100 big-mobile" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                	</div>
                	<div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>    
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100 big-web" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                            <img src="img/thousand-media/transparent.png" class="width100 height100 big-mobile" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                	</div>
                	<div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>                         
          </div>
          <div class="clear"></div>
          <h3 class="thousand-h1 blog-title3 float-none">Categories</h3>          
          <div class="width100">
                <a href="marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 margin-left-0">Marketing</div></a>
                <a href="lifestyle.php" class="hover-a"><div class="catagory-div green-div blog-aa2">Lifestyle</div></a>
                <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2">Business</div></a>
                <a href="promotion.php" class="hover-a"><div class="caagory-div red-div blog-aa2 article-second-last">Promotion</div></a>
                <a href="bookmark.php" class="hover-a"><div class="catagory-div orange-div blog-aa2 article-last-cat">Bookmark</div></a> 
          </div>
          
          
        </div>
</div>    
</div>          
            
            
            <!--- -->

    
    


<div class="footer-div width100 same-padding">
    <p class="footer-p white-text text-center">© 2019 Thousand Media, All Rights Reserved.</p>
</div>
<?php include 'js.php'; ?>
<style>
.menu-bg{
	background:white !important;
	background-color:white !important;}
</style>

</body>
</html>