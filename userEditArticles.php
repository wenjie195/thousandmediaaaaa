<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Users.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
// require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUsers($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
<?php include 'meta.php'; ?>

<title>Edit Article | Thousand Media</title>
<meta property="og:title" content="Malaysia Graphic Design & Social Media Marketing Agency Blog | Thousand Media Online Advertising Strategy" />
<link rel="canonical" href="https://thousandmedia.asia/userEditArticles.php" />
<meta property="og:url" content="https://thousandmedia.asia/userEditArticles.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />

<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />

<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">

<?php include 'css.php'; ?>
</head>

<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height100vh overflow menu-distance padding-bottom50">

    <!-- <h1 class="h1-title">Edit Company</h1> -->

    <h1 class="details-h1" onclick="goBack()">
    	<a class="opacity-hover">
    		<img src="img/thousand-media/back.png" class="back-btn2" alt="back" title="back">
            
                Edit Article
        </a>
    </h1>

    <div class="clear"></div>

    <?php
    if(isset($_POST['news_uid']))
    {
    $conn = connDB();
    $articlesDetails = getArticles($conn,"WHERE uid = ? ", array("uid") ,array($_POST['news_uid']),"s");
    ?>

    <!-- <form action="utilities/adminEditArticleFunction.php" method="POST" enctype="multipart/form-data"> -->
    <form action="utilities/userEditArticleFunction.php" method="POST" enctype="multipart/form-data">

            <div class="width100 overflow">
                <p class="input-top-text">Title*</p>
                <input class="clean blog-input" type="text" value="<?php echo $articlesDetails[0]->getTitle();?>" placeholder="Title" name="update_title" id="update_title" required>      
            </div>

            <div class="width100 overflow">
                <p class="input-top-text">Article Slug/Link* (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use -  <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-referlink" alt="Click Me!" title="Click Me!"></p>
                <input class="clean blog-input" type="text" value="<?php echo $articlesDetails[0]->getArticleLink();?>" placeholder="article-title" name="update_article_link" id="update_article_link" required>              	
            </div>

            <div class="clear"></div>

            <div class="width100 overflow">
                <p class="input-top-text">Keyword (Use Coma , to Separate Each Keyword, Avoid"') <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-referkeyword" alt="Click Me!" title="Click Me!"></p>
                <textarea class="clean blog-input keyword-input" type="text" placeholder="Keyword" name="update_keyword_one" id="update_keyword_one" required><?php echo $articlesDetails[0]->getKeywordOne();?></textarea>  	
            </div>        

            <div class="clear"></div>  

            <div class="width100 overflow">
                <p class="input-top-text">Upload Cover Photo (Less Than 1.8mb)</p>
                <p class="input-top-text">View Current Cover Photo : <a href="uploadsArticle/<?php echo $articlesDetails[0]->getTitleCover();?>" class="blue-to-orange" target="_blank"><?php echo $articlesDetails[0]->getTitleCover();?></a></p>
                <input id="file-upload" type="file" name="file_one" id="file_one" accept="image/*">    
                <input class="clean blog-input" type="hidden" value="<?php echo $articlesDetails[0]->getTitleCover();?>" name="ori_cover_pic" id="ori_cover_pic">
            </div>        

            <div class="clear"></div>

            <div class="width100 overflow ow-margin-top20">
                <p class="input-top-text">Photo Source/Credit (Optional)</p>
                <input class="clean blog-input" type="text" value="<?php echo $articlesDetails[0]->getImgCoverSrc();?>" placeholder="Photo Source" name="update_cover_photo_source" id="update_cover_photo_source">              	
            </div>  

            <div class="clear"></div>

            <div class="width100 overflow">
                <!-- <p class="input-top-p admin-top-p">Article Summary/Description (Won't Appear inside the Main Content, Avoid "') <img src="img/attention2.png" class="attention-png opacity-hover open-desc" alt="Click Me!" title="Click Me!"></p> -->
                <p class="input-top-text">Article Summary/Description (Won't Appear inside the Main Content, Avoid "') <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-referde" alt="Click Me!" title="Click Me!"></p>
                <textarea class="clean blog-input desc-textarea" type="text" placeholder="Article Summary" name="update_descprition" id="update_descprition" required><?php echo $articlesDetails[0]->getDescription();?></textarea>  	
            </div>        

            <div class="clear"></div>      

            <div class="form-group publish-border input-div width100 overflow">
                <p class="input-top-text">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial)<img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-refertexteditor" alt="Click Me!" title="Click Me!"></p>
                <textarea name="editor" id="editor" rows="10" cols="80"  class="clean blog-input" ><?php echo $articlesDetails[0]->getParagraphOne();?></textarea>
            </div>   

            <input class="clean blog-input"  type="hidden" value="<?php echo $articlesDetails[0]->getUid();?>" id="article_uid" name="article_uid" readonly> 

            <div class="width100 overflow text-center ow-margin-top50">     
                <button class="clean-button clean login-btn pink-button mobile-width100" name="submit">Submit</button>
            </div>

    </form>

    <?php
    }
    ?>

</div>

<?php include 'js.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

<script>
    CKEDITOR.replace('editor');
</script>
<script>


var referlinkmodal = document.getElementById("referlink-modal");
var refertexteditormodal = document.getElementById("refertexteditor-modal");
var referdemodal = document.getElementById("referde-modal");
var referkeywordmodal = document.getElementById("referkeyword-modal");


var openreferlink = document.getElementsByClassName("open-referlink")[0];
var openrefertexteditor = document.getElementsByClassName("open-refertexteditor")[0];
var openreferde = document.getElementsByClassName("open-referde")[0];
var openreferkeyword = document.getElementsByClassName("open-referkeyword")[0];




var closereferlink = document.getElementsByClassName("close-referlink")[0];
var closerefer1 = document.getElementsByClassName("close-refer")[1];
var closereferlink1 = document.getElementsByClassName("close-referlink")[1];
var closerefertexteditor = document.getElementsByClassName("close-refertexteditor")[0];
var closerefertexteditor1 = document.getElementsByClassName("close-refertexteditor")[1];
var closereferde = document.getElementsByClassName("close-referde")[0];
var closereferde1 = document.getElementsByClassName("close-referde")[1];
var closereferkeyword = document.getElementsByClassName("close-referkeyword")[0];
var closereferkeyword1 = document.getElementsByClassName("close-referkeyword")[1];

if(openreferlink){
openreferlink.onclick = function() {
  referlinkmodal.style.display = "block";
}
}
if(openrefertexteditor){
openrefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "block";
}
}
if(openreferde){
openreferde.onclick = function() {
  referdemodal.style.display = "block";
}
}
if(openreferkeyword){
openreferkeyword.onclick = function() {
  referkeywordmodal.style.display = "block";
}
}




if(closereferlink){
  closereferlink.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferlink1){
  closereferlink1.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferkeyword){
  closereferkeyword.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closereferkeyword1){
  closereferkeyword1.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closerefertexteditor){
  closerefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closerefertexteditor1){
  closerefertexteditor1.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closereferde){
  closereferde.onclick = function() {
  referdemodal.style.display = "none";
}
}
if(closereferde1){
  closereferde1.onclick = function() {
  referdemodal.style.display = "none";
}
}


window.onclick = function(event) {

    
  
  if (event.target == referlinkmodal) {
    referlinkmodal.style.display = "none";
  } 
  if (event.target == refertexteditormodal) {
    refertexteditormodal.style.display = "none";
  }
  if (event.target == referdemodal) {
    referdemodal.style.display = "none";
  }        
  if (event.target == referkeywordmodal) {
    referkeywordmodal.style.display = "none";
  }  
    
}
</script>
</body>
</html>