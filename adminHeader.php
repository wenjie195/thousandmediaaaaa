<header id="header" class="header header--fixed same-padding header1 menu-white blog-menu admin-header" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<a href="index.php">
            <img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media">
            <!--<img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">-->
            </a>
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="addArticle.php" class="black-text menu-padding red-hover">Write</a>
            <a href="adminArticlesDashboard.php" class="black-text menu-padding red-hover">Edit</a>
        	<a href="digital-marketing-blog-news.php" target="_blank" class="black-text menu-padding red-hover">View</a>
            <a href="adminAddNewUser.php" class="black-text menu-padding red-hover">Add User</a>
            <a href="logout.php" class="black-text menu-padding red-hover">Logout</a>           
            <!--
 			<div class="dropdown">
            <a class="black-text menu-padding red-hover pointer hover1">Blog <img src="img/thousand-media/arrow.png" class="dropdown-png hover1a"><img src="img/thousand-media/dropdown-pink.png" class="dropdown-png hover1b"></a>
                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="blog.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Latest</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-content-marketing.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Marketing</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-graphic-design.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Design</a></p>
                	</div>            
            
            </div>-->            
        
        </div>
	</div>

</header>