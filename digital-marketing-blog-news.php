<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 50 ");
$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>

<title>Latest Articles | Thousand Media Online Advertising Strategy & Digital Marketing</title>

<meta property="og:url" content="https://thousandmedia.asia/digital-marketing-blog-news.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Latest Articles | Thousand Media Online Advertising Strategy & Digital Marketing" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />

<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">
<link rel="canonical" href="https://thousandmedia.asia/digital-marketing-blog-news.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>
<?php include 'blogHeader.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?> 

<div class="width100 same-padding min-height100vh overflow menu-distance" id="app">
<h1 class="pink-title">Latest Articles</h1>

    <div class="clear"></div>
    <div class="blog-padding-big-div">
    <?php
    $conn = connDB();
    if($articles)
    {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
        
        <a href='articleDetails.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
            <div class="two-div-width shadow-white-box blog-box opacity-hover">
            	<a href='articleDetails.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                <div class="left-img-div2 progressive">
                	
                        <img src="img/cover-tiny.jpg" class="preview lazy width100" data-src="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" src="img/thousand-media/cover-tiny.jpg"  alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>" />
                    
                   
                </div>
				</a>
                <a href='articleDetails.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
                <div class="right-content-div3">
                    <h3 class="article-title text-overflow">
                        <?php echo $articles[$cnt]->getTitle();?>
                    </h3>
                    <p class="date-p">
                        <?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?>
                    </p>
                    <p class="right-content-p">
                        <?php echo $description = $articles[$cnt]->getDescription();?>
                    </p>
                </div>
                </a>
            </div>
        </a>
        

        <?php
        }
        ?>
    <?php
    }
    $conn->close();
    ?>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>