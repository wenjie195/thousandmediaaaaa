<?php
class Articles {
    /* Member variables */
    var $id,$uid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$keywordOne,$keywordTwo,$description,$titleCover,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,$paragraphThree,
            $imageThree,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$author,$type,$display,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getAuthorUid()
    {
        return $this->authorUid;
    }

    /**
     * @param mixed $authorUid
     */
    public function setAuthorUid($authorUid)
    {
        $this->authorUid = $authorUid;
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param mixed $authorName
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @param mixed $seoTitle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return mixed
     */
    public function getArticleLink()
    {
        return $this->articleLink;
    }

    /**
     * @param mixed $articleLink
     */
    public function setArticleLink($articleLink)
    {
        $this->articleLink = $articleLink;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getKeywordTwo()
    {
        return $this->keywordTwo;
    }

    /**
     * @param mixed $keywordTwo
     */
    public function setKeywordTwo($keywordTwo)
    {
        $this->keywordTwo = $keywordTwo;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitleCover()
    {
        return $this->titleCover;
    }

    /**
     * @param mixed $titleCover
     */
    public function setTitleCover($titleCover)
    {
        $this->titleCover = $titleCover;
    }

    /**
     * @return mixed
     */
    public function getParagraphOne()
    {
        return $this->paragraphOne;
    }

    /**
     * @param mixed $paragraphOne
     */
    public function setParagraphOne($paragraphOne)
    {
        $this->paragraphOne = $paragraphOne;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getParagraphTwo()
    {
        return $this->paragraphTwo;
    }

    /**
     * @param mixed $paragraphTwo
     */
    public function setParagraphTwo($paragraphTwo)
    {
        $this->paragraphTwo = $paragraphTwo;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getParagraphThree()
    {
        return $this->paragraphThree;
    }

    /**
     * @param mixed $paragraphThree
     */
    public function setParagraphThree($paragraphThree)
    {
        $this->paragraphThree = $paragraphThree;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getImgCoverSrc()
    {
        return $this->imgCoverSrc;
    }

    /**
     * @param mixed $imgCoverSrc
     */
    public function setImgCoverSrc($imgCoverSrc)
    {
        $this->imgCoverSrc = $imgCoverSrc;
    }

    /**
     * @return mixed
     */
    public function getImgOneSrc()
    {
        return $this->imgOneSrc;
    }

    /**
     * @param mixed $imgOneSrc
     */
    public function setImgOneSrc($imgOneSrc)
    {
        $this->imgOneSrc = $imgOneSrc;
    }

    /**
     * @return mixed
     */
    public function getImgTwoSrc()
    {
        return $this->imgTwoSrc;
    }

    /**
     * @param mixed $imgTwoSrc
     */
    public function setImgTwoSrc($imgTwoSrc)
    {
        $this->imgTwoSrc = $imgTwoSrc;
    }

    /**
     * @return mixed
     */
    public function getImgThreeSrc()
    {
        return $this->imgThreeSrc;
    }

    /**
     * @param mixed $imgThreeSrc
     */
    public function setImgThreeSrc($imgThreeSrc)
    {
        $this->imgThreeSrc = $imgThreeSrc;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getArticles($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","author_uid","author_name","title","seo_title","article_link","keyword_one","keyword_two","description","title_cover","paragraph_one",
                            "image_one","paragraph_two","image_two","paragraph_three","image_three","img_cover_source","img_one_source","img_two_source","img_three_source",
                            "author","type","display","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"articles");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$keywordOne,$keywordTwo,$description,$titleCover,$paragraphOne,$imageOne,$paragraphTwo,
                                $imageTwo,$paragraphThree,$imageThree,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$author,$type,$display,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Articles;
            $class->setId($id);
            $class->setUid($uid);
            $class->setAuthorUid($authorUid);
            $class->setAuthorName($authorName);
            $class->setTitle($title);
            $class->setSeoTitle($seoTitle);
            $class->setArticleLink($articleLink);
            $class->setKeywordOne($keywordOne);
            $class->setKeywordTwo($keywordTwo);

            $class->setDescription($description);

            $class->setTitleCover($titleCover);
            $class->setParagraphOne($paragraphOne);
            $class->setImageOne($imageOne);
            $class->setParagraphTwo($paragraphTwo);
            $class->setImageTwo($imageTwo);
            $class->setParagraphThree($paragraphThree);
            $class->setImageThree($imageThree);
            $class->setImgCoverSrc($imgCoverSrc);
            $class->setImgOneSrc($imgOneSrc);
            $class->setImgTwoSrc($imgTwoSrc);
            $class->setImgThreeSrc($imgThreeSrc);
            $class->setAuthor($author);
            $class->setType($type);
            $class->setDisplay($display);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
