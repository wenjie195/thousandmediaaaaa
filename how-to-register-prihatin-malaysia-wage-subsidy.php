<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<title>4 Steps to Register RM600 Wage Subsidy | Thousand Media</title>
<meta property="og:url" content="https://thousandmedia.asia/how-to-register-prihatin-malaysia-wage-subsidy.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/rm600-wage-subsidy.jpg" />
<meta property="og:title" content="4 Steps to Register RM600 Wage Subsidy | Thousand Media" />
<meta property="og:description" content="Private sector employers can register for RM600 wage subsidy per worker for a duration of 3 months offered by the Social Security Organisation (Socso) due to Covid-19 outbreak. This is to help the employers in surviving their business yet retaining their employees." />
<meta name="description" content="Private sector employers can register for RM600 wage subsidy per worker for a duration of 3 months offered by the Social Security Organisation (Socso) due to Covid-19 outbreak. This is to help the employers in surviving their business yet retaining their employees." />
<meta name="keywords" content="Prihatin, how to register, malaysia, covid-19, bantuan, socso, Employment Insurance Scheme (EIS), Graphic Design, 平面设计, 设计, typography, photography, demonstration, illustration,  Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, blog, article, tips, news, etc">
<link rel="canonical" href="https://thousandmedia.asia/how-to-register-prihatin-malaysia-wage-subsidy.php" />
<?php include 'css.php'; ?>
</head>

<body class="body" >

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>

	<?php include 'blog-header.php'; ?>
<div class="blog-bg-div article-bg">

    <div class="article-padding width100 menu-distance">
        <div class="width100 same-padding">
            <h1 class="thousand-h1 blog-title article-title">4 Steps to Register RM600 Wage Subsidy</h1>
            <div class="gradient-border first-div-gradient blog-gradient"></div>
            <p class="author-name">by <b class="author-name weight-800">Raymond Ho</b></p>
			<a href="malaysia-penang-content-marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 article-cat blog-article-cat">Content Marketing</div></a>
            <div class="width100 article-content">
            	<img src="img/thousand-media/rm600-wage-subsidy.jpg" class="width100" alt="4 Steps to Register RM600 Wage Subsidy" title="4 Steps to Register RM600 Wage Subsidy">
            	<p class="article-date">1 Apr 2020</p>
                <h3 class="title-h3">4 Steps to Register RM600 Wage Subsidy</h3>
                <p class="article-p">
                    Today is the date (1/4/2020) opened for registration of Wage Subsidy Programme. Private sector employers can register for RM600 wage subsidy per worker for a duration of 3 months offered by the Social Security Organisation (Socso) due to Covid-19 outbreak. This is to help the employers in surviving their business yet retaining their employees.
                <br><br>
                </p>
				<img src="img/thousand-media/how-to-register-prihatin-malaysia-wage-subsidy1.jpg" class="width100" alt="4 Steps to Register RM600 Wage Subsidy" title="4 Steps to Register RM600 Wage Subsidy">                
                <p class="article-p">
					To qualify, the company and employee must meet these requirements:

				<br><br>
                <ul class="article-ul">
                	<li>The subsidy is only for employees earning below RM4,000</li>
                    <li>The employees have been registered and contributed to the Employment Insurance Scheme (EIS)</li>
                    <li>The company has seen a 50% decrease in income since 1st January 2020</li>
                </ul>
        
                </p>
                
                <h3 class="title-h3">Steps for Registration:</h3>
                <p class="article-p">
                	<b>Step 1:</b> Visit 
                    <a href="https://www.perkeso.gov.my/index.php/ms/program-subsidi-upah" target="_blank" class="blog-link-a">https://www.perkeso.gov.my/index.php/ms/program-subsidi-upah</a>(Under Wage Subsidy Programme) and click “Pendaftaran” (Register)
				<br><br>
                </p>
                <img src="img/thousand-media/how-to-register-prihatin-malaysia-wage-subsidy4.jpg" class="width100" alt="4 Steps to Register RM600 Wage Subsidy" title="4 Steps to Register RM600 Wage Subsidy">
                <p class="article-p">
                	<b>Step 2:</b> Fill up your company details and information ( Company registration number, bank account, address, reasons why income decreased, number of employees and etc)
                </p>
                <br><br>
                <img src="img/thousand-media/how-to-register-prihatin-malaysia-wage-subsidy6.jpg" class="width100" alt="4 Steps to Register RM600 Wage Subsidy" title="4 Steps to Register RM600 Wage Subsidy">
               
                <p class="article-p">
                	<b>Step 3:</b> Upload supportive documents like “Akuan Pengisytiharan PSU50” (Declaration form), Employee List Form, Copy of SSM/ ROS / ROB documents and etc.
                    <br><br>
                </p>
                
                
                <img src="img/thousand-media/how-to-register-prihatin-malaysia-wage-subsidy5.jpg" class="width100" alt="4 Steps to Register RM600 Wage Subsidy" title="4 Steps to Register RM600 Wage Subsidy">          
                <p class="article-p">
                    *You can download the Declaration form and Employee List Form from the PERKESO Website.
					<br><br>
                    <b>Step 4:</b>Click send. You are all set!
                    <br><br>
                    Employers are not allowed to do the three things once they signed up for the Wage Subsidy Programme.
                    <br><br>
                    <ul class="article-ul">
                        <li>They cannot lay off workers.</li>
                        <li>They cannot force workers to take unpaid leave.</li>
                        <li>They cannot cut their workers wages.</li>
                    </ul> 
                    <br><br>
                    *Employers who did one of the three things can be fined RM10,000 for each offence committed.
                    <br><br>
                    Employees can make a complaint with the Human Resource Ministry if they feel their employee has gone against the guideline given at <a href="tel:+60388865192" class="blog-link-a">+603-8886 5192</a>. 
                    <br><br>
                    It will potentially be a short wait for the payment, as the subsidy will be transferred to the employer’s account within 7 days of submitting the application. The amount given will also be based on the number of eligible employees, as not every employee would qualify for the subsidy.			
                    <br><br>
                    Take care and stay safe Malaysian. We can go through this hard time together !
                    <br><br>
                    <b>Thousand Media,<br>
					Your Trusted Creative Agency</b>
                  	<br><br>
                    Check Out Our <b>FREE DESIGN & CASH REBATE</b> Promotions (Extended to 14/4/2020):
                </p> 
                <a href="https://www.facebook.com/thousandmedia/photos/a.2078544398919865/2758058047635160/?type=3&theater" target="_blank" class="opacity-hover">
                <img src="img/thousand-media/grab-free-design.jpg" class="width100" alt="Free Design" title="Free Design">
               </a>
            </div>
            
            <div class="share-div">
            	<h3 class="thousand-h1 blog-title3 share-title">Share</h3>	
                <div class="clear"></div>
            	<script async src="https://static.addtoany.com/menu/page.js"></script>

                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        <a class="a2a_button_copy_link"></a>
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_linkedin"></a>
                        <a class="a2a_button_blogger"></a>
                        <a class="a2a_button_facebook_messenger"></a>
                        <a class="a2a_button_whatsapp"></a>
                        <a class="a2a_button_wechat"></a>
                        <a class="a2a_button_line"></a>
                        <a class="a2a_button_telegram"></a>
                        <!--<a class="a2a_button_print"></a>-->
                        </div> 
                               
            </div>
            <div class="clear"></div>
            <div class="share-div comment-div">
            	<h3 class="thousand-h1 blog-title3 share-title">Comment</h3>	
                <div class="clear"></div>            
            	<div id="disqus_thread"></div>
				<script>
                
                /**
                *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                var disqus_config = function () {
                this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                */
                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://thousandmedia.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>                 <script id="dsq-count-scr" src="//thousandmedia.disqus.com/count.js" async></script>         
                            
                            
                            
                             <a href="https://thousandmedia.asia/how-to-register-prihatin-malaysia-wage-subsidy.php#disqus_thread" class="live-hide"></a>
            </div>            
            
            
            
            
            
            
            
            <h3 class="thousand-h1 blog-title3">Latest Article</h3>
            <a href="blog.php"><div class="white-button">See More</div></a>
            <div class="clear"></div>
        	<div class="width100 white-box2">
            	<div class="three-div-image planning-bg">
                	<a href="steps-to-convert-business-to-online-business.php" class="hover-opacity">
                    	<img src="img/thousand-media/online-business0.jpg" class="width100" alt="How to Convert Business to Online Business" title="How to Convert Business to Online Business">
                   </a>
                </div>
                <div class="big-blog-content">
                    <h2 class="blog-title-h2 text-overflow"><a href="steps-to-convert-business-to-online-business.php" class="hover-turn-red">5 Steps to Convert Your Brick and Mortar Business to Online Business During This COVID-19 Crisis</a></h2>
                    <p class="blog-date"><a href="steps-to-convert-business-to-online-business.php" class="hover-opacity">8 Apr 2020</a></p>
                    <p class="blog-desc"><a href="steps-to-convert-business-to-online-business.php" class="hover-opacity">Competitor research - Look into competitors and understand how their business works, Understand competitor marketing strategy and emulate it. How? Use Google to search for your niche product and from there look into competitor websites and gather information on the website such as the website layout design, promotion method and social media. 
</a></p>
                    <a href="malaysia-penang-content-marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 blog-article-cat">Marketing</div></a>
                </div>
            </div>            
            
            
            
            	<div class="width100 white-box2">
                    <div class="three-div-image planning-bg">
                        <a href="copywriting-content-marketing-content-strategy.php" class="hover-opacity">
                            <img src="img/thousand-media/content-marketing.jpg" class="width100" alt="Content Marketing" title="Content Marketing">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="copywriting-content-marketing-content-strategy.php" class="hover-turn-red">Copywriting vs content marketing vs content strategy. Which is the best to make or increase your brand exposure?</a></h2>
                        <p class="blog-date"><a href="copywriting-content-marketing-content-strategy.php" class="hover-opacity">19 Feb 2020</a></p>
                        <p class="blog-desc"><a href="copywriting-content-marketing-content-strategy.php" class="hover-opacity">Let’s review the definition of copywriting, content marketing and strategy first before proceeding to the detailed information. The word “content” is a tricky concept because there are several formats and types that constitute content.</a></p>
                        <a href="malaysia-penang-content-marketing.php" class="hover-a"><div class="catagory-div blue-div blog-aa2 blog-article-cat width-auto">Marketing</div></a>
                    </div>
                </div>            
            	<div class="width100 white-box2">
                    <div class="three-div-image planning-bg">
                        <a href="definition-of-graphic-design-designer.php" class="hover-opacity">
                            <img src="img/thousand-media/graphic-design.jpg" class="width100" alt="Graphic Design" title="Graphic Design">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="definition-of-graphic-design-designer.php" class="hover-turn-red">What is the definition of graphic design, and what are the best things to take into consideration to become a designer.</a></h2>
                        <p class="blog-date"><a href="definition-of-graphic-design-designer.php" class="hover-opacity">19 Feb 2020</a></p>
                        <p class="blog-desc"><a href="definition-of-graphic-design-designer.php" class="hover-opacity">Graphic design is the specialty of making visual content to impart messages. Graphic Design is the process of communication with people and solving problems with the support of typography, photography, demonstration and illustration.</a></p>
                        <a href="malaysia-penang-graphic-design.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat width-auto">Graphic Design</div></a>
                    </div>
                </div>             
            
            <!--<div class="three-in-one-row">
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                    </div>
                    <div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                	</div>
                	<div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>    
            	<div class="three-one-small-div">
                    <div class="three-div-image planning-bg">
                        <a href="article.php" class="hover-opacity">
                            <img src="img/thousand-media/planning.jpg" class="width100" alt="Plan Your Future With Us" title="Plan Your Future With Us">
                       </a>
                	</div>
                	<div class="three-div-content">
                        <h2 class="blog-title-h2 text-overflow"><a href="article.php" class="hover-turn-red">Plan Your Future With Us</a></h2>
                        <p class="blog-date"><a href="article.php" class="hover-opacity">22 Feb 2020</a></p>
                        <p class="blog-desc"><a href="article.php" class="hover-opacity">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euis.Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</a></p>
                        <a href="business.php" class="hover-a"><div class="catagory-div purple-div blog-aa2 blog-article-cat">Business</div></a>
                    </div>
                </div>                         
          </div>-->
          <div class="clear"></div>
          
          
        </div>
</div>    
</div>          


    
    


<?php include 'js.php'; ?>

</body>
</html>