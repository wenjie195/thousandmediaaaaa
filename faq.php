<!DOCTYPE html>

<!--

Thousand Media reserves all of the rights of this website.

Contents of this webpage can't be seen as they are not meant to be viewed or copied.

Any violator will be prosecuted to the full extent of law and may face civil and criminal
charges and huge monetary fines. Beware!
-->






























































































<html>
<head>
<meta charset="utf-8">
<title>FAQ | Thousand Media Online advertising Strategy</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:url" content="https://thousandmedia.asia/faq.php" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/FBCover.png" />
<meta property="og:title" content="FAQ | Thousand Media Online advertising Strategy" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="author" content="Thousand Media">
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, etc">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137506603-1');
</script>
<!-- Global site tag (gtag.js) - Google Ads: 678086507 --> <script async src="https://www.googletagmanager.com/gtag/js?id=AW-678086507"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-678086507'); </script>
<!-- Event snippet for Website lead conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-678086507/_hlVCMbwucABEOuOq8MC'}); </script>

<link rel="canonical" href="https://thousandmedia.asia/terms.php" />
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<link rel="icon" href="./img/thousand-media/thousand-media-favicon.png"   /> 


</head>

<body class="body">
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<div class="data-top-left-border"></div>

<div class="data-white-bg">
   <div class="data-content" id="style-1">
   
         <h1 class="data-h1">FAQ</h1>
         <h1 class="data-h1">1. Does it work for my business?</h1>
         <p class="data-p">Our marketing packs are bundled together in a way to resolve business owners’ biggest challenges and to overcome it through deep market research, marketing plan and much more.</p>       
         <h1 class="data-h1">2. What if it doesn’t work?</h1>
         <p class="data-p">Feel free to unsubscribe to us, if it doesn’t work for your business.</p>       
         <h1 class="data-h1">3. The price is too expensive?</h1>
         <p class="data-p">Price represents the quality and professionalism. Thousand Media does not offer products but solutions to your business problems.</p>            
         <h1 class="data-h1">4. How does the queued based for unlimited package works?</h1>
         <p class="data-p">Queued based means the client submits requests and our assigned team will work on it one request and proceed to next requests once it’s done. Requests will be done around 1 to 2 reviews per business day.</p>            
         <h1 class="data-h1">5. How does this going to help my business?</h1>
         <p class="data-p">Businesses without online markets nowadays are dying. However, entering the online market is difficult challenges and without proper skills, businesses might lose money more than profit. Thus, Thousand Media is a platform to help your online business.</p>            
         <h1 class="data-h1">6. Why do I need SEO?</h1>
         <p class="data-p">It is now estimated that approximately 80%-90% of customers will check online reviews before making a purchase. Search Engine Optimization gives your company a better chance of being seen by potential customers. According to Forbes, almost 60% of small businesses are now investing in some form of online marketing. So the odds are that your competitors are using SEO.</p>            
         <h1 class="data-h1">7. Why should I hire an agency rather than in-house?</h1>
         <p class="data-p">It is better to let the professionals handle it. You and your staff are experts at what you do and we are the experts of digital marketing. The primary benefit of using a digital marketing agency is experience and resources. A digital marketing agency like Thousand Media has access to knowledge and resources that you may not.</p>            
         
         
         
         
         <p class="data-p" style="margin-bottom:0 !important;">© <?php echo $time;?> Thousand Media, All Rights Reserved.</p>                   
   </div>
   
<div class="data-bottom-right-border"></div>   
   
   
</div>
 
 
 
 
 <!----- Style ---->
<style>
*{
	box-sizing:border-box;}
.body{
    font-family: 'Raleway', sans-serif;
	margin:0 auto;
	border:0;
}
a:hover{
	opacity:0.85;
	transition:.15s ease-in-out;}
.data-white-bg{
	background-image:url(https://thousandmedia.asia/img/thousand-media/faq4.jpg);
	background-position:center;
	background-size:cover;
	width:100%;
	height:100vh;
	overflow:hidden;}
.data-content {
    width: 60%;
    margin-left: 20%;
    background-color: rgba(255, 255, 255, 0.9);
	padding: 40px 100px;
	height: 70vh;
    margin-top: 15vh;
	overflow-y: scroll;
	border-radius: 10px;
}
.data-top-left-border {
    position: absolute;
    width: 12%;
    height: 18vh;
    border-left: 2px solid white;
    border-top: 2px solid white;
    margin-left: 10%;
    margin-top: 5vh;
	border-radius: 10px;
}
.data-bottom-right-border {
    width: 12%;
    height: 18vh;
    border-bottom: 2px solid white;
    border-right: 2px solid white;
    position: absolute;
    right: 10%;
    bottom: 5vh;
	border-radius: 10px;
}
.data-h1 {
    color: #717171;
    border-top: 1px solid #ececec;
    padding-top: 35px;
    padding-bottom: 35px;
    border-bottom: 1px solid #ececec;
    font-weight: 300 !important;
	letter-spacing: 1.5px;
}
.data-p {
    margin-top: 40px;
    color: #777777;
    font-size: 17px;
    line-height: 27px;
	margin-bottom:40px;
}
.data-p2 {

    color: #777777;
    font-size: 17px;
    line-height: 27px;

}
.data-ul {
    padding-left: 2%;}
.data-li{
    color: #777777;
    font-size: 17px;
    line-height: 27px;
	margin-bottom: 5px;
	}
.data-span {
    font-size: 14px;}
.no-border{
	border:0 !important;}
.data-title {
    font-size: 21px;
    margin-bottom: 0;
    padding-bottom: 0;
	font-weight:bold;
}

/*
 *  STYLE 1
 */

#style-1::-webkit-scrollbar-track
{
	border-radius: 10px;
	background-color: #f1f1f1;
}

#style-1::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
	border-radius: 25px;
}

#style-1::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	background-color: #b3b3b3;
}


@media all and (max-width: 1200px){
.data-content {
    width: 70%;
    margin-left: 15%;
    height: 80vh;
    margin-top: 10vh;}
.data-h1{
	font-size: 24px;
	padding-top: 25px;
    padding-bottom: 25px;	
}
.data-p{
	font-size: 15.5px;	
}			
	}
@media all and (max-width: 800px){
.data-h1 {
    padding-top: 20px;
    padding-bottom: 20px;
    font-size: 22px;
}
.data-p {
    margin-top: 30px;
    font-size: 15px;
    line-height: 24px;
    margin-bottom: 30px;
}
.data-p2{
	font-size: 15px;
    line-height: 24px;}
.data-li {
    font-size: 15px;
    line-height: 24px;
    margin-bottom: 5px;
}

.data-content{
    background-color: rgba(255, 255, 255, 0.90);}
}
@media all and (max-width: 600px){
.data-top-left-border{
	margin-left:5%;}	
.data-bottom-right-border{
	right: 5%;
	}
.data-content {
    width: 80%;
    margin-left: 10%;
	padding: 20px 50px;
	text-align: justify;}
.data-h1{
	font-size: 24px;
	text-align:left;}
.data-title{
	text-align:left;}	
	
	}
@media all and (max-width: 500px){
.data-p{
	font-size: 14px;	
}
.data-h1 {
    font-size: 21px;}	
}
@media all and (max-width: 450px){	
.data-top-left-border, .data-bottom-right-border{
	display:none;}
.data-content {
    width: 90%;
    margin-left: 5%;
    padding: 20px 30px;
	height: 90vh;
    margin-top: 5vh;}
}
@media all and (max-width: 400px){
.data-p {
    font-size: 13.5px;
}
.data-h1 {
    font-size: 19px;
    padding-top: 15px;
    padding-bottom: 15px;}	
}
</style>
<!---script--->



         
</body>
</html>
