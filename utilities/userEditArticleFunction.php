<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/Users.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];
$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $articleUid = rewrite($_POST['article_uid']);

    $title = rewrite($_POST['update_title']);
    // $articleLink = rewrite($_POST['update_article_link']);


    $stringOne = ($_POST['update_article_link']);
    $preArticleLink = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
    $articleLink = str_replace(' ', '-', trim($preArticleLink));

    $str = str_replace( array(' ',','), '', $title);
    $strkeywordOne = str_replace( array(','), '', $articleLink);
    $AddToEnd = "-";
    $seoTitle = $str.$AddToEnd.$strkeywordOne;

    $keywordOne = rewrite($_POST['update_keyword_one']);

    // $title = rewrite($_POST['title']);
    // $title = rewrite($_POST['old_cover_pic']);

    $oriCoverPic = rewrite($_POST["ori_cover_pic"]);
    $newFileOne = $_FILES['file_one']['name'];
    if($newFileOne == '')
    {
        $file = $oriCoverPic;
    }
    else
    {
        $file = $oriFileOne;
        $file = $timestamp.$_FILES['file_one']['name'];
        $target_dir = "../uploadsArticle/";
        $target_file = $target_dir . basename($_FILES["file_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif","pdf");
        if( in_array($imageFileType,$extensions_arr) )
        {
            move_uploaded_file($_FILES['file_one']['tmp_name'],$target_dir.$file);
        }
    }

    $coverPhotoSource = rewrite($_POST['update_cover_photo_source']);
    $description = ($_POST['update_descprition']);
    $paragraphOne = ($_POST['editor']);

    // $display = "Yes";
    $display = "Pending";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $articleDetails = getArticles($conn," uid = ? ",array("uid"),array($articleUid),"s");   

    if(!$articleDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($title)
        {
            array_push($tableName,"title");
            array_push($tableValue,$title);
            $stringType .=  "s";
        }
        if($articleLink)
        {
            array_push($tableName,"article_link");
            array_push($tableValue,$articleLink);
            $stringType .=  "s";
        }
        if($seoTitle)
        {
            array_push($tableName,"seo_title");
            array_push($tableValue,$seoTitle);
            $stringType .=  "s";
        }
        if($keywordOne)
        {
            array_push($tableName,"keyword_one");
            array_push($tableValue,$keywordOne);
            $stringType .=  "s";
        }
        if($file)
        {
            array_push($tableName,"title_cover");
            array_push($tableValue,$file);
            $stringType .=  "s";
        }
        if($coverPhotoSource)
        {
            array_push($tableName,"img_cover_source");
            array_push($tableValue,$coverPhotoSource);
            $stringType .=  "s";
        }
        if($description)
        {
            array_push($tableName,"description");
            array_push($tableValue,$description);
            $stringType .=  "s";
        }
        if($paragraphOne)
        {
            array_push($tableName,"paragraph_one");
            array_push($tableValue,$paragraphOne);
            $stringType .=  "s";
        }
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }

        array_push($tableValue,$articleUid);
        $stringType .=  "s";
        $articleUpdated = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($articleUpdated)
        {
            header('Location: ../userArticlesDashboard.php');
        }
        else
        {
            echo "<script>alert('FAIL !!');window.location='../userArticlesDashboard.php'</script>";  
        }
    }
    else
    {
        echo "<script>alert('ERROR !!');window.location='../userArticlesDashboard.php'</script>";  
    }

}
else 
{
    header('Location: ../index.php');
}
?>