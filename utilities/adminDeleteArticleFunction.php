<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/Users.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $articleUid = rewrite($_POST["article_uid"]);
    $display = "Deleted";
    $type = "4";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $articleDetails = getArticles($conn," uid = ? ",array("uid"),array($articleUid),"s");   

    if(!$articleDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }

        array_push($tableValue,$articleUid);
        $stringType .=  "s";
        $statusUpdated = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($statusUpdated)
        {
            $_SESSION['messageType'] = 3;
            header('Location: ../adminArticlesDashboard.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 3;
            header('Location: ../adminArticlesDashboard.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 3;
        header('Location: ../adminArticlesDashboard.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>