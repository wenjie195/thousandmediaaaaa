<?php
if(isset($_SESSION['uid']))
{
?>
    <?php
    if($_SESSION['usertype'] == 0)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-white blog-menu admin-header" role="banner">
            <div class="big-container-size hidden-padding">
                <div class="left-logo-div float-left hidden-logo-padding">
                    <a href="index.php">
                    <img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media">
                    <!--<img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">-->
                    </a>
                </div>
                
                <div class="right-menu-div float-right" id="top-menu">
                    <a href="addArticle.php" class="black-text menu-padding red-hover">Write</a>
                    <a href="adminArticlesDashboard.php" class="black-text menu-padding red-hover">Edit</a>
                    <a href="digital-marketing-blog-news.php" target="_blank" class="black-text menu-padding red-hover">View</a>
                    <a href="adminAddNewUser.php" class="black-text menu-padding red-hover">Add User</a>
                    <a href="logout.php" class="black-text menu-padding red-hover">Logout</a>           
                    <!--
                    <div class="dropdown">
                    <a class="black-text menu-padding red-hover pointer hover1">Blog <img src="img/thousand-media/arrow.png" class="dropdown-png hover1a"><img src="img/thousand-media/dropdown-pink.png" class="dropdown-png hover1b"></a>
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="blog.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Latest</a></p>
                                <p class="dropdown-p"><a href="malaysia-penang-content-marketing.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Marketing</a></p>
                                <p class="dropdown-p"><a href="malaysia-penang-graphic-design.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Design</a></p>
                            </div>            
                    
                    </div>-->            
                
                </div>
            </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-white blog-menu admin-header" role="banner">
            <div class="big-container-size hidden-padding">
                <div class="left-logo-div float-left hidden-logo-padding">
                    <a href="index.php">
                        <img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media">
                    </a>
                </div>
                
                <div class="right-menu-div float-right" id="top-menu">
                    <a href="userAddArticle.php" class="black-text menu-padding red-hover">Write</a>
                    <a href="userArticlesDashboard.php" class="black-text menu-padding red-hover">Edit</a>
                    <a href="digital-marketing-blog-news.php" target="_blank" class="black-text menu-padding red-hover">View</a>
                    <a href="logout.php" class="black-text menu-padding red-hover">Logout</a>                           
                </div>
            </div>

        </header>

    <?php
    }
    ?>
<?php
}
else
{
?>

    <header id="header" class="header header--fixed same-padding header1 menu-white blog-menu" role="banner">
        <div class="big-container-size hidden-padding">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php">
                <img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media">
                </a>
            </div>
            
            <div class="right-menu-div float-right" id="top-menu">
                <a href="index.php" class="black-text menu-padding red-hover">Thousand Media</a>
                 
                <div class="dropdown">
                <a class="black-text menu-padding red-hover pointer hover1">Blog <img src="img/thousand-media/arrow.png" class="dropdown-png hover1a"><img src="img/thousand-media/dropdown-pink.png" class="dropdown-png hover1b"></a>
                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="blog.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Latest</a></p>
                            <p class="dropdown-p"><a href="malaysia-penang-content-marketing.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Marketing</a></p>
                            <p class="dropdown-p"><a href="malaysia-penang-graphic-design.php"  class="black-text menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Design</a></p>
                        </div>            
                
                </div>            

                <!-- Mobile View-->
                <a href="index.php" class="black-text menu-padding red-hover2">
                    <img src="img/thousand-media/about-us2.png" class="menu-img" alt="About Thousand Media" title="About Thousand Media">
                </a>     
                <a href="malaysia-penang-content-marketing.php" class="black-text menu-padding red-hover2">
                    <img src="img/thousand-media/content-marketing.png" class="menu-img" alt="Content Marketing" title="Content Marketing">            
                </a>
                <a href="malaysia-penang-graphic-design.php" class="black-text red-hover2">
                    <img src="img/thousand-media/graphic-design.png" class="menu-img" alt="Graphic Design" title="Graphic Design">            
                </a>    
                        
            </div>
        </div>

    </header>

<?php
}
?>