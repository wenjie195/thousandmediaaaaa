<?php
function rewrite($info){
    $info = stripslashes($info);
    $info = trim($info);
    $info = htmlspecialchars($info,ENT_QUOTES,'ISO-8859-1', true);
    return $info;
}