<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Digital Marketing | Thousand Media</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:url" content="https://thousandmedia.asia/" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Digital Marketing | Thousand Media" />
<meta property="og:description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="author" content="Thousand Media">
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, etc">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137506603-1');
</script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
    </script>
    <script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
    </script>
   <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '390782708409897'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=390782708409897&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
  <link rel="canonical" href="https://thousandmedia.asia/" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/thousandmedia-style.css">
  <link rel="icon" href="./img/thousand-media/thousand-media-favicon.png"   />
</head>

<body class="body" >

<div id="overlay">
 <div class="center-food"><img src="img/thousand-media/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>
<header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<img src="img/thousand-media/logo.png" class="logo-img web-logo" alt="Thousand Media" title="Thousand Media">
            <img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo" alt="Thousand Media" title="Thousand Media">
           
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="#whatyouneed" class="white-text menu-padding red-hover">What You Need</a>
            <a href="#promotion" class="white-text menu-padding red-hover">Promotion</a>
            <!--<a href="#packages" class="white-text menu-padding red-hover">Packages</a>-->
            <a href="#services" class="white-text menu-padding red-hover">Services</a>
            <a href="#contact" class="white-text menu-padding red-hover">Contact Us</a>
            <a  class="white-text menu-padding red-hover open-register pointer">Register</a>
            <a  class="white-text red-hover open-login pointer">Login</a>
		<!-- Mobile View-->
            <a href="#whatyouneed" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-14.png" class="menu-img" alt="Your Need" title="Your Need">
            </a>
            <a href="#promotion" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-13.png" class="menu-img" alt="Promotion" title="Promotion">
            </a>
            <!--<a href="#packages" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-12.png" class="menu-img" alt="Packages" title="Packages">            
            </a>-->
            <a href="#services" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Services" title="Services">            
            </a>
            <a href="#contact" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-10.png" class="menu-img" alt="Contact" title="Contact">            
            </a>
            <a class="white-text menu-padding red-hover2 open-register pointer">
            	<img src="img/thousand-media/menu-icon-15.png" class="menu-img" alt="Register" title="Register">            
            </a>
            <a class="white-text red-hover2 open-login pointer">
            	<img src="img/thousand-media/menu-icon-16.png" class="menu-img" alt="Login" title="Login">            
            </a>            
        </div>
	</div>

</header>
<div class="width100 second-row" id="whatyouneed">
    <div class="float-left first-left-div padding-left">
        <h1 class="thousand-h1">Thousand Media</h1>
        <div class="gradient-border first-div-gradient"></div>
        <p class="thousand-p first-div-p">The journey of a thousand miles begins with your first step with us at <b class="black-text first-div-b">Thousand Media</b>.<br>
        	
        </p>
        <p class="check-p"><a href="blog.php" class="red-link check-link">Check out our blog</a></p>
    </div> 
	<div class="float-right first-right-div">
    	<!--<video poster="img/thousand-media/rocket2.jpg" class="width100 web-rocket" playsinline autoplay muted loop  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
          <source src="img/thousand-media/rocket-animation2.mp4" type="video/mp4"  class="width100 web-rocket" >
          Your browser does not support the video tag.
        </video>-->
       <img src="img/thousand-media/rocket3.png" class="width100 web-rocket" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
       <img src="img/thousand-media/rocket-mobile7.jpg" class="width100 mobile-rocket" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
       <!--<img src="img/thousand-media/rocket4.gif" class="width100 web-rocket2" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">

        <img src="img/thousand-media/rocket-mobile4.gif" class="width100 mobile-rocket" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">-->
    </div>

</div>
<div class="clear"></div>
<div class="third-row width100 same-padding overflow">
	<div class="third-row-first-div float-left">
    	<img src="img/thousand-media/service.png" class="width100" alt="Digital Marketing Services" title="Digital Marketing Services">
    </div>
	<div class="third-row-second-div float-right">
    	<p class="thousand-p2"><b class="black-text">Thousand Media</b> is your quick and effective <b class="red-text">marketing planning buddy</b> to guide you to online and offline business success with continued support of strategies and <b class="red-text">creativity</b> with minimal investment.</p>
        <div class="gradient-border second-gradient"></div>
    </div>    
</div>
<div class="clear"></div>


<div class="four-row-div same-padding width100">
	<img src="img/thousand-media/pink-left.png" class="left-img">
    <img src="img/thousand-media/pink-right.png" class="right-img">
    <p class="text-center img-p"><img src="img/thousand-media/idea.png" class="bulb-img" alt="Creaivity" title="Creaivity"></p>
    <p class="thousand-p2 row-4-p">Let’s dive into our creativity and increase your exposure by 10x in the market.</p>
    <p class="arrow-p"><img src="img/thousand-media/arrow.png" class="text-center arrow-img"></p>
    
</div> 
<div class="clear"></div>
<div id="promotion">
    <div class="four-row-div same-padding width100 ow-padding-bottom">
        <img src="img/thousand-media/pink-left2.png" class="left-img second-left-img">
        <img src="img/thousand-media/pink-right2.png" class="right-img second-right-img">
        <p class="text-center img-p">
        <img src="img/thousand-media/package4.gif" class="box-img" alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
            <!--<video poster="img/thousand-media/promotion.jpg" class="box-img" autoplay loop  alt="The journey begins with Thousand Media" title="The journey begins with Thousand Media">
          		<source src="img/thousand-media/promotion.mp4" type="video/mp4"  class="box-img" >
          		Your browser does not support the video tag.
        	</video>-->
       
        </p>
        <h2 class="thousand-h2 text-center gift-h2">Marketing Basic Pack now only RM1,000/month*!</h2><p class="small">*3-month Commitment</p>
        <div class="gradient-border margin-auto"></div>
        <p class="thousand-p3 text-center">Get a head start with us to discover your potential.</p>    
    </div> 
    <div class="clear"></div>
    
    <div class="four-div-container width100 same-padding">
        <div class="four-div float-left text-center">
            <img src="img/thousand-media/marketing-strategy.png" class="four-image" alt="Marketing Strategy" title="Marketing Strategy">
            <p class="four-box-p">Marketing Strategy</p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div">
            <img src="img/thousand-media/seo-guideline.png" class="four-image" alt="SEO Guideline" title="SEO Guideline">
            <p class="four-box-p">SEO Guideline</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/branding.png" class="four-image" alt="Basic Branding" title="Basic Branding">
            <p class="four-box-p">Basic Branding</p>
        </div>
        
        <div class="four-div float-left text-center four-second-div">
            <img src="img/thousand-media/artwork-design.png" class="four-image" alt="Artwork Design x2/Month" title="Artwork Design x2/Month">
            <p class="four-box-p">Artwork Design x2/Month</p>
        </div>      	
    </div>
    <div class="four-div-container width100 same-padding">
        <div class="four-div float-left text-center">
            <img src="img/thousand-media/copywriting.png" class="four-image" alt="Social Media Caption Enhancement x2/Month" title="Social Media Caption Enhancement x2/Month">
            <p class="four-box-p special-m-height">Social Media Caption Enhancement x2/Month</p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div">
            <img src="img/thousand-media/facebook.png" class="four-image" alt="Social Media Publishing x2/Month" title="Social Media Publishing x2/Month">
            <p class="four-box-p special-m-height">Social Media Publishing x2/Month</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/email-blasting.png" class="four-image" alt="EDM Blasting x1/Month" title="EDM Blasting x1/Month">
            <p class="four-box-p">EDM Blasting x1/Month</p>
        </div>
        
        <div class="four-div float-left text-center four-second-div">
            <img src="img/thousand-media/marketing-monthly-report.png" class="four-image" alt="Monthly Marketing Report" title="Monthly Marketing Report">
            <p class="four-box-p">Monthly Marketing Report</p>
        </div>      	
    </div>
    
    <div class="clear"></div>
    <div class="five-row width100 same-padding overflow">
            <h3 class="thousand-h3 text-center">Available for Add-on*</h3>
            <div class="gradient-border margin-auto"></div>
    </div>
    <div class="four-div-container width100 same-padding add-padding-top">
        <div class="four-div float-left text-center empty-div">
        </div>
        <div class="four-div float-left text-center middle-4-div four-second-div ow-margin1">
            <img src="img/thousand-media/logo-design.png" class="four-image" alt="Logo Crafting" title="Logo Crafting">
            <p class="four-box-p">Logo Crafting</p>
        </div>
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/digital-campaign.png" class="four-image" alt="Digital Campaign Planning" title="Digital Campaign Planning">
            <p class="four-box-p">Recommended Digital Campaign Planning</p>
        </div>
        <div class="clear"></div>
        <div class="width100 text-center">
        	<button class="subscribe-button pointer clean separate-bottom">Subscribe Now</button>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div id="services">
    <div class="four-row-div same-padding width100 ow-padding-bottom">
        <h2 class="thousand-h2 text-center">Marketing Packages</h2>
        <div class="gradient-border margin-auto"></div>
        <p class="thousand-p3 text-center">Dive right into one of our amazing marketing solutions</p>    
    </div> 
    <div class="clear"></div>
    <div class="five-row-div same-padding width100">
        <div class="package-box">
            <div class="color-box red-box">
                <h3 class="thousand-h3a white-text text-center">Basic Pack</h3>
            </div>
            <div class="box-content-div mh">
                <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/fee.png" class="width100 box-icon" alt="Fee"  title="Fee">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">RM 1,800 Monthly</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/duration.png" class="width100 box-icon"  alt="12-Month Commitment"  title="12-Month Commitment">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">12-Month Commitment</p>
                    </div>
               </div>    
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/how-much.png" class="width100 box-icon" alt="setup fee" title="setup fee">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">One time setup fee: RM1,000</p>
                    </div>
               </div>
               <div class="one-row width100"><div class="gradient-border ow-gradient-width"></div></div>                               
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/marketing-strategy2.png" class="width100 box-icon" alt="Marketing Strategy" title="Marketing Strategy">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Marketing strategy</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/seo.png" class="width100 box-icon" alt="SEO guideline" title="SEO guideline">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">SEO guideline</p>
                    </div>
               </div>           
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/branding-logo.png" class="width100 box-icon" alt="Basic Branding Guideline" title="Basic Branding Guideline">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Basic branding guideline</p>
                    </div>
               </div>            
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/logo-design2.png" class="width100 box-icon" alt="Artwork Design" title="Artwork Design">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Artwork design x2/month</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/social-media-caption.png" class="width100 box-icon" alt="Social Media Caption Enhancement" title="Social Media Caption Enhancement">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media caption enhancement x2/month</p>
                    </div>
               </div>           
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/instagram2.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media publishing x2/month</p>
                    </div>
               </div> 
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/email.png" class="width100 box-icon" alt="EDM Blasting" title="EDM Blasting">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">EDM blasting x1/month</p>
                    </div>
               </div> 
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/monthly-marketing-report.png" class="width100 box-icon" alt="Monthly Marketing Report" title="Monthly Marketing Report">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Monthly marketing report</p>
                    </div>
               </div>            
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/event-planner.png" class="width100 box-icon" alt="Digital Campaign Planning" title="Digital Campaign Planning">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Digital campaign planning x1/year</p>
                    </div>
               </div>           
                                                
            </div>
            <div class="width100 text-center">
        		<button class="subscribe-button pointer clean separate-bottom subscribe-button80">Subscribe</button>
        	</div>
        </div>
        <div class="package-box middle-package-box">
            <div class="color-box orange-box">
                <h3 class="thousand-h3a white-text text-center">Essential Marketing</h3>
            </div> 
            <div class="box-content-div mh">
                <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/fee.png" class="width100 box-icon" alt="Fee"  title="Fee">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">RM 5,450 Monthly</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/duration.png" class="width100 box-icon"  alt="12-Month Commitment"  title="12-Month Commitment">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">12-Month Commitment</p>
                    </div>
               </div>    
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/how-much.png" class="width100 box-icon" alt="setup fee" title="setup fee">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">One time setup fee: RM1,000</p>
                    </div>
               </div>
               <div class="one-row width100"><div class="gradient-border ow-gradient-width"></div></div>                               
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/manager.png" class="width100 box-icon" alt="Dedicated account manager" title="Dedicated account manager">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Dedicated account manager</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/review.png" class="width100 box-icon" alt="Comprehensive brand review" title="Comprehensive brand review">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Comprehensive brand review</p>
                    </div>
               </div>           
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/marketing-strategy-planning.png" class="width100 box-icon" alt="Marketing strategy and planning" title="Marketing strategy and planning">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Marketing strategy and planning</p>
                    </div>
               </div>            
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/branding-logo.png" class="width100 box-icon" alt="Branding guideline" title="Branding guideline">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Branding guideline</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/seo.png" class="width100 box-icon" alt="SEO guideline" title="SEO guideline">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">SEO guideline</p>
                    </div>
               </div>           

               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/social-media-content.png" class="width100 box-icon" alt="Social media content creation x2/month" title="Social media content creation x2/month">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media content creation x2/month</p>
                    </div>
               </div> 
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/logo-design2.png" class="width100 box-icon" alt="Artwork design x4/month" title="Artwork design x4/month">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Artwork design x4/month</p>
                    </div>
               </div>            
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/social-media-caption.png" class="width100 box-icon" alt="Social media caption enhancement x4/month" title="Social media caption enhancement x4/month">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media caption enhancement x4/month</p>
                    </div>
               </div>           
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/instagram2.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media publishing x6/month</p>
                    </div>
               </div>                                                 

               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/email.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">EDM blasting x1/month</p>
                    </div>
               </div>  
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/monthly-marketing-report.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Monthly marketing report</p>
                    </div>
               </div> 
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/event-planner.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Digital campaign planning x4/year</p>
                    </div>
               </div>  
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/seo-keyword.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">SEO keywords x10/year</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/networking-event.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Exclusive quarterly networking event</p>
                    </div>
               </div>  
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/sales-coaching-event.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Professional monthly sales coaching event</p>
                    </div>
               </div>  
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/cross-selling.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Cross-selling opportunities</p>
                    </div>
               </div>
            </div>                                                                                                               
            <div class="width100 text-center">
       			<button class="subscribe-button pointer clean separate-bottom subscribe-button80">Subscribe</button>
        		<!--<button class="subscribe-button pointer clean separate-bottom">Subscribe</button>-->
        	</div>  
        </div>
        <div class="package-box">
            <div class="color-box golden-box">
                <h3 class="thousand-h3a white-text text-center">Corporate Marketing</h3>
            </div> 
            <div class="box-content-div text-center mh">
                <img src="img/thousand-media/advanced-pack.png" class="package-img margin-auto"alt="Corporate Marketing Pack"  title="Corporate Marketing Pack">
                <p class="text-center coming-p">Coming Soon</p>
            </div> 
            <div class="width100 text-center">
        		<button class="subscribe-button pointer clean separate-bottom transparent">&nbsp;</button>
                <!--<button class="subscribe-button pointer clean separate-bottom">Subscribe</button>-->
        	</div> 
        </div>
    </div>
    <div class="six-row-div width100 overflow">
        <div class="float-left left-content">
            <h2 class="thousand-h2">Customized Packages</h2>
            <div class="gradient-border"></div>
            <p class="thousand-p3">Would you like to make your own unique selection of marketing services? Call us now!</p>
            <div class="one-row width100">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/call.png" class="width100 box-icon contact-left-icon" alt="Contact Us Thousand Media"  title="Contact Us Thousand Media">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p"><span class="web-span">+60 4 638 6082</span><a href="tel:+6046386082" class="tel-a">+60 4 638 6082</a></p>
                </div>
           </div> 
            <div class="one-row width100">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/email.png" class="width100 box-icon contact-left-icon" alt="Thousand Media Email"  title="Thousand Media Email">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p">thousandmedia.asia@gmail.com</p>
                </div>
           </div>        
           <p class="icon-p">
                <a href="https://www.instagram.com/thousandmedia.asia/" target="_blank">
                    <img src="img/thousand-media/instagram2.png" class="social-icon hover-opacity" alt="Thousand Media Instagram"  title="Thousand Media Instagram">
                </a>
                <a href="https://www.facebook.com/thousandmedia" target="_blank">
                    <img src="img/thousand-media/facebook2.png" class="social-icon social-icon2 hover-opacity" alt="Thousand Media Facebook"  title="Thousand Media Facebook">
                </a>
           </p>    
        </div>
        <div class="float-right right-img-div">
            <img src="img/thousand-media/customized-package.png" class="width100"  alt="Customized Digital Marketing Package"  title="Customized Digital Marketing Package">
        </div>
    </div>

    <div class="four-row-div same-padding width100 ow-padding-bottom">
        <h2 class="thousand-h2 text-center">Ad-hoc Services</h2>
        <div class="gradient-border margin-auto"></div>
       
    </div> 
    <div class="clear"></div>
    
    <div class="four-div-container width100 same-padding">
        <div class="four-div float-left text-center four-div-mh">
            <img src="img/thousand-media/video.png" class="four-image" alt="Marketing Strategy" title="Marketing Strategy">
            <p class="four-box-p ad-mh"><b>Film Production (2 mins)</b><br><span class="low-weight">From RM5,000</span></p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div four-div-mh">
            <img src="img/thousand-media/website.png" class="four-image" alt="SEO Guideline" title="SEO Guideline">
            <p class="four-box-p ad-mh">Website Development</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-div-mh">
            <img src="img/thousand-media/app.png" class="four-image" alt="Basic Branding" title="Basic Branding">
            <p class="four-box-p">App Development</p>
        </div>
        
        <div class="four-div float-left text-center four-second-div four-div-mh">
            <img src="img/thousand-media/influencer-marketing.png" class="four-image" alt="Artwork Design x2/Month" title="Artwork Design x2/Month">
            <p class="four-box-p">Influencer Marketing</p>
        </div>
        
        
        <div class="four-div float-left text-center four-div-mh">
            <img src="img/thousand-media/direct-email.png" class="four-image" alt="Electronic Direct Mail" title="Electronic Direct Mail">
            <p class="four-box-p ad-mh"><b>Electronic Direct Mail</b></p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div four-div-mh">
            <img src="img/thousand-media/seo2.png" class="four-image" alt="Search Engine Optimization" title="Search Engine Optimization">
            <p class="four-box-p ad-mh">Search Engine Optimization</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-div-mh">
            <img src="img/thousand-media/gsuite.png" class="four-image" alt="GSuite" title="GSuite">
            <p class="four-box-p">GSuite</p>
        </div>
        <div class="four-div float-left text-center four-second-div four-div-mh">
            
            <p class="four-box-p">&nbsp;</p>
        </div>        
                      	
    </div>
    <div class="clear"></div>
    <div class="four-row-div same-padding width100 ow-padding-bottom">
        <h2 class="thousand-h2 text-center">Designing Services</h2>
        <div class="gradient-border margin-auto"></div>
       
    </div> 
    <div class="clear"></div>    
    
    <div class="four-div-container width100 same-padding">
        <div class="four-div float-left text-center">
            <img src="img/thousand-media/logo-design.png" class="four-image" alt="Logo" title="Logo">
            <p class="four-box-p"><b>Logo</b></p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div">
            <img src="img/thousand-media/branding.png" class="four-image" alt="Business card" title="Business card">
            <p class="four-box-p">Business card</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/stationery.png" class="four-image" alt="Stationery" title="Stationery">
            <p class="four-box-p">Stationery</p>
        </div>
        
        <div class="four-div float-left text-center four-second-div">
            <img src="img/thousand-media/presentation-slide.png" class="four-image" alt="Presentation" title="Presentation">
            <p class="four-box-p">Presentation</p>
        </div>
        
        <div class="clear"></div>
        <div class="four-div float-left text-center">
            <img src="img/thousand-media/corporate-profile.png" class="four-image" alt="Corporate Profile" title="Corporate Profile">
            <p class="four-box-p"><b>Corporate Profile</b></p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div">
            <img src="img/thousand-media/brochure.png" class="four-image" alt="Brochure" title="Brochure">
            <p class="four-box-p">Brochure</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/flyer.png" class="four-image" alt="Flyer" title="Flyer">
            <p class="four-box-p">Flyer</p>
        </div>
        <div class="four-div float-left text-center four-second-div">
            <img src="img/thousand-media/poster.png" class="four-image" alt="Poster" title="Poster">
            <p class="four-box-p">Poster</p>
        </div>         
        <div class="clear"></div>


        <div class="four-div float-left text-center">
            <img src="img/thousand-media/bunting.png" class="four-image" alt="Bunting" title="Bunting">
            <p class="four-box-p"><b>Bunting</b></p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div">
            <img src="img/thousand-media/banner.png" class="four-image" alt="Banner" title="Banner">
            <p class="four-box-p">Banner</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/booklet.png" class="four-image" alt="Booklet" title="Booklet">
            <p class="four-box-p">Booklet</p>
        </div>        
        <div class="four-div float-left text-center four-second-div">
            
            <p class="four-box-p">&nbsp;</p>
        </div>        
                      	
    </div>
    <div class="clear"></div>    
    
</div>
<div id="contact">
    <div class="contactus-row width100">
        <div class="contactus-graphic float-left">
            <img src="img/thousand-media/bird-left1.gif"  class="width100 content-img" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">
            <!--<img src="img/thousand-media/sky5-left.png" class="width100 content-img sky2" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">-->
            </div>
            
        <div class="float-left contact-us-form">
            <h2 class="thousand-h2 ow-margin-bottom">Contact Us</h2>
            <div class="gradient-border"></div>
            <div class="one-row width100 extra-margin">
                <div class="left-icon float-left contact-icon">
                    <img src="img/thousand-media/call.png" class="width100 box-icon" alt="Contact Us Thousand Media"  title="Contact Us Thousand Media">
                </div>
                <div class="right-content float-left contact-right-content">
                    <p class="box-p contact2-p">+60 4 638 6082</p>
                </div>
           </div> 
           <div class="one-row width100">
                <div class="left-icon float-left contact-icon">
                    <img src="img/thousand-media/email.png" class="width100 box-icon" alt="Thousand Media Email"  title="Thousand Media Email">
                </div>
                <div class="right-content float-left contact-right-content">
                    <p class="box-p contact2-p">thousandmedia.asia@gmail.com</p>
                </div>
           </div>        
           <p class="icon-p">
                <a href="https://www.instagram.com/thousandmedia.asia/" target="_blank">
                    <img src="img/thousand-media/instagram2.png" class="social-icon hover-opacity" alt="Thousand Media Instagram"  title="Thousand Media Instagram">
                </a>
                <a href="https://www.facebook.com/thousandmedia" target="_blank">
                    <img src="img/thousand-media/facebook2.png" class="social-icon social-icon2 hover-opacity" alt="Thousand Media Facebook"  title="Thousand Media Facebook">
                </a>
           </p>
           <h2 class="thousand-h2 ow-margin-bottom">Send Us A Message!</h2>
           <div class="gradient-border"></div> 
                <form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                  <input type="text" name="name" placeholder="Your Name" class="input-name clean" ><br>
                  <input type="email" name="email" placeholder="Email" class="input-name clean" ><br>
                  <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean" ><br>
                  <textarea name="comments" placeholder="Type your message here" class="input-message clean" ></textarea>
                  <div class="clear"></div>
                  <input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required><p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
                  <input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required><p class="opt-msg left"> I just want to be contacted based on my request/ inquiry</p>
                  <div class="clear"></div>
                   
                  <input type="submit" name="submit" value="Send" class="input-submit white-text clean pointer">
                </form>       
        </div>
        <div class="contactus-graphic float-left">
			<img src="img/thousand-media/bird-right1.gif" class="width100 content-img sky1-right" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">
			<!--<img src="img/thousand-media/sky5-right.png" class="width100 content-img sky2 sky2a" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">-->
        </div>
        
    </div>
    <div class="clear"></div>
    <div class="contactus-row width100 tree3-div">
        <div class="contactus-graphic tree3-div">
            <img src="img/thousand-media/footer-left.png" class="width100 content-img purple-tree3" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">
            <img src="img/thousand-media/tree5.png" class="width100 content-img purple-tree4" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">

        </div>
        <div class="contactus-graphic tree3-div right0">
			<img src="img/thousand-media/footer-right.png" class="width100 content-img orange-tree3" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">
            <img src="img/thousand-media/tree5a.png" class="width100 content-img orange-tree4" alt="Thousand Media Contact Us" title="Thousand Media Contact Us">
        </div>
    <div class="footer-div width100 same-padding">
        <p class="footer-p white-text text-center">© 2019 Thousand Media, All Rights Reserved.</p>
    </div>
</div>





<style>
.food-gif{
	width:100px;
	position:absolute;
	top:calc(50% - 150px);
	text-align:center;
}
.center-food{
	width:100%;
	text-align:center;
	margin-left:-50px;}
#container{
	margin-top:-20px;}
#overlay{
  position:fixed;
  z-index:99999;
  top:0;
  left:0;
  bottom:0;
  right:0;
  background:#d21f3c;
  /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
  background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
  background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
  transition: 1s 0.4s;
}
#progress{
  height:1px;
  background:#fff;
  position:absolute;
  width:0;
  top:50%;
}
#progstat{
  font-size:0.7em;
  letter-spacing: 3px;
  position:absolute;
  top:50%;
  margin-top:-40px;
  width:100%;
  text-align:center;
  color:#fff;
}
.playstore-img:hover{
	opacity:0.8 !Important;}
.animated{
	-webkit-box-shadow:none !important; 
	box-shadow:none !important; 
	-moz-box-shadow:none !important; }
@media all and (max-width: 500px){
.food-gif{
	width:60px;
	top:calc(50% - 120px);
	text-align:center;
}
.center-food{
	margin-left:-30px;}	

}
</style>


<?php include 'js.php'; ?>
<!---- Contact Us Form ---->

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "thousandmedia.asia@gmail.com";
    $email_subject = "Contact Form via Thousand Media website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone']) ||
        !isset($_POST['comments'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; // required
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 

 
  if(strlen($comments) < 2) {
    $error_message .= 'The message you entered do not appear to be valid.\n';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you for contacting us. We will be in touch with you very soon.")</script>';
header("Location: https://vidatechft.com/vidatechinc/index.php"); /* Redirect browser */
exit();
?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>