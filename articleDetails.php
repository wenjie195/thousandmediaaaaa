<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Article.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$articles = getArticles($conn,"WHERE article_link = ? AND display = 'YES' ",array("article_link"),array($_GET['id']), "s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php 
    // Program to display URL of current page. 
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
    else
    $link = "http"; 

    // Here append the common URL characters. 
    $link .= "://"; 

    // Append the host(domain name, ip) to the URL. 
    $link .= $_SERVER['HTTP_HOST']; 

    // Append the requested resource location to the URL 
    $link .= $_SERVER['REQUEST_URI']; 

    // Print the link 
    // echo $link; 
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$articlesDetails = getArticles($conn,"WHERE article_link = ? ", array("article_link") ,array($_GET['id']),"s");
?>
<?php
if($articlesDetails)
{
for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
{
?>

<head>
<?php include 'meta.php'; ?>
<title><?php echo $articlesDetails[$cnt]->getTitle();?> | Thousand Media Online Advertising Strategy & Digital Marketing</title>
<meta property="og:title" content="<?php echo $articlesDetails[$cnt]->getTitle();?>  | Thousand Media Online Advertising Strategy & Digital Marketing" />
<meta property="og:url" content="https://thousandmedia.asia/articleDetails.php?id=<?php echo $articles[$cnt]->getArticleLink();?>" />
<link rel="canonical" href="https://thousandmedia.asia/articleDetails.php?id=<?php echo $articles[$cnt]->getArticleLink();?>" />
<meta property="og:image" content="https://thousandmedia.asia/uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();?>" />
<meta property="og:description" content="<?php echo $description = $articles[$cnt]->getDescription();?>" />
<meta name="description" content="<?php echo $description = $articles[$cnt]->getDescription();?>" />
<meta name="keywords" content="<?php echo $articles[$cnt]->getKeywordOne();?>,Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">
<?php include 'css.php'; ?>
</head>

<?php
}
}
?>
<?php
}
?>

<body class="body">
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>
<?php include 'header.php'; ?>
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="width100 blog-big-div overflow min-height menu-distance2">
	<div class="blog-inner-div">

    	<div class="blog-content">
            <?php
            if(isset($_GET['id']))
            {
            $conn = connDB();
            $articlesDetails = getArticles($conn,"WHERE article_link = ? ", array("article_link") ,array($_GET['id']),"s");
            ?>
                <?php
                if($articlesDetails)
                {
                    for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
                    {
                    ?>

                        <img src="uploadsArticle/<?php echo $articlesDetails[$cnt]->getTitleCover();?>" class="cover-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>"> 
                        <h1 class="user-title ow-margin-bottom-0"><?php echo $articlesDetails[$cnt]->getTitle();?></h1>       
                        
                        <p class="author-p">
                            <?php 
                                $authorName = $articlesDetails[$cnt]->getAuthorName();
                                $authorUid = $articlesDetails[$cnt]->getAuthorUid();
                                if($authorName == 'admin' && $authorUid == 'd2ac97ece5aa5d5674f3fb2941ade6e7')
                                {
                                    echo "Thousand Media";
                                }
                                else
                                {
                                    echo $authorName;
                                }
                            ?>
                        </p>

                        <p class="small-blog-date">
                            <?php echo $date = date("d-m-Y",strtotime($articlesDetails[$cnt]->getDateCreated()));?>
                        </p>
                        <p class="article-paragraph">
                            <?php echo $articlesDetails[$cnt]->getParagraphOne();?>
                        </p>
                        <?php $currentUid =  $articlesDetails[$cnt]->getUid();?>
                    <?php
                    }
                }
                ?>
            <?php
            }
            ?>
        </div>
        
     <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->
    <div class="clear"></div>
  <div class="width100 overflow">
              <!-- AddToAny BEGIN -->
        <div class="border-div"></div>
        <h3 class="darkpink-text share-h3">Share:</h3>
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_button_copy_link"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_linkedin"></a>
        <a class="a2a_button_blogger"></a>
        <a class="a2a_button_facebook_messenger"></a>
        <a class="a2a_button_whatsapp"></a>
        <a class="a2a_button_wechat"></a>
        <a class="a2a_button_line"></a>
        <a class="a2a_button_telegram"></a>
        <!--<a class="a2a_button_print"></a>-->
        </div> 
  </div>
        <div class="border-new-div share-div">
        	<h1 class="user-title">Recommended Articles :</h1>      
        	<div class="width100 recommend">
 
                <?php
                // $conn = connDB();
                // $allArticles = getArticles($conn);
                $allArticles = getArticles($conn," WHERE uid != '$currentUid' AND display = 'Yes' ORDER BY date_created DESC LIMIT 3");

                if($allArticles)
                {   
                    for($cntAA = 0;$cntAA < count($allArticles) ;$cntAA++)
                    {
                    ?>
                        
                    <a href='articleDetails.php?id=<?php echo $allArticles[$cntAA]->getArticleLink();?>' class="opacity-hover">
                        <div class="shadow-white-box width100 blog-box opacity-hover">
                            <div class="left-img-div2">
                                <img src="uploadsArticle/<?php echo $allArticles[$cntAA]->getTitleCover();?>" class="width100" alt="Blog Title" title="Blog Title">
                            </div>
                            <div class="right-content-div3">
                                <h3 class="article-title text-overflow">
                                    <?php echo $allArticles[$cntAA]->getTitle();?>
                                </h3>
                                <p class="date-p">
                                    <?php echo $allArticles[$cntAA]->getDateCreated();?>
                                </p>
                                <p class="right-content-p">
                                    <?php echo $allArticles[$cntAA]->getKeywordOne();?>
                                </p>
                            </div>
                        </div>
                    </a>

                    <?php
                    }
                    ?>
                <?php
                }
                ?>

            </div>

        </div>

        
	</div>
</div>

<?php include 'js.php'; ?>
<script>
document.onkeydown = function(e) {
    if(e.keyCode == 123) {
      return false;
     }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
     return false;
    }
	 if(e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }      
 }
</script>
</body>
</html>