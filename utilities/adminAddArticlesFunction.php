<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/Users.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

$timestamp = time();

function uploadArticles($conn,$uid,$authorUid,$authorName,$title,$seoTitle,$keywordOne,$articleLink,$titleCover,$imgCoverSrc,$description,$paragraphOne,$display)
{
     if(insertDynamicData($conn,"articles",array("uid","author_uid","author_name","title","seo_title","keyword_one","article_link","title_cover","img_cover_source",
                              "description","paragraph_one","display"),
          array($uid,$authorUid,$authorName,$title,$seoTitle,$keywordOne,$articleLink,$titleCover,$imgCoverSrc,$description,$paragraphOne,$display),"ssssssssssss") === null)
     {
          header('Location: ../userUploadArticles.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $userDetails = getUsers($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");

     $authorUid = $userDetails[0]->getUid();
     $authorName = $userDetails[0]->getUsername();

     $title = rewrite($_POST['title']);

     // $articleLink = rewrite($_POST['article_link']);
     $stringOne = ($_POST['article_link']);
     $preArticleLink = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
     $articleLink = str_replace(' ', '-', trim($preArticleLink));

     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;

     $keywordOne = rewrite($_POST['keyword_one']);

     $imgCoverSrc = rewrite($_POST['cover_photo_source']);

     $titleCover = $timestamp.$_FILES['cover_photo']['name'];
     // $titleCover = $uid.$_FILES['cover_photo']['name'];
     $target_dir = "../uploadsArticle/";
     $target_file = $target_dir . basename($_FILES["cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['cover_photo']['tmp_name'],$target_dir.$titleCover);
     }

     // $str2 = rewrite($_POST["descprition"]);
     // $newSPAPrice = str_replace( ',', '', $str2);
     // $keywordOne = $newSPAPrice;

     // $keywordOne = rewrite($_POST['descprition']);
     $description = ($_POST['descprition']);  //no rewrite, cause error in db
     $paragraphOne = ($_POST['editor']);  //no rewrite, cause error in db
     $display = "Yes";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $title."<br>";
     // echo $paragraphOne."<br>";
     // echo $type."<br>";

     $currentTitle = getArticles($conn," WHERE title = ? AND display = 'YES' ",array("title"),array($_POST['title']),"s");
     $previousTitle = $currentTitle[0];

     $currentArticleLink = getArticles($conn," WHERE article_link = ? AND display = 'YES' ",array("article_link"),array($_POST['article_link']),"s");
     $previousArticleLink = $currentArticleLink[0];

     if (!$previousTitle && !$previousArticleLink)
     {

          if(uploadArticles($conn,$uid,$authorUid,$authorName,$title,$seoTitle,$keywordOne,$articleLink,$titleCover,$imgCoverSrc,$description,$paragraphOne,$display))
          {
               // echo "success";
               $_SESSION['messageType'] = 1;
               header('Location: ../adminArticlesDashboard.php?type=1');
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminArticlesDashboard.php?type=2');
          }

     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../adminArticlesDashboard.php?type=3');
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>