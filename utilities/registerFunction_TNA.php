<?php
if (session_id() == "")
{
     session_start();
}
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Users.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$fullname,$phoneNo,$email,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"users",array("uid","username","full_name","phone","email","password","salt"),
          array($uid,$username,$fullname,$phoneNo,$email,$finalPassword,$salt),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $fullname = rewrite($_POST['register_fullname']);
     $phoneNo = rewrite($_POST['register_phone']);
     $email = rewrite($_POST['register_email_user']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $email."<br>";
     // echo $finalPassword."<br>";
     // echo $salt."<br>";
     // echo $phoneNo ."<br>";
     // echo $tac."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUsers($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUsers($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
               $userEmailDetails = $userEmailRows[0];

               // $userPhoneRows = getUsers($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
               // $userPhoneDetails = $userPhoneRows[0];

               if(!$usernameDetails && !$userEmailDetails)
               {
                    if(registerNewUser($conn,$uid,$username,$fullname,$phoneNo,$email,$finalPassword,$salt))
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../index.php?type=1');
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=5');
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../index.php?type=3');
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=4');
     }      
}
else 
{
     header('Location: ../index.php');
}
?>