<header id="header" class="header header--fixed same-padding header1 menu-white blog-menu admin-header" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<a href="index.php">
            <img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media">
            <!--<img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">-->
            </a>
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="index.php" class="black-text menu-padding red-hover">Thousand Media</a>
            <a href="digital-marketing-blog-news.php" class="black-text menu-padding red-hover">Blog</a>
           
        </div>
	</div>

</header>