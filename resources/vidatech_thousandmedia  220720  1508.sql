-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2020 at 09:08 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_thousandmedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `article_link` text DEFAULT NULL,
  `keyword_one` text DEFAULT NULL,
  `keyword_two` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `title_cover` text DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `description`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, 'ab490266031eaa664c78239bcc0ec119', 'd2ac97ece5aa5d5674f3fb2941ade6e7', 'admin', '1000 Media 1st Editing 2 Testing Article', '1000Media1stEditing2TestingArticle-1000Media1stEditing2TestingArticle', '1000Media1stEditing2TestingArticle', 'Thousand Media First Editing Testing Article', NULL, '绘本很简单，通常篇幅不长，很快就翻完了', '1595400888464154da-07e5-44f7-8660-c539c0a2baa2.jpg', '<h1 style=\"text-align:center\"><span style=\"font-size:24px\"><strong>流动绘本教学馆</strong></span></h1>\r\n\r\n<p style=\"text-align:center\">有人说，绘本很简单，通常篇幅不长，很快就翻完了；可又有人说，绘本太不简单了！简洁的图文背后，可以蕴含著丰富感情、刻画深刻真理，放飞童梦的想像，留下无尽的余韵&hellip;</p>\r\n\r\n<p style=\"text-align:center\">一页页有趣的图文，能触发孩子阅读的兴趣，培养良好的性格、行为习惯，提高他们的想像力、感知力、观察能力与认识世界的能力。绘本教育极其奥妙，这也是为何曾子轩和胡嘉昱夫妇在3年前放弃原本的高薪工作，走出自己的舒适圈，展开全职推广绘本教学之路，并成立&ldquo;嘉轩绘本课程开发培训室&rdquo;。</p>\r\n\r\n<p style=\"text-align:center\">&ldquo;我是子轩，她是嘉昱，所以我们是&lsquo;嘉轩&rsquo;&rdquo;，曾子轩说：&ldquo;嘉昱在幼教领域有约20年经验，最后还是抛开了所有顾虑，下定决心和我携手共进，把推广绘本教育当作事业归属的重要时刻。&rdquo;2010年，他开发绘本阅读写作课程，历经3年带课实践，课程配套正式推出，并与多家教育中心合作。直到2017年，&ldquo;嘉轩绘本课程开发培训室&rdquo;正式成立。</p>\r\n\r\n<p style=\"text-align:center\">胡嘉昱笑说：&ldquo;不止是推广绘本阅读，我们要做的还是最吃力不讨好的课程开发及师资培训。&rdquo;说实在的，绘本近年来在国内逐渐盛行，各大书局的绘本都登上了热销榜，各类绘本活动数之不尽，然而，说到以推动绘本教学为主要使命的培训机构，&ldquo;嘉轩&rdquo;是第一个。&ldquo;我们相信，只要有优质教育内容和有素质的教学者，再加上家长愿意改变追逐&lsquo;成绩至上&rsquo;，三方配合，孩子们一定可以快乐学习。&rdquo;</p>\r\n\r\n<p style=\"text-align:center\">推广过程中，夫妻俩开著私家车，从玻璃市到新山跑了约50个地区，其中包括一些偏乡地区。从当地居民中得知，偏乡小镇资源少，居民也希望孩子能够接触绘本。为了让资源匮乏的当地学校或社区也能受惠，两人更在去年7月展开《绘本故事车环马行》计划，开著货车到偏乡社区提供免费教育服务，推广绘本阅读。&ldquo;我们希望把绘本带到全国各角落，用&lsquo;让阅读的种子在绘本中萌芽&rsquo;的口号，积极推动全马的阅读风气。&rdquo;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:20px\"><strong>儿童绘本 改变僵化教育模式</strong></span></p>\r\n\r\n<p>&ldquo;嘉轩&rdquo;绘本故事车就像流动绘本馆，每个月前往半岛不同州属和社区。胡嘉昱分享：&ldquo;除了到偏远地区的小学和社区推广绘本阅读，我们也会针对地方需求，培训当地人成为故事志工，或绘本妈妈。&rdquo;除了夫妻俩，由家长、老师、社区活动工作者等组成的当地志工团队也会全力协助，共同带动该项社区服务。每到一个地区，他们都会逗留几天，走入学校举办讲座或阅读活动，并开办志工团队培训班、公开讲座、市集等。</p>\r\n\r\n<p style=\"text-align:right\">《绘本故事车环马行》社区回馈计划的经费从何而来？曾子轩笑说：&ldquo;这项计划一个月一次，我们平日有在售卖课程配套，同时会接一些培训工作，作为主要收入来源。&rdquo;他续指，《嘉轩绘本》得到了很多热心人士的帮助，回馈社会是理所当然的。至于故事车的头期款，则是以众筹方式筹得。他补充，众筹反应非常热烈，只用了3天就筹获逾8000令吉，&ldquo;我们注重精神延续和种子萌芽，这是打造故事车的最大目的。&rdquo;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"绘本的魅力很大，被誉为是学前教育的最佳启蒙读物。若想要培养孩子的阅读习惯，绘本是一个效率极高的工具，因为孩子先是透过聆听学习，再来才是观看探索。\" src=\"https://www.orientaldaily.com.my/images/uploads/news/s006.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"绘本故事车\" src=\"https://www.orientaldaily.com.my/images/uploads/news-cover/s003.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><span style=\"font-size:20px\"><strong>许孩子发现世界的眼光</strong></span></p>\r\n\r\n<p style=\"text-align:center\">绘本是亲子间的沟通桥梁，当家长与孩子相依而坐，家长为孩子轻声朗读一个又一个绘本中的故事，孩子们好奇地看著一页页的图画、听著故事，心情也会伴随著故事轻轻飞扬。两人强调，绘本只是一个工具，使用方法必须由家长和老师主导。因此他们积极推广亲子共读，由父母讲故事给孩子听，同时帮助孩子学习更多新词汇。胡嘉昱表示：&ldquo;亲子共读的主要目的是让孩子感受阅读的乐趣，父母不需要问太多问题，只要好好地陪伴就好。&rdquo;</p>\r\n\r\n<p style=\"text-align:center\">&ldquo;一场绘本活动不是圆满结束、人人赞好就足够了。&rdquo;他们期许活动的后续发展，而当地的志工团队就是后续发展的推动者，通过举办各类绘本活动，如设立图书馆、举办读书会等，大力推动阅读风气，延续《嘉轩绘本》的精神。曾子轩说：&ldquo;做社区服务就应该要有系统地推广，团队力量是很重要的一环，因此我们把培训集中于社区团队的架构建立，同时也培训故事志工的技能，训练他们说故事的技巧。唯有如此，才能让绘本教育扎根在各处并逐渐发芽，让绘本给孩子们&lsquo;发现&rsquo;世界的眼睛，进而拥有一片辽阔的心灵视野。&rdquo;</p>\r\n', NULL, NULL, NULL, NULL, NULL, 'oriental daily 22', NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-07-22 06:40:50', '2020-07-22 06:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `en_name` varchar(255) DEFAULT NULL,
  `ch_name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `en_name`, `ch_name`, `value`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Beauty', '美容', 'Beauty', 'Available', '2020-07-09 01:47:35', '2020-07-09 01:47:35'),
(2, 'Fashion', '时尚', 'Fashion', 'Available', '2020-07-09 01:47:35', '2020-07-09 01:47:35'),
(3, 'Social', '社交', 'Social', 'Available', '2020-07-09 01:47:54', '2020-07-09 01:47:54'),
(4, 'Romance', '浪漫', 'Romance', 'Available', '2020-07-09 01:47:54', '2020-07-09 01:47:54'),
(5, 'Entrepreneurship', '创业', 'Entrepreneurship', 'Available', '2020-07-09 01:48:10', '2020-07-09 01:48:10'),
(6, 'DIY', '自制', 'Diy', 'Available', '2020-07-09 01:48:10', '2020-07-09 01:48:10'),
(7, 'Cooking', '烹饪', 'Cooking', 'Available', '2020-07-09 01:48:27', '2020-07-09 01:48:27'),
(8, 'E-Sports', '电游', 'Esport', 'Available', '2020-07-09 01:48:27', '2020-07-09 01:48:27'),
(9, 'Lifestyle', '生活', 'Lifestyle', 'Available', '2020-07-09 01:48:45', '2020-07-09 01:48:45'),
(10, 'Pets', '宠物', 'Pets', 'Available', '2020-07-09 01:48:45', '2020-07-09 01:48:45'),
(11, 'Travel', '旅游', 'Travel', 'Available', '2020-07-09 01:48:56', '2020-07-09 01:48:56');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` int(255) NOT NULL,
  `username` varchar(255) DEFAULT 'NULL',
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `selection` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `username`, `email`, `phone`, `full_name`, `message`, `selection`, `package`, `date_created`) VALUES
(1, 'aasdplayer', 'joeflezer39@gmail.com', '0162992991', NULL, 'I am interesed', 'contact-on-request', 'Thousand Arts', '2020-01-31 05:54:50'),
(14, 'Brianacido', 'no-reply@hilkom-digital.de', '85846163232', NULL, 'hi there \r\nI have just checked thousandmedia.asia for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly repor', '', NULL, '2020-02-05 11:34:32'),
(15, 'YoungWaync', 'stunningkorea@gmail.com', '83257637874', NULL, 'We would like to take this opportunity to introduce our company. \r\nWe are STUNNING KOREA, a special event tour company in Korea. \r\nWe provide various different types of event in Korea for associations and groups. \r\n \r\n \r\nWe’re offering a event tour for a ', '', NULL, '2020-02-14 11:45:53'),
(16, 'Felicity Smith', 'felicitysmith.seo@gmail.com', '09563297372', NULL, 'Hello SEO Analyst/Manager,\r\n\r\nI hope you are rocking with the best Link building techniques to enhance all SEO and marketing efforts at scale!\r\n\r\nAre you struggling to find high domain authority blogs to link back to your website? We have a diverse list o', 'contact-more-info', NULL, '2020-02-18 06:15:12'),
(17, 'MichaelCof', 'hcard.marketing@gmail.com', '82124923742', NULL, '嗨，我是克里斯（Hcard）的行销总监克里斯（Chris）。 \r\n \r\n这次我与您联系以在您的网站上介绍Hcard。 \r\n \r\nHcard是万事达品牌的借记卡巫婆，非常好用。 \r\n \r\n使用Hcard需要Fiatbit和Hcard应用程序。 \r\n \r\nFiatbit应用程序具有会员功能，当发卡行从您的推荐链接中出来时，您可以获得以下奖励。 \r\n \r\n&gt;&gt;卡发行奖励 \r\n1级：$ 100s \r\n2等：$ 50 \r\n3等：$ 30 \r\n \r\n&gt;&gt;充值奖励 \r\n1级：费用的30％ \r\n2', '', NULL, '2020-02-19 11:13:32'),
(18, 'David Gevorkian', 'david@beaccessible.com', '+1 8007350565', NULL, 'Hello,\r\n\r\nIf you are a web development or marketing firm that builds eCommerce sites, one of your clients will likely face web accessibility litigation in 2019.\r\n\r\nFor the first time, in summer 2018, a Los Angeles state judge began assessing statutory pen', 'contact-more-info', NULL, '2020-02-22 10:57:18'),
(19, 'Felicity Smith', 'felicitysmith.seo@gmail.com', '09563297372', NULL, 'Hello SEO Analyst/Manager,\r\n\r\nI hope you are rocking with the best Link building techniques to enhance all SEO and marketing efforts at scale!\r\n\r\nAre you struggling to find high domain authority blogs to link back to your website? We have a diverse list o', 'contact-more-info', NULL, '2020-02-24 11:05:20'),
(20, 'joe', 'joe.vidatech@gmail.com', '0152778281', NULL, 'Testing again', 'contact-on-request', 'Advanced Marketing Pack', '2020-02-26 02:16:48'),
(21, 'AnthonyElids', 'raphaeDyence@gmail.com', '82134363866', NULL, 'Hi!  thousandmedia.asia \r\n \r\nDo you know the easiest way to state your products or services? Sending messages through feedback forms can permit you to easily enter the markets of any country (full geographical coverage for all countries of the world).  Th', '', NULL, '2020-03-01 18:27:27'),
(22, 'sherry', 'sherry2.vidatech@gmail.com', '123', NULL, 'test', 'contact-on-request', 'Basic Marketing Pack', '2020-03-02 02:43:03'),
(23, 'Davidflace', 'no-reply@hilkom-digital.de', '88163352189', NULL, 'hi there \r\nI have just checked thousandmedia.asia for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly repor', '', NULL, '2020-03-05 06:53:10'),
(24, 'Isla Reed', 'islareedseo@gmail.com', '6296246665', NULL, 'Hello Marketing Team,\r\n \r\nThis is Isla from OLM Digital Agency, a leading full content writing, and marketing solution Provider from small to large online businesses to Marketing Agencies in many industries! \r\n\r\nI’m reaching out to see if you might be int', 'contact-on-request', NULL, '2020-03-06 12:35:51'),
(25, 'Ettienne', 'ettienne@rugoshath.com', '(03) 5334 0968', NULL, 'I&#039;m currently looking at businesses who are close to ranking on\r\npage one of Google, and noticed that your website https://thousandmedia.asia/\r\nis currently in the top 100 pages of search results for\r\n&quot;social media marketing service&quot;, which', 'contact-more-info', 'Basic Marketing Pack', '2020-03-18 16:13:54'),
(26, 'Tam Shapoval', 'tspieker6@gmail.com', '82785135326', NULL, 'Dear CEO: thousandmedia.asia \r\nIt will be my pleasure to collaborate with you and maintain a confidential/Financial transaction with your Company for mutual benefits. I’m looking forward to a prospective business relationship with you. Your company profil', '', NULL, '2020-03-19 03:25:51'),
(27, 'Martinvaf', 'no-reply@hilkom-digital.de', '88941117534', NULL, 'hi there \r\nI have just checked thousandmedia.asia for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly repor', '', NULL, '2020-03-28 03:04:56'),
(28, 'Domenic McCutcheon', 'domenic.mccutcheon84@gmail.com', '570-322-1156', NULL, 'If you’ve been online any amount of time, you’re most likely still struggling to make the money you want online…\r\n\r\nAm I right?\r\n\r\nWell, we’re confident to say that you’ve come to the right place.\r\n\r\nYou might be thinking… Why are we different from the th', 'contact-more-info', '7 Days Free Trial', '2020-04-10 19:18:00'),
(29, 'Jamesrhirl', 'coronavaccine@hotmail.com', '81562565934', NULL, 'COVID-19 outbreak: airplanes grounded, borders closed, businesses shut, citizens quarantined, political power seized, democracy undermined. \r\nAll this, if it is not stopped shortly, can lead to chaos and unrests. \r\nCurrently http://ST-lF.NET focus on rais', '', NULL, '2020-04-14 01:35:42'),
(30, 'MickeyCibia', 'brymosamson@gmail.com', '86967456652', NULL, 'We supply Medical-Grade Level 1,2 and 3 surgical face Mask, Hand Sanitizer, Infrared Thermometer, Disposable/Surgical gloves and N95 Filter Mask reduces the risk from Coronavirus. Lab tested and clinically proven to prevent the risk of infection. The \r\nen', '', NULL, '2020-04-15 11:44:59'),
(31, 'Raymond 	Brown', 'info@thecctvhub.com', '87961125681', NULL, 'Dear Sir/mdm, \r\n \r\nHow are you? \r\n \r\nWe supply medical products: \r\n \r\nMedical masks \r\n3M, 3ply, KN95, KN99, N95 masks \r\nProtective masks \r\nEye mask \r\nProtective cap \r\nDisinfectant \r\nHand sanitiser \r\nMedical alcohol \r\nEye protection \r\nDisposable latex glov', '', NULL, '2020-04-23 05:10:26'),
(32, 'Martinvaf', 'no-reply@hilkom-digital.de', '87196762544', NULL, 'hi there \r\nI have just checked thousandmedia.asia for the ranking keywords and seen that your SEO metrics could use a boost. \r\n \r\nWe will improve your SEO metrics and ranks organically and safely, using only whitehat methods, while providing monthly repor', '', NULL, '2020-04-25 05:29:58'),
(33, 'Moon', 'munhua@juruquest.com', '0123479085', NULL, 'I would like to know more on the Marketing Packages', 'contact-more-info', NULL, '2020-04-27 06:48:50'),
(34, 'James Giovanni', 'jgiovanni90@comcast.net', '84732735855', NULL, 'Good day, \r\n \r\n* Do you have a viable project that requires funding ? \r\n \r\n* Long term loan with reasonable interest rate ? \r\n \r\n* B.G/S.B.L.C \r\n \r\nRegards, \r\n \r\nJames Giovanni \r\nFinancial Broker \r\nTell +1 302 440 3223 \r\nChat Telegram +1 302 440 3223', '', NULL, '2020-05-11 15:20:56'),
(35, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, 'Test', 'contact-on-request', 'Thousand Arts', '2020-06-02 09:59:32'),
(36, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, 'u', 'contact-on-request', 'Thousand Arts', '2020-06-02 10:00:25'),
(37, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, 's', 'contact-on-request', '7 Days Free Trial', '2020-06-02 10:04:05'),
(38, 'Sherry Tan', 'sherry.tanhl@gmail', '012345678', NULL, '', 'contact-on-request', '7 Days Free Trial', '2020-06-02 10:04:29'),
(39, 'Sherry Tan', '', '', NULL, '', 'contact-on-request', '7 Days Free Trial', '2020-06-02 10:04:44'),
(40, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, 's', 'contact-more-info', 'Basic Marketing Pack', '2020-06-02 10:06:37'),
(41, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, 's', 'contact-on-request', 'Basic Marketing Pack', '2020-06-02 10:08:45'),
(42, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, '1', 'contact-on-request', 'Infinity Content', '2020-06-03 04:48:41'),
(43, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, '1', 'contact-more-info', NULL, '2020-06-05 10:28:28'),
(44, 'Sherry Tan', 'sherry.tanhl@gmail.com', '012345678', NULL, '-', 'contact-more-info', 'Consultancy', '2020-07-08 04:43:34');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `username` varchar(255) DEFAULT 'NULL',
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `ic` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `selection` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `user_type` int(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT 'NULL',
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `ic` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `selection` varchar(255) DEFAULT NULL,
  `package` varchar(255) DEFAULT NULL,
  `user_type` int(20) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone`, `ic`, `full_name`, `message`, `selection`, `package`, `user_type`, `date_created`) VALUES
(1, 'd2ac97ece5aa5d5674f3fb2941ade6e7', 'admin', 'admin@gmail.com', 'eae3059600adab5071e7a142cee7d3824c4d6aa26c166d300a744ef0ec47c2d3', '465164d3dae3de7343cb82a7e71b99d3f47addf3', '012345678', NULL, 'Admin 1000 Media', NULL, NULL, NULL, 0, '2020-07-22 02:07:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
