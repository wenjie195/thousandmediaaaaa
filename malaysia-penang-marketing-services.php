<!doctype html>
<html>
<head>

<title>Marketing Solutions Grow Your Business With Us | Thousand Media Online advertising Strategy</title>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://thousandmedia.asia/malaysia-penang-marketing-services.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/fb-meta.jpg" />
<meta property="og:title" content="Marketing Solutions Grow Your Business With Us | Thousand Media Online advertising Strategy" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">

<?php include 'css.php'; ?>
  <link rel="canonical" href="https://thousandmedia.asia/malaysia-penang-marketing-services.php" />
</head>

<body class="body" >

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>
<header id="header" class="header header--fixed same-padding header1 menu-white tart-menu" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
        	<a href="index.php">
    			<img src="img/thousand-media/logo.png" class="logo-img web-logo red-logo" alt="Thousand Media" title="Thousand Media">
            	<img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">
            </a>
           
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="index.php" class="white-text menu-padding red-hover opacity-white-hover ">About Thousand Media</a>
 			<div class="dropdown">
            <a class="white-text menu-padding red-hover opacity-white-hover ">Services <img src="img/thousand-media/dropdown.png" class="dropdown-png"></a>
                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="malaysia-penang-graphic-design-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Graphic Design</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-marketing-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Marketing Solutions</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-content-copywriting.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Writing</a></p>  
                	</div>            
            
            </div> 
            <a href="blog.php" class="white-text red-hover opacity-white-hover ">Blog</a>
		<!-- Mobile View-->
            <a href="index.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/about-us.png" class="menu-img" alt="About Thousand Media" title="About Thousand Media">
            </a>
            <a href="malaysia-penang-graphic-design-services.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/graphic-design3.png" class="menu-img" alt="Graphic Design" title="Graphic Design">            
            </a>  
            <a href="malaysia-penang-content-copywriting.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/content-writing-white.png" class="menu-img" alt="Content Writing" title="Content Writing">            
            </a>          
            <a href="blog.php" class="white-text red-hover2">
            	<img src="img/thousand-media/blog.png" class="menu-img" alt="Blog" title="Blog">            
            </a>            
        </div>
	</div>

</header>
<div class="width100 same-padding overflow starry-bg" >
	<div data-wow-iteration="infinite" data-wow-duration="1.5s" data-wow-delay="300ms" class="wow pulse tzexin" style="visibility: visible; animation-iteration-count: infinite; animation-name: pulse;">
		<img src="img/thousand-media/design-planet-tzexin727.png" alt="Design Planet TZ727" title="Design Planet TZ727" class="planet-img" >
    </div>
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" data-wow-delay="300ms" class="wow pulse left-star-div3 absolute" style="visibility: visible; animation-iteration-count: infinite; animation-name: pulse;">
    	<img src="img/thousand-media/star2.png" class="star-img2" alt="Malaysia Marketing" title="Malaysia Marketing">
    </div>        
	<div class="first-content-div">
    	<h1 class="galaxy-h1-title white-text">Marketing Solutions<br>Grow Your Business With Us!</h1>
        <p class="explanation-p">Enjoy your free setup (RM500) for all packages.</p>
        <p class="details-p">Have you ever wonder why your business is not growing although you have all set up while others are so successful? It may because of specific reasons.</p>
    </div>
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" class="wow pulse left-star-div">
    	<img src="img/thousand-media/star.png" class="star-img" alt="Marketing Star" title="Marketing Star">
    </div>
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" data-wow-delay="300ms" class="wow pulse left-star-div2 absolute" >
    	<img src="img/thousand-media/star2.png" class="star-img" alt="Malaysia Marketing" title="Malaysia Marketing">
    </div>    
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" data-wow-delay="300ms" class="wow pulse right-star-div absolute">
    	<img src="img/thousand-media/star2.png" class="star-img" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>   
    <div data-wow-iteration="infinite" data-wow-duration="2s" data-wow-delay="420ms" class="wow pulse right-star-div2 absolute">
    	<img src="img/thousand-media/star.png" class="star-img3" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>
   
	<div data-wow-iteration="infinite" data-wow-duration="15.5s" class="span3 wow shake left-pink-planet absolute">
		<img src="img/thousand-media/copywriting-services-planet.png" alt="Copywriting Planet" title="Copywriting Planet"  class="planet-img" >
    </div>    
    <div class="second-content-div white-box-div">
    	<h1 class="galaxy-h1-title2 green-text">Basic Marketing Solutions</h1>
        	<p class="li-p">&#8226; Provide guidelines to position your brand to compete with others</p>
            <p class="li-p">&#8226; Provide guidelines for Search engine optimization (SEO) and rank your website on Google Search Engine</p>
            <p class="li-p">&#8226; Position your brand to compete with others</p>
            <p class="li-p">&#8226; Creative artwork design (x 2)</p>
            <p class="li-p">&#8226; Creative content writing (x 2)</p>
            <p class="li-p margin-bottom0">&#8226; Social media publishing (x 2)</p>
        </ul>
    </div>
    <div class="width100 text-center">
    	<div class="pink-radius-button hover-a-reverse red-btn open-form"  id="my_input" name="my_input" value="Basic Marketing Pack">Learn More</div>
    </div>
	<div data-wow-iteration="infinite" data-wow-duration="1.5s" class="wow pulse absolute blue-planet" style="visibility: visible; animation-iteration-count: infinite; animation-name: pulse;">
		<img src="img/thousand-media/sherry-planet.png" alt="Good Luck Planet" title="Good Luck Planet"  class="planet-img"  >
    </div>    
	<div data-wow-iteration="infinite" data-wow-duration="15.5s" class="span3 wow shake gold-planet absolute">
		<img src="img/thousand-media/increase-sales-planet.png" alt="Increase Sales Planet" title="Increase Sales Planet"  class="planet-img" >
    </div>   
	<div  data-wow-iteration="infinite" data-wow-duration="10.15s" class="span3 wow bounce roi-planet absolute">
		<img src="img/thousand-media/better-roi-planet.png" alt="Better ROI Planet" title="Better ROI Planet"  class="planet-img" >
    </div>  
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" class="wow pulse left-star-div-roi absolute">
    	<img src="img/thousand-media/star2.png" class="star-img" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>   
    <div data-wow-iteration="infinite" data-wow-duration="2s" class="wow pulse left-star-div-roi2 absolute">
    	<img src="img/thousand-media/star.png" class="star-img3" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>    
 	<div  data-wow-iteration="infinite" data-wow-duration="12.15s" class="span3 wow bounce mingshie absolute">
		<img src="img/thousand-media/raining-planet-mingshie518.png" alt="Raining MS518 Planet" title="Raining MS518 Planet"  class="planet-img" >
    </div>     
	<div data-wow-iteration="infinite" data-wow-duration="3.5s" class="wow pulse blue-iron  absolute" style="visibility: visible; animation-iteration-count: infinite; animation-name: pulse;">
		<img src="img/thousand-media/increase-customer-planet.png" alt="Increase Cutsomer Planet" title="Increase Cutsomer Planet"  class="planet-img" >
    </div>
    <div class="third-content-div white-box-div">
    	<h1 class="galaxy-h1-title2 purple-text">Essential Marketing Solutions</h1>
        	<p class="li-p">&#8226; Given a dedicated account manager to handle your request</p>
            <p class="li-p">&#8226; Conduct brand review to enhance your competition</p>
            <p class="li-p">&#8226; Develop marketing strategies and planning for future growth</p>
            <p class="li-p">&#8226; Provide guidelines for Search engine optimization (SEO) and rank your website on Google Search Engine</p>
            <p class="li-p">&#8226; Social media publishing (x 8)</p>
            <p class="li-p margin-bottom0 red-highlight">&#8226; Can choose to use Thousand Arts or Infinity Content</p>
        </ul>
    </div>
    <div class="width100 text-center">
    	<div class="pink-radius-button hover-a-reverse red-btn open-form" id="package_type" name="package_type" value="Essential Marketing Pack">Learn More</div>
    </div>    
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" class="wow pulse left-star-div-purple absolute">
    	<img src="img/thousand-media/star2.png" class="star-img" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>   
    <div data-wow-iteration="infinite" data-wow-duration="2s" class="wow pulse left-star-div-purple2 absolute">
    	<img src="img/thousand-media/star.png" class="star-img3" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>    
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" class="wow pulse left-star-div-purple3 absolute">
    	<img src="img/thousand-media/star2.png" class="star-img" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>   
    <div data-wow-iteration="infinite" data-wow-duration="2s" class="wow pulse left-star-div-purple4 absolute">
    	<img src="img/thousand-media/star.png" class="star-img3" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>    
 	<div  data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow bounce black-rock absolute">
		<img src="img/thousand-media/penang-marketing-solution.png" alt="Penang Marketing Solution" title="Penang Marketing Solution"  class="planet-img" >
    </div>
    <div class="four-content-div white-box-div">
    	<h1 class="galaxy-h1-title2 orange-text">Advanced Marketing Solutions</h1>
        	<p class="li-p">&#8226; Given a dedicated account manager to handle your request</p>
            <p class="li-p">&#8226; Conduct brand review to enhance your competition</p>
            <p class="li-p">&#8226; Develop marketing strategies and planning for future growth</p>
            <p class="li-p">&#8226; Provide guidelines for Search engine optimization (SEO) and rank your website on Google Search Engine</p>
            <p class="li-p">&#8226; Daily social media publishing</p>
            <p class="li-p">&#8226; Develop product or service strategies to increase your sales</p>
            <p class="li-p margin-bottom0 red-highlight">&#8226; Can use both Thousand Arts and Infinity Content at same time</p>
        </ul>
    </div>
    <div class="width100 text-center">
    	<div class="pink-radius-button hover-a-reverse red-btn open-form" id="package_type" name="package_type" value="Advanced Marketing Pack">Learn More</div>
    </div>      
    <div data-wow-iteration="infinite" data-wow-duration="15.5s" class="span3 wow shake advanced-marketing-planet absolute animated" style="visibility: visible; animation-duration: 15.5s; animation-iteration-count: infinite; animation-name: shake;">
    	<img src="img/thousand-media/advanced-marketing-solutions.png" alt="Advanced Marketing Solution" title="Advanced Marketing Solution"  class="planet-img" >
    </div>
    <div data-wow-iteration="infinite" data-wow-duration="2s" class="wow pulse advanced-marketing-planet-red absolute">
    	<img src="img/thousand-media/malaysia-marketing-solution.png"  alt="Malaysia Marketing Services" title="Malaysia Marketing Services"  class="planet-img" >
    </div>
    <div data-wow-iteration="infinite" data-wow-duration="10.15s" class="span3 wow bounce marketing-rocket absolute animated" style="visibility: visible; animation-duration: 10.15s; animation-iteration-count: infinite; animation-name: bounce;">
		<img src="img/thousand-media/grow-business.png" alt="Grow Business" title="Grow Business"  class="planet-img" >
    </div> 
	<div data-wow-iteration="infinite" data-wow-duration="1.5s" data-wow-delay="300ms" class="wow pulse absolute halo-planet" style="visibility: visible; animation-iteration-count: infinite; animation-name: pulse;">
		<img src="img/thousand-media/best-luck-planet.png" alt="Best Luck Planet" title="Best Luck Planet"  class="planet-img" >
    </div>    
    <div data-wow-iteration="infinite" data-wow-duration="2s" class="wow pulse bottom-star absolute">
    	<img src="img/thousand-media/star.png" class="star-img3" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>    
    <div data-wow-iteration="infinite" data-wow-duration="1.5s" class="wow pulse bottom-star2 absolute">
    	<img src="img/thousand-media/star2.png" class="star-img" alt="Penang Marketing Services" title="Penang Marketing Services">
    </div>   
    <div data-wow-iteration="infinite" data-wow-duration="2s" class="wow pulse bottom-star3 absolute">
    	<img src="img/thousand-media/star.png" class="star-img3" alt="Penang Marketing Services" title="Penang Marketing Services">  
    </div>                
</div>

<div id="form-modal" class="modal-css">

  <!-- Modal content need to click learn more-->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-form">&times;</span>
                <!-- <form id="contactform" method="post" action="index.php" class="form-class extra-margin"> -->
                <form class="form-class extra-margin" action="utilities/selectPackageFunction.php" method="POST">
                  <input type="text" name="name" placeholder="Your Name" class="input-name clean form-input" required><br>
                  <!-- <input type="email" name="email" placeholder="Email" class="input-name clean form-input" ><br> -->
                  <input type="text" name="email" placeholder="Email" class="input-name clean form-input" required><br>
                  <!-- <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean form-input" ><br> -->
                  <input type="text" name="phone" placeholder="Contact Number" class="input-name clean form-input" required><br>

                    <select class="input-name clean form-input" name="package" required>
                        <option value="">Please Select a Package</option>
                        <option value="Basic Marketing Pack">Basic Marketing Pack</option>
                        <option value="Essential Marketing Pack">Essential Marketing Pack</option>
                        <option value="Advanced Marketing Pack">Advanced Marketing Pack</option>
                    </select>

                  <textarea name="comments" placeholder="Type your message here" class="input-message clean form-input" ></textarea>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required>                  
                  </div>
                  <div class="float-left radio-p-div">
                  	<p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
                  </div>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required>                   
                  </div>
                  <div class="float-left radio-p-div">                                    
                  	<p class="opt-msg left">I just want to be contacted based on my request/ inquiry</p>
                  </div>
                  <div class="clear"></div>
                   
                  <input type="submit" name="submit" value="Send" class="input-submit white-text clean pointer hover-a-reverse width100">
                </form> 
  </div>

</div>
<?php include 'js.php'; ?>
<script>
var formmodal = document.getElementById("form-modal");
var openform = document.getElementsByClassName("open-form")[0];
var openform1 = document.getElementsByClassName("open-form")[1];
var openform2 = document.getElementsByClassName("open-form")[2];
var closeform = document.getElementsByClassName("close-form")[0];

if(openform){
openform.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform1){
openform1.onclick = function() {
  formmodal.style.display = "block";
}
}
if(openform2){
openform2.onclick = function() {
  formmodal.style.display = "block";
}
}

if(closeform){
closeform.onclick = function() {
  formmodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == formmodal) {
    formmodal.style.display = "none";
  }
}
</script>    
    

        <script>
	$(document).ready(function() {
	var s = $(".menu-white");
	var r = $(".red-logo");
	var w = $(".white-logo");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=200) {
			s.addClass("menu-bg");
			r.addClass("display-none");
			w.addClass("display-block");
		} else {
			s.removeClass("menu-bg");
			r.removeClass("display-none");
			w.removeClass("display-block");	
		}
		});
	});

	</script> 
</body>
</html>