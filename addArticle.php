<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Users.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUsers($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>

<title>Write An Article | Thousand Media</title>
<meta property="og:title" content="Write An Article | Thousand Media" />
<meta property="og:url" content="https://thousandmedia.asia/addArticle.php" />
<link rel="canonical" href="https://thousandmedia.asia/addArticle.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />

<meta property="og:description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings. Social Media Marketing with copywriting, content strategy, illustration design, and others." />

<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, 
, etc">

<?php include 'css.php'; ?>
</head>

<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding min-height100vh overflow menu-distance padding-bottom50">


        <h1 class="h1-title">Add New Article</h1>

        <div class="clear"></div>
        <!-- <form> -->
        <form action="utilities/adminAddArticlesFunction.php" method="POST" enctype="multipart/form-data">

        <!-- <div class="dual-input"> -->
        <div class="width100 overflow">
            <p class="input-top-text">Title*</p>
            <input class="clean blog-input" type="text" placeholder="Title" name="title" id="title" required>      
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-text">Article Slug/Link* (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use -  <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-referlink" alt="Click Me!" title="Click Me!"></p>
            <input class="clean blog-input" type="text" placeholder="digital-marketing-tips" name="article_link" id="article_link" required>              	
        </div>

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-text">Keyword (Use Coma , to Separate Each Keyword, Avoid"')  <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-referkeyword" alt="Click Me!" title="Click Me!"></p>
            <textarea class="clean blog-input keyword-input" type="text" placeholder="Keyword" name="keyword_one" id="keyword_one" required></textarea>  	
        </div>        

        <div class="clear"></div>  

        <div class="width100 overflow">
            <p class="input-top-text">Upload Cover Photo (Less Than 1.8mb)</p>
            <input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
            <!-- Crop Photo Feature --> 	
        </div>        

        <div class="clear"></div>

        <div class="width100 overflow ow-margin-top20">
            <p class="input-top-text">Photo Source/Credit (Optional) <!--<img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!">--></p>
            <input class="clean blog-input" type="text" placeholder="Photo Source:" name="cover_photo_source" id="cover_photo_source">              	
        </div>        

        <div class="clear"></div>

        <div class="width100 overflow">
            <p class="input-top-text">Article Summary/Description (Won't Appear inside the Main Content, Avoid "') <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-referde" alt="Click Me!" title="Click Me!"></p>
            <textarea class="clean blog-input desc-textarea" type="text" placeholder="Article Summary" name="descprition" id="descprition" required></textarea>  	
        </div>        

        <div class="clear"></div>      

        <div class="form-group publish-border input-div width100 overflow">
            <p class="input-top-text">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial) <img src="img/thousand-media/refer3.png" class="attention-png opacity-hover open-refertexteditor" alt="Click Me!" title="Click Me!"></p>
            <textarea name="editor" id="editor" rows="10" cols="80"  class="input-name clean input-textarea admin-input editor-input" ></textarea>
        </div>    

        <div class="clear"></div>    

        <div class="width100 overflow text-center ow-margin-top50">     
            <button class="clean-button clean login-btn pink-button mobile-width100">Submit</button>
        </div>

        </form>

    </div>
    
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>
<script>


var referlinkmodal = document.getElementById("referlink-modal");
var refertexteditormodal = document.getElementById("refertexteditor-modal");
var referdemodal = document.getElementById("referde-modal");
var referkeywordmodal = document.getElementById("referkeyword-modal");


var openreferlink = document.getElementsByClassName("open-referlink")[0];
var openrefertexteditor = document.getElementsByClassName("open-refertexteditor")[0];
var openreferde = document.getElementsByClassName("open-referde")[0];
var openreferkeyword = document.getElementsByClassName("open-referkeyword")[0];




var closereferlink = document.getElementsByClassName("close-referlink")[0];
var closerefer1 = document.getElementsByClassName("close-refer")[1];
var closereferlink1 = document.getElementsByClassName("close-referlink")[1];
var closerefertexteditor = document.getElementsByClassName("close-refertexteditor")[0];
var closerefertexteditor1 = document.getElementsByClassName("close-refertexteditor")[1];
var closereferde = document.getElementsByClassName("close-referde")[0];
var closereferde1 = document.getElementsByClassName("close-referde")[1];
var closereferkeyword = document.getElementsByClassName("close-referkeyword")[0];
var closereferkeyword1 = document.getElementsByClassName("close-referkeyword")[1];

if(openreferlink){
openreferlink.onclick = function() {
  referlinkmodal.style.display = "block";
}
}
if(openrefertexteditor){
openrefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "block";
}
}
if(openreferde){
openreferde.onclick = function() {
  referdemodal.style.display = "block";
}
}
if(openreferkeyword){
openreferkeyword.onclick = function() {
  referkeywordmodal.style.display = "block";
}
}




if(closereferlink){
  closereferlink.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferlink1){
  closereferlink1.onclick = function() {
  referlinkmodal.style.display = "none";
}
}
if(closereferkeyword){
  closereferkeyword.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closereferkeyword1){
  closereferkeyword1.onclick = function() {
  referkeywordmodal.style.display = "none";
}
}
if(closerefertexteditor){
  closerefertexteditor.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closerefertexteditor1){
  closerefertexteditor1.onclick = function() {
  refertexteditormodal.style.display = "none";
}
}
if(closereferde){
  closereferde.onclick = function() {
  referdemodal.style.display = "none";
}
}
if(closereferde1){
  closereferde1.onclick = function() {
  referdemodal.style.display = "none";
}
}


window.onclick = function(event) {

    
  
  if (event.target == referlinkmodal) {
    referlinkmodal.style.display = "none";
  } 
  if (event.target == refertexteditormodal) {
    refertexteditormodal.style.display = "none";
  }
  if (event.target == referdemodal) {
    referdemodal.style.display = "none";
  }        
  if (event.target == referkeywordmodal) {
    referkeywordmodal.style.display = "none";
  }  
    
}
</script>
</body>
</html>