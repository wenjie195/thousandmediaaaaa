<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Users.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['submit'])){

        $username = rewrite($_POST['username']);
        $password = rewrite($_POST['password']);

        $userRows = getUsers($conn," WHERE username = ? ",array("username"),array($username),"s");
        if($userRows)
        {
            $user = $userRows[0];

            $tempPass = hash('sha256',$password);
            $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['usertype'] = $user->getUserType();
                    
                    if($user->getUserType() == 0)
                    {
                        header('Location: ../adminArticlesDashboard.php');
                    }
                    elseif($user->getUserType() == 1)
                    {
                        header('Location: ../userArticlesDashboard.php');
                    }

                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../index.php?type=6');
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=4');
                    //header('Location: ../index.php?type=11');
                    //echo "<script>alert('incorrect password');window.location='../index.php'</script>";
                }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../index.php?type=2');
        }
    }

    $conn->close();
}
?>