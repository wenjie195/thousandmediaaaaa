<!doctype html>
<html>
<head>

<title>Infinity Content Creator | Thousand Media Online Advertising Strategy</title>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://thousandmedia.asia/malaysia-penang-content-copywriting.php" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/fb-meta.jpg" />
<meta property="og:title" content="Infinity Content Creator | Thousand Media Online Advertising Strategy" />
<meta property="og:description" content="We provide unlimited graphic designs and content writings in Penang, Malaysia. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="description" content="We provide unlimited graphic designs and content writings in Penang, Malaysia. Social Media Marketing with copywriting, content strategy, illustration design, and others." />
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing,  website, graphic design, marketing agency, illustration design, digital marketing agency, online advertising, online digital marketing, internet marketing, marketing strategy, marketing plan, business logo design, content creator, copy writing, etc">

<?php include 'css.php'; ?>
  <link rel="canonical" href="https://thousandmedia.asia/malaysia-penang-content-copywriting.php" />
</head>

<body class="body" >

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>
<header id="header" class="header header--fixed same-padding header1 menu-white tart-menu" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
        	<a href="index.php">
    			<img src="img/thousand-media/logo.png" class="logo-img web-logo red-logo" alt="Thousand Media" title="Thousand Media">
            	<img src="img/thousand-media/logo-white.png" class="logo-img mobile-logo white-logo" alt="Thousand Media" title="Thousand Media">
            </a>
           
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="index.php" class="white-text menu-padding red-hover opacity-white-hover ">Thousand Media</a>
 			<div class="dropdown">
            <a class="white-text menu-padding red-hover opacity-white-hover ">Services <img src="img/thousand-media/dropdown.png" class="dropdown-png"></a>
                	<div class="dropdown-content yellow-dropdown-content">

                        <p class="dropdown-p"><a href="malaysia-penang-graphic-design-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Graphic Design</a></p>
                        <p class="dropdown-p"><a href="malaysia-penang-marketing-services.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Marketing Solutions</a></p>
                         <p class="dropdown-p"><a href="malaysia-penang-content-copywriting.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">Content Writing</a></p>                       
                	</div>            
            
            </div>
            <a href="blog.php" class="white-text red-hover opacity-white-hover ">Blog</a>
		<!-- Mobile View-->
            <a href="index.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/about-us.png" class="menu-img" alt="About Thousand Media" title="About Thousand Media">
            </a>
            <a href="malaysia-penang-graphic-design-services.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/graphic-design3.png" class="menu-img" alt="Graphic Design" title="Graphic Design">            
            </a>                
            <a href="malaysia-penang-marketing-services.php" class="white-text menu-padding red-hover2">
            	<img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Marketing Solutions" title="Marketing Solutions">            
            </a>           
            <a href="blog.php" class="white-text red-hover2">
            	<img src="img/thousand-media/blog.png" class="menu-img" alt="Blog" title="Blog">            
            </a>            
        </div>
	</div>

</header>
<div class="width100 overflow blue-bg2" >
    <div class="width100 overflow padding-top100">
    	<div data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow bounce animated float-left right-ani-pic " style="visibility: visible; animation-duration: 15.15s; animation-iteration-count: infinite; animation-name: bounce;">
        	<img src="img/thousand-media/forest3.png" class="width100" alt="Infinity Content Creator" title="Infinity Content Creator">
        </div>
        <div class="span3 wow bounce animated float-right left-detail-div cloud-div" data-wow-iteration="infinite" data-wow-duration="15.15s" style="visibility: visible; animation-duration: 15.15s; animation-iteration-count: infinite; animation-name: bounce;">
        	
        	<p class="bold-detail-p">Infinity Content Creator</p>
        	<p class="normal-detail-p">We make limitless creative contents & keep it affordable to any size of businesses.</p>
        </div>
    </div> 
    <div class="clear"></div>
	<div class="width100 overflow">
        <div class="stand-top middle-text-div">
        	<h1 class="art-h12 ow-white-text">Thousand Media Provides An Unlimited & Creative Contents For All Size Of Businesses.</h1>
        </div>    
    	<img src="img/thousand-media/purple-gradient.png" class="width100 opacity-control" alt="Malaysia Graphic Design Services" title="Malaysia Graphic Design Services">

    </div>
    <div class="clear"></div>
    <div class="width100 same-padding">
    	<div data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow bounce animated float-right right-ani-pic" style="visibility: visible; animation-duration: 15.15s; animation-iteration-count: infinite; animation-name: bounce;">
        	<img src="img/thousand-media/save-time.png" class="width100" alt="Save Time" title="Save Time">
        </div>
        <div class="wow slideInLeft float-left left-detail-div green-gradient" data-wow-offset="300" style="visibility: visible; animation-name: slideInLeft;">
        	<p class="thin-detail-p">We Got Your Back!</p>
        	<p class="left-detail-p">Doesn’t matter what business you are in, we’ll help you save time and money.</p>
        </div>
    </div>
    <div class="clear"></div>
    <div class="width100 overflow padding-top same-padding margin-top50">
    	<div data-wow-iteration="infinite" data-wow-duration="15.15s" class="span3 wow bounce animated float-left right-ani-pic" style="visibility: visible; animation-duration: 15.15s; animation-iteration-count: infinite; animation-name: bounce;">
        	<img src="img/thousand-media/comfortable.png" class="width100" alt="Save Time" title="Save Time">
        </div>
        <div class="wow slideInRight float-right left-detail-div slight-top green-gradient" data-wow-offset="300" style="visibility: visible; animation-name: slideInRight;">
        	<p class="thin-detail-p">You Are the Master Key</p>
        	<p class="left-detail-p">One Flat Monthly Fee. No Contracts, No Hidden Fees, No Stress Of Finding A Professional Copywriter.</p>
        </div>
    </div> 
    <div class="clear"></div>
    <div class="width100 same-padding ow-margin-top50">
    	<div class="width100 white-box-css">
            <h1 class="artz-h1">Infinity Content<br>RM1500 monthly</h1>
            <div class="inifi-left-div">
                
                    <ul class="white-ul">
                        <li>Social Media & Website Content Creation</li>
                        <li>Copywriting Caption</li>
                        <li>Advertisement</li>
                        <li>Case Study</li>
                        <li>Blogs</li>
                        <li>Infographics</li>
                        <li>Checklists</li>
                        <li>Any Other Writing/Content Creation</li>
                    </ul>
                   
            </div>
            <div class="infi-right-div">
                <img src="img/thousand-media/infinity-content.png" data-wow-iteration="infinite" data-wow-duration="7.5s" data-wow-delay="1000ms" class="wow pulse width100" style="visibility: visible; animation-iteration-count: infinite; animation-name: pulse;" alt="Infinity Content" title="Infinity Content">
            </div>
        </div>
        <div class="clear"></div>
        <div class="width100 overflow text-center margin-top30">
        	<div class="pink-radius-button ow-pink-btn red-btn open-form">Learn More</div>
            
        </div>
    </div>

    <div class="clear"></div>
<img src="img/thousand-media/left-bird.png" data-wow-iteration="infinite" data-wow-duration="12s" class="span3 wow bounce animated left-bird" style="visibility: visible; animation-duration: 12s; animation-iteration-count: infinite; animation-name: bounce;">   
<img src="img/thousand-media/right-bird.png" data-wow-iteration="infinite" data-wow-duration="10s" class="span3 wow bounce animated right-bird" style="visibility: visible; animation-duration: 10s; animation-iteration-count: infinite; animation-name: bounce;">      
       
</div>

<div id="form-modal" class="modal-css">

  <!-- Modal content need to click learn more-->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-form">&times;</span>
                <!-- <form id="contactform" method="post" action="index.php" class="form-class extra-margin"> -->
                <form class="form-class extra-margin" action="utilities/selectPackageFunction.php" method="POST">
                  <input type="text" name="name" placeholder="Your Name" class="input-name clean form-input" required><br>
                  <!-- <input type="email" name="email" placeholder="Email" class="input-name clean form-input" ><br> -->
                  <input type="text" name="email" placeholder="Email" class="input-name clean form-input" required><br>
                  <!-- <input type="text" name="telephone" placeholder="Contact Number" class="input-name clean form-input" ><br> -->
                  <input type="text" name="phone" placeholder="Contact Number" class="input-name clean form-input" required><br>

                    <select class="input-name clean form-input" name="package" required>
                        <option value="Infinity Content" selected>Infinity Content</option>
                     
                        
                        
                    </select>

                  <textarea name="comments" placeholder="Type your message here" class="input-message clean form-input" ></textarea>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean" required>                  
                  </div>
                  <div class="float-left radio-p-div">
                  	<p class="opt-msg left"> I want to be contacted with more information about your company's offering marketing services and consulting</p>
                  </div>
                  <div class="clear"></div>
                  <div class="float-left radio-div">
					<input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean"  required>                   
                  </div>
                  <div class="float-left radio-p-div">                                    
                  	<p class="opt-msg left">I just want to be contacted based on my request/ inquiry</p>
                  </div>
                  <div class="clear"></div>
                   
                  <input type="submit" name="submit" value="Send" class="input-submit white-text clean pointer hover-a-reverse width100">
                </form> 
  </div>

</div>


<?php include 'js.php'; ?>
<script>
var formmodal = document.getElementById("form-modal");
var openform = document.getElementsByClassName("open-form")[0];

var closeform = document.getElementsByClassName("close-form")[0];

if(openform){
openform.onclick = function() {
  formmodal.style.display = "block";
}
}


if(closeform){
closeform.onclick = function() {
  formmodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == formmodal) {
    formmodal.style.display = "none";
  }
}
</script>    
    

<script>
	$(document).ready(function() {
	var s = $(".menu-white");
	var r = $(".red-logo");
	var w = $(".white-logo");
	var pos = s.position();					   
	$(window).scroll(function() {
		var windowpos = $(window).scrollTop();
		if (windowpos >= pos.top & windowpos >=200) {
			s.addClass("blue-menu");
			r.addClass("display-none");
			w.addClass("display-block");
		} else {
			s.removeClass("blue-menu");
			r.removeClass("display-none");
			w.removeClass("display-block");	
		}
		});
	});

	</script>  
</body>
</html>