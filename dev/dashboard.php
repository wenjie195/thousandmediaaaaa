<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Dashboard | Thousand Media</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:url" content="https://thousandmedia.asia/dashboard.php" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Dashboard | Thousand Media" />
<meta property="og:description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="author" content="Thousand Media">
<meta name="keywords" content="Thousand Media, ThousandMedia, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, blog, article, tips, news, etc">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137506603-1');
</script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
    </script>
    <script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
    </script>
   <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '390782708409897'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=390782708409897&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
  <link rel="canonical" href="https://thousandmedia.asia/dashboard.php" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/thousandmedia-style.css">
  <link rel="icon" href="./img/thousand-media/thousand-media-favicon.png"   />
</head>

<body class="body" >


<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>



<?php include 'profile-header.php'; ?>
<div class="width100 same-padding">
        <h3 class="thousand-h3 blog-title mtop-50">Promotion/ Announcement</h3>
        <div class="gradient-border first-div-gradient blog-gradient"></div>	
        <div class="clear"></div>
        <img src="img/thousand-media/basicpack-promotion.jpg" class="width100 small-mtop">
        <div class="new-section-divider clear"></div>
		<h3 class="thousand-h3 blog-title margin-top-0">Current Plan</h3>
        <div class="gradient-border first-div-gradient blog-gradient"></div>
		<div class="title-content-divider"></div>
        <div class="white-box-shadow width100 overflow">
        	<div class="float-left img-div">
            	<img src="img/thousand-media/basicpack.png" class="plan-img" height="100%">
            </div>
            <div class="float-left content-part">
            	<p class="red-text profile-box-title">Basic Pack</p>
                <table class="box-table">
                	<tr>
                    	<td class="first-td bold-td">Start Date</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">10/05/2019</td>
                    </tr>
                 	<tr>
                    	<td class="first-td bold-td">End Date</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">10/08/2019</td>
                    </tr>                   
                 	<tr>
                    	<td class="first-td bold-td">Fee</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">RM1,800</td>
                    </tr>  
                  	<tr>
                    	<td class="first-td bold-td">Next Payment Date</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">10/06/2019</td>
                    </tr>                    
                                      
                </table>
            </div>
            <div class="float-right pay-div">
            	<a class="hover-opacity"><img src="img/thousand-media/pay.png" class="width100 pay-img"></a>
            </div>
        </div>
        <div class="new-section-divider clear"></div>
        <h3 class="thousand-h3 blog-title mtop-0">Check Out Other Offers</h3>
        <div class="gradient-border first-div-gradient blog-gradient"></div>
        <div class="title-content-divider"></div>	
		<div class="width100 overflow">
        <div class="package-box">
            <div class="color-box red-box">
                <h3 class="thousand-h3a white-text text-center">Basic Pack</h3>
            </div>
            <div class="box-content-div mh">
                <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/fee.png" class="width100 box-icon" alt="Fee"  title="Fee">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">RM 1,800 Monthly</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/duration.png" class="width100 box-icon"  alt="12-Month Commitment"  title="12-Month Commitment">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">12-Month Commitment</p>
                    </div>
               </div>    
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/how-much.png" class="width100 box-icon" alt="setup fee" title="setup fee">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">One time setup fee: RM1,000</p>
                    </div>
               </div>
               <div class="one-row width100"><div class="gradient-border ow-gradient-width"></div></div>                               
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/marketing-strategy2.png" class="width100 box-icon" alt="Marketing Strategy" title="Marketing Strategy">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Marketing strategy</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/seo.png" class="width100 box-icon" alt="SEO guideline" title="SEO guideline">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">SEO guideline</p>
                    </div>
               </div>           
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/branding-logo.png" class="width100 box-icon" alt="Basic Branding Guideline" title="Basic Branding Guideline">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Basic branding guideline</p>
                    </div>
               </div>            
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/logo-design2.png" class="width100 box-icon" alt="Artwork Design" title="Artwork Design">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Artwork design x2/month</p>
                    </div>
               </div>
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/social-media-caption.png" class="width100 box-icon" alt="Social Media Caption Enhancement" title="Social Media Caption Enhancement">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media caption enhancement x2/month</p>
                    </div>
               </div>           
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/instagram2.png" class="width100 box-icon" alt="Instagram" title="Instagram">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Social media publishing x2/month</p>
                    </div>
               </div> 
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/email.png" class="width100 box-icon" alt="EDM Blasting" title="EDM Blasting">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">EDM blasting x1/month</p>
                    </div>
               </div> 
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/monthly-marketing-report.png" class="width100 box-icon" alt="Monthly Marketing Report" title="Monthly Marketing Report">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Monthly marketing report</p>
                    </div>
               </div>            
               <div class="one-row width100">
                    <div class="left-icon float-left">
                        <img src="img/thousand-media/event-planner.png" class="width100 box-icon" alt="Digital Campaign Planning" title="Digital Campaign Planning">
                    </div>
                    <div class="right-content float-left">
                        <p class="box-p">Digital campaign planning x1/year</p>
                    </div>
               </div>           
                                                
            </div>
            <div class="width100 text-center">
        		<button class="subscribe-button pointer clean separate-bottom subscribe-button80">Subscribe</button>
        	</div>
        </div>
        <div class="package-box middle-package-box dashboard-middle-pack">
            <div class="color-box orange-box">
                <h3 class="thousand-h3a white-text text-center">Intermediate Pack</h3>
            </div> 
            <div class="box-content-div text-center mh">
                <img src="img/thousand-media/intermediate-pack.png" class="package-img margin-auto" alt="Intermediate Digital Marketing Service Pack"  title="Intermediate Digital Marketing Service Pack">
                <p class="text-center coming-p">Coming Soon</p>
            </div> 
            <div class="width100 text-center">
       			<button class="subscribe-button pointer clean separate-bottom subscribe-button80 grey-button">Upgrade</button>
        		<!--<button class="subscribe-button pointer clean separate-bottom">Subscribe</button>-->
        	</div>  
        </div>
        <div class="package-box">
            <div class="color-box golden-box">
                <h3 class="thousand-h3a white-text text-center">Advanced Pack</h3>
            </div> 
            <div class="box-content-div text-center mh">
                <img src="img/thousand-media/advanced-pack.png" class="package-img margin-auto"alt="Advanced Digital Marketing Service Pack"  title="Advanced Digital Marketing Service Pack">
                <p class="text-center coming-p">Coming Soon</p>
            </div> 
            <div class="width100 text-center">
        		<button class="subscribe-button pointer clean separate-bottom subscribe-button80 grey-button">Upgrade</button>
                <!--<button class="subscribe-button pointer clean separate-bottom">Subscribe</button>-->
        	</div> 
        </div>
        <div class="new-section-divider clear"></div>
        <h3 class="thousand-h3 blog-title mtop-0">Contact Us for Customize Plan</h3>
        <div class="gradient-border first-div-gradient blog-gradient"></div>        
        </div>
        
     <div class="width115">
        <div class="float-left left-content3 left-content">
            <p class="thousand-p3">Would you like to make your own unique selection of marketing services? Call us now!</p>
            <div class="one-row width100">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/call.png" class="width100 box-icon contact-left-icon" alt="Contact Us Thousand Media"  title="Contact Us Thousand Media">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p"><span class="web-span">+60 4 638 6082</span><a href="tel:+6046386082" class="tel-a">+60 4 638 6082</a></p>
                </div>
           </div> 
            <div class="one-row width100">
                <div class="left-icon float-left">
                    <img src="img/thousand-media/email.png" class="width100 box-icon contact-left-icon" alt="Thousand Media Email"  title="Thousand Media Email">
                </div>
                <div class="right-content float-left">
                    <p class="box-p contact-p">thousandmedia.asia@gmail.com</p>
                </div>
           </div>        
           <p class="icon-p">
                <a href="https://www.instagram.com/thousandmedia.asia/" target="_blank">
                    <img src="img/thousand-media/instagram2.png" class="social-icon hover-opacity" alt="Thousand Media Instagram"  title="Thousand Media Instagram">
                </a>
                <a href="https://www.facebook.com/thousandmedia" target="_blank">
                    <img src="img/thousand-media/facebook2.png" class="social-icon social-icon2 hover-opacity" alt="Thousand Media Facebook"  title="Thousand Media Facebook">
                </a>
           </p>    
        </div>
        <div class="float-right right-img-div">
            <img src="img/thousand-media/customized-package.png" class="width100"  alt="Customized Digital Marketing Package"  title="Customized Digital Marketing Package">
        </div>
    </div>


     <div class="new-section-divider clear"></div>
    <h3 class="thousand-h3 blog-title mtop-0">Ad-hoc Services</h3>
    <div class="gradient-border first-div-gradient blog-gradient"></div>
    <div class="title-content-divider"></div>	
    
    <div class="four-div-container width100">
        <div class="four-div float-left text-center">
            <img src="img/thousand-media/video.png" class="four-image" alt="Marketing Strategy" title="Marketing Strategy">
            <p class="four-box-p ad-mh"><b>Film Production (3 mins)</b><br><span class="low-weight">From RM5,000</span></p>        
        </div>
        
        <div class="four-div float-left text-center middle-4-div four-second-div">
            <img src="img/thousand-media/website.png" class="four-image" alt="SEO Guideline" title="SEO Guideline">
            <p class="four-box-p ad-mh">Website Development</p>
        </div>
        
        <div class="four-div float-left text-center middle-4-div">
            <img src="img/thousand-media/app.png" class="four-image" alt="Basic Branding" title="Basic Branding">
            <p class="four-box-p">App Development</p>
        </div>
        
        <div class="four-div float-left text-center four-second-div">
            <img src="img/thousand-media/influencer-marketing.png" class="four-image" alt="Artwork Design x2/Month" title="Artwork Design x2/Month">
            <p class="four-box-p">Influencer Marketing</p>
        </div>      	
    </div>
    <div class="clear"></div>
</div>       
        </div>
        
  
        
               
</div>




<div class="footer-div width100 same-padding">
    <p class="footer-p white-text text-center">© 2019 Thousand Media, All Rights Reserved.</p>
</div>
<?php include 'js.php'; ?>
<style>
.dashboard-menu{
	color:#d60d26;
	font-weight:600;}
.menu-bg{
	background-color:white !important;
	background:white !important;}
</style>


 

</body>
</html>