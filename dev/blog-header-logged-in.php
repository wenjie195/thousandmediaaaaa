<header id="header" class="header header--fixed same-padding header1 menu-white white-bg" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<a href="index.php" class="hover-opacity"><img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media"></a>           
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="blog.php" class="menu-padding red-hover blog-menu-a red-menu latest-menu">Latest</a>
            <a href="marketing.php" class="menu-padding red-hover blog-menu-a red-menu marketing-menu">Marketing</a>
            <a href="lifestyle.php" class="menu-padding red-hover blog-menu-a red-menu lifestyle-menu">Lifestyle</a>
            <a href="business.php" class="menu-padding red-hover blog-menu-a red-menu business-menu">Business</a>
            <a href="promotion.php" class="menu-padding red-hover blog-menu-a red-menu promotion-menu">Promotion</a>           
            <a href="bookmark.php" class="menu-padding red-hover blog-menu-a red-menu bookmark-menu">Bookmark</a>
            <a class="menu-padding red-hover blog-menu-a red-menu">Logout</a>
        
		<!-- Mobile View-->
            <a href="blog.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/latest2.png" class="menu-img blog-menu-img" alt="Latest" title="Latest">
            </a>
            <a href="marketing.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/marketing2.png" class="menu-img blog-menu-img" alt="Marketing" title="Marketing">
            </a>
             <a href="marketing.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/lifestyle-menu.png" class="menu-img blog-menu-img" alt="Lifestyle" title="Lifestyle">
            </a>           
            
            <a href="business.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/business3.png" class="menu-img blog-menu-img" alt="Business" title="Business">            
            </a>
            <a href="promotion.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/promotion3.png" class="menu-img blog-menu-img" alt="Promotion" title="Promotion">            
            </a>
            <a href="bookmark.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/bookmark2.png" class="menu-img blog-menu-img" alt="Login" title="Login">            
            </a>
            <a class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/logout3.png" class="menu-img blog-menu-img" alt="Logout" title="Logout">            
            </a>
		<!-- Modal-->               
        </div>
	</div>

</header>